
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsMaintenanceCoeficcients
/// </summary>
public class clsMaintenanceCoeficcients
{
    #region MEMBER VARIABLES

     private     int m_iMaintenanceCoeficcientID;
     private     DateTime m_dtAdded;
     private     int m_iAddedBy;
     private     DateTime m_dtEdited;
     private     int m_iEditedBy;
     private     string m_strTitle;
     private     string m_strDescription;
     private     double m_dblPrice;
     private     int m_iProductCategoryListID;
     private     int m_iBiomassListID;
     private     int m_iCarbonSourceCategoryListID;
     private     double m_dblCoeficcient;
     private     double m_dblTime;
     private     string m_strReferenceNotes;
     private     bool m_bIsAnEstimate;
     private     bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

    public int iMaintenanceCoeficcientID
    {
        get
        {
            return m_iMaintenanceCoeficcientID;
        }
    }

    public    DateTime dtAdded
    {
        get
        {
            return m_dtAdded;
        }
        set
        {
            m_dtAdded = value;
        }
    }

    public    int iAddedBy
    {
        get
        {
            return m_iAddedBy;
        }
        set
        {
            m_iAddedBy = value;
        }
    }

    public    DateTime dtEdited
    {
        get
        {
            return m_dtEdited;
        }
        set
        {
            m_dtEdited = value;
        }
    }

    public    int iEditedBy
    {
        get
        {
            return m_iEditedBy;
        }
        set
        {
            m_iEditedBy = value;
        }
    }

    public    string strTitle
    {
        get
        {
            return m_strTitle;
        }
        set
        {
            m_strTitle = value;
        }
    }

    public    string strDescription
    {
        get
        {
            return m_strDescription;
        }
        set
        {
            m_strDescription = value;
        }
    }

    public    double dblPrice
    {
        get
        {
            return m_dblPrice;
        }
        set
        {
            m_dblPrice = value;
        }
    }

    public    int iProductCategoryListID
    {
        get
        {
            return m_iProductCategoryListID;
        }
        set
        {
            m_iProductCategoryListID = value;
        }
    }

    public    int iBiomassListID
    {
        get
        {
            return m_iBiomassListID;
        }
        set
        {
            m_iBiomassListID = value;
        }
    }

    public    int iCarbonSourceCategoryListID
    {
        get
        {
            return m_iCarbonSourceCategoryListID;
        }
        set
        {
            m_iCarbonSourceCategoryListID = value;
        }
    }

    public    double dblCoeficcient
    {
        get
        {
            return m_dblCoeficcient;
        }
        set
        {
            m_dblCoeficcient = value;
        }
    }

    public    double dblTime
    {
        get
        {
            return m_dblTime;
        }
        set
        {
            m_dblTime = value;
        }
    }

    public    string strReferenceNotes
    {
        get
        {
            return m_strReferenceNotes;
        }
        set
        {
            m_strReferenceNotes = value;
        }
    }

    public    bool bIsAnEstimate
    {
        get
        {
            return m_bIsAnEstimate;
        }
        set
        {
            m_bIsAnEstimate = value;
        }
    }

    public    bool bIsDeleted
    {
        get
        {
            return m_bIsDeleted;
        }
        set
        {
            m_bIsDeleted = value;
        }
    }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsMaintenanceCoeficcients()
    {
        m_iMaintenanceCoeficcientID = 0;
    }

    public clsMaintenanceCoeficcients(int iMaintenanceCoeficcientID)
    {
        m_iMaintenanceCoeficcientID = iMaintenanceCoeficcientID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iMaintenanceCoeficcientID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@strTitle", m_strTitle),
                        new SqlParameter("@strDescription", m_strDescription),
                        new SqlParameter("@dblPrice", m_dblPrice),
                        new SqlParameter("@iProductCategoryListID", m_iProductCategoryListID),
                        new SqlParameter("@iBiomassListID", m_iBiomassListID),
                        new SqlParameter("@iCarbonSourceCategoryListID", m_iCarbonSourceCategoryListID),
                        new SqlParameter("@dblCoeficcient", m_dblCoeficcient),
                        new SqlParameter("@dblTime", m_dblTime),
                        new SqlParameter("@strReferenceNotes", m_strReferenceNotes),
                        new SqlParameter("@bIsAnEstimate", m_bIsAnEstimate)                  
                  };

                  //### Add
                  m_iMaintenanceCoeficcientID = (int)clsDataAccess.ExecuteScalar("spMaintenanceCoeficcientsInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iMaintenanceCoeficcientID", m_iMaintenanceCoeficcientID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@strTitle", m_strTitle),
                         new SqlParameter("@strDescription", m_strDescription),
                         new SqlParameter("@dblPrice", m_dblPrice),
                         new SqlParameter("@iProductCategoryListID", m_iProductCategoryListID),
                         new SqlParameter("@iBiomassListID", m_iBiomassListID),
                         new SqlParameter("@iCarbonSourceCategoryListID", m_iCarbonSourceCategoryListID),
                         new SqlParameter("@dblCoeficcient", m_dblCoeficcient),
                         new SqlParameter("@dblTime", m_dblTime),
                         new SqlParameter("@strReferenceNotes", m_strReferenceNotes),
                         new SqlParameter("@bIsAnEstimate", m_bIsAnEstimate)
          };
          //### Update
          clsDataAccess.Execute("spMaintenanceCoeficcientsUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iMaintenanceCoeficcientID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iMaintenanceCoeficcientID", iMaintenanceCoeficcientID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spMaintenanceCoeficcientsDelete", sqlParameter);
        }

        public static DataTable GetMaintenanceCoeficcientsList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spMaintenanceCoeficcientsList", EmptySqlParameter);
        }
        public static DataTable GetMaintenanceCoeficcientsList(string strFilterExpression, string strSortExpression)
        {
            DataView dvMaintenanceCoeficcientsList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvMaintenanceCoeficcientsList = clsDataAccess.GetDataView("spMaintenanceCoeficcientsList", EmptySqlParameter);
            dvMaintenanceCoeficcientsList.RowFilter = strFilterExpression;
            dvMaintenanceCoeficcientsList.Sort = strSortExpression;

            return dvMaintenanceCoeficcientsList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iMaintenanceCoeficcientID", m_iMaintenanceCoeficcientID)
                        };
                DataRow drRecord = clsDataAccess.GetRecord("spMaintenanceCoeficcientsGetRecord", sqlParameter);

               m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
               m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

           if (drRecord["dtEdited"] != DBNull.Value)
               m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

           if (drRecord["iEditedBy"] != DBNull.Value)
               m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

                   m_strTitle = drRecord["strTitle"].ToString();
                   m_strDescription = drRecord["strDescription"].ToString();
                   m_dblPrice = Convert.ToDouble(drRecord["dblPrice"]);
               m_iProductCategoryListID = Convert.ToInt32(drRecord["iProductCategoryListID"]);
               m_iBiomassListID = Convert.ToInt32(drRecord["iBiomassListID"]);
               m_iCarbonSourceCategoryListID = Convert.ToInt32(drRecord["iCarbonSourceCategoryListID"]);
                   m_dblCoeficcient = Convert.ToDouble(drRecord["dblCoeficcient"]);
                   m_dblTime = Convert.ToDouble(drRecord["dblTime"]);
                   m_strReferenceNotes = drRecord["strReferenceNotes"].ToString();
                   m_bIsAnEstimate = Convert.ToBoolean(drRecord["bIsAnEstimate"]);
                   m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}