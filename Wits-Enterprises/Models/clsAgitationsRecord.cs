
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsAgitations
/// </summary>
public class clsAgitations
{
    #region MEMBER VARIABLES

     private     int m_iAgitationID;
     private     DateTime m_dtAdded;
     private     int m_iAddedBy;
     private     DateTime m_dtEdited;
     private     int m_iEditedBy;
     private     string m_strTitle;
     private     string m_strDescription;
     private     double m_dblPrice;
     private     int m_iImpellerID;
     private     double m_dblNumberOfTanks;
     private     double m_dblAgitationEfficiency;
     private     double m_dblPowerPerUnitVolume;
     private     bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

    public int iAgitationID
    {
        get
        {
            return m_iAgitationID;
        }
    }

    public    DateTime dtAdded
    {
        get
        {
            return m_dtAdded;
        }
        set
        {
            m_dtAdded = value;
        }
    }

    public    int iAddedBy
    {
        get
        {
            return m_iAddedBy;
        }
        set
        {
            m_iAddedBy = value;
        }
    }

    public    DateTime dtEdited
    {
        get
        {
            return m_dtEdited;
        }
        set
        {
            m_dtEdited = value;
        }
    }

    public    int iEditedBy
    {
        get
        {
            return m_iEditedBy;
        }
        set
        {
            m_iEditedBy = value;
        }
    }

    public    string strTitle
    {
        get
        {
            return m_strTitle;
        }
        set
        {
            m_strTitle = value;
        }
    }

    public    string strDescription
    {
        get
        {
            return m_strDescription;
        }
        set
        {
            m_strDescription = value;
        }
    }

    public    double dblPrice
    {
        get
        {
            return m_dblPrice;
        }
        set
        {
            m_dblPrice = value;
        }
    }

    public    int iImpellerID
    {
        get
        {
            return m_iImpellerID;
        }
        set
        {
            m_iImpellerID = value;
        }
    }

    public    double dblNumberOfTanks
    {
        get
        {
            return m_dblNumberOfTanks;
        }
        set
        {
            m_dblNumberOfTanks = value;
        }
    }

    public    double dblAgitationEfficiency
    {
        get
        {
            return m_dblAgitationEfficiency;
        }
        set
        {
            m_dblAgitationEfficiency = value;
        }
    }

    public    double dblPowerPerUnitVolume
    {
        get
        {
            return m_dblPowerPerUnitVolume;
        }
        set
        {
            m_dblPowerPerUnitVolume = value;
        }
    }

    public    bool bIsDeleted
    {
        get
        {
            return m_bIsDeleted;
        }
        set
        {
            m_bIsDeleted = value;
        }
    }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsAgitations()
    {
        m_iAgitationID = 0;
    }

    public clsAgitations(int iAgitationID)
    {
        m_iAgitationID = iAgitationID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iAgitationID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@strTitle", m_strTitle),
                        new SqlParameter("@strDescription", m_strDescription),
                        new SqlParameter("@dblPrice", m_dblPrice),
                        new SqlParameter("@iImpellerID", m_iImpellerID),
                        new SqlParameter("@dblNumberOfTanks", m_dblNumberOfTanks),
                        new SqlParameter("@dblAgitationEfficiency", m_dblAgitationEfficiency),
                        new SqlParameter("@dblPowerPerUnitVolume", m_dblPowerPerUnitVolume)                  
                  };

                  //### Add
                  m_iAgitationID = (int)clsDataAccess.ExecuteScalar("spAgitationsInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iAgitationID", m_iAgitationID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@strTitle", m_strTitle),
                         new SqlParameter("@strDescription", m_strDescription),
                         new SqlParameter("@dblPrice", m_dblPrice),
                         new SqlParameter("@iImpellerID", m_iImpellerID),
                         new SqlParameter("@dblNumberOfTanks", m_dblNumberOfTanks),
                         new SqlParameter("@dblAgitationEfficiency", m_dblAgitationEfficiency),
                         new SqlParameter("@dblPowerPerUnitVolume", m_dblPowerPerUnitVolume)
          };
          //### Update
          clsDataAccess.Execute("spAgitationsUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iAgitationID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iAgitationID", iAgitationID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spAgitationsDelete", sqlParameter);
        }

        public static DataTable GetAgitationsList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spAgitationsList", EmptySqlParameter);
        }
        public static DataTable GetAgitationsList(string strFilterExpression, string strSortExpression)
        {
            DataView dvAgitationsList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvAgitationsList = clsDataAccess.GetDataView("spAgitationsList", EmptySqlParameter);
            dvAgitationsList.RowFilter = strFilterExpression;
            dvAgitationsList.Sort = strSortExpression;

            return dvAgitationsList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iAgitationID", m_iAgitationID)
                        };
                DataRow drRecord = clsDataAccess.GetRecord("spAgitationsGetRecord", sqlParameter);

               m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
               m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

           if (drRecord["dtEdited"] != DBNull.Value)
               m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

           if (drRecord["iEditedBy"] != DBNull.Value)
               m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

                   m_strTitle = drRecord["strTitle"].ToString();
                   m_strDescription = drRecord["strDescription"].ToString();
                   m_dblPrice = Convert.ToDouble(drRecord["dblPrice"]);
               m_iImpellerID = Convert.ToInt32(drRecord["iImpellerID"]);
                   m_dblNumberOfTanks = Convert.ToDouble(drRecord["dblNumberOfTanks"]);
                   m_dblAgitationEfficiency = Convert.ToDouble(drRecord["dblAgitationEfficiency"]);
                   m_dblPowerPerUnitVolume = Convert.ToDouble(drRecord["dblPowerPerUnitVolume"]);
                   m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}