
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsGrowthRates
/// </summary>
public class clsGrowthRates
{
    #region MEMBER VARIABLES

     private     int m_iGrowthRateID;
     private     DateTime m_dtAdded;
     private     int m_iAddedBy;
     private     DateTime m_dtEdited;
     private     int m_iEditedBy;
     private     string m_strTitle;
     private     string m_strDescription;
     private     double m_dblPrice;
     private     int m_iProductCategoryListID;
     private     int m_iBiomassListID;
     private     int m_iCarbonSourceCategoryListID;
     private     double m_dblInitialBiomassConcentration;
     private     double m_dblFinalBiomassConcentration;
     private     double m_dblMaxSpecificGrowth;
     private     double m_dblLimitingNutrientConcentrationHalfLife;
     private     double m_dblFinalProductConcentration;
     private     double m_dblRx;
     private     string m_strReferenceNotes;
     private     bool m_bIsAnEstimate;
     private     bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

    public int iGrowthRateID
    {
        get
        {
            return m_iGrowthRateID;
        }
    }

    public    DateTime dtAdded
    {
        get
        {
            return m_dtAdded;
        }
        set
        {
            m_dtAdded = value;
        }
    }

    public    int iAddedBy
    {
        get
        {
            return m_iAddedBy;
        }
        set
        {
            m_iAddedBy = value;
        }
    }

    public    DateTime dtEdited
    {
        get
        {
            return m_dtEdited;
        }
        set
        {
            m_dtEdited = value;
        }
    }

    public    int iEditedBy
    {
        get
        {
            return m_iEditedBy;
        }
        set
        {
            m_iEditedBy = value;
        }
    }

    public    string strTitle
    {
        get
        {
            return m_strTitle;
        }
        set
        {
            m_strTitle = value;
        }
    }

    public    string strDescription
    {
        get
        {
            return m_strDescription;
        }
        set
        {
            m_strDescription = value;
        }
    }

    public    double dblPrice
    {
        get
        {
            return m_dblPrice;
        }
        set
        {
            m_dblPrice = value;
        }
    }

    public    int iProductCategoryListID
    {
        get
        {
            return m_iProductCategoryListID;
        }
        set
        {
            m_iProductCategoryListID = value;
        }
    }

    public    int iBiomassListID
    {
        get
        {
            return m_iBiomassListID;
        }
        set
        {
            m_iBiomassListID = value;
        }
    }

    public    int iCarbonSourceCategoryListID
    {
        get
        {
            return m_iCarbonSourceCategoryListID;
        }
        set
        {
            m_iCarbonSourceCategoryListID = value;
        }
    }

    public    double dblInitialBiomassConcentration
    {
        get
        {
            return m_dblInitialBiomassConcentration;
        }
        set
        {
            m_dblInitialBiomassConcentration = value;
        }
    }

    public    double dblFinalBiomassConcentration
    {
        get
        {
            return m_dblFinalBiomassConcentration;
        }
        set
        {
            m_dblFinalBiomassConcentration = value;
        }
    }

    public    double dblMaxSpecificGrowth
    {
        get
        {
            return m_dblMaxSpecificGrowth;
        }
        set
        {
            m_dblMaxSpecificGrowth = value;
        }
    }

    public    double dblLimitingNutrientConcentrationHalfLife
    {
        get
        {
            return m_dblLimitingNutrientConcentrationHalfLife;
        }
        set
        {
            m_dblLimitingNutrientConcentrationHalfLife = value;
        }
    }

    public    double dblFinalProductConcentration
    {
        get
        {
            return m_dblFinalProductConcentration;
        }
        set
        {
            m_dblFinalProductConcentration = value;
        }
    }

    public    double dblRx
    {
        get
        {
            return m_dblRx;
        }
        set
        {
            m_dblRx = value;
        }
    }

    public    string strReferenceNotes
    {
        get
        {
            return m_strReferenceNotes;
        }
        set
        {
            m_strReferenceNotes = value;
        }
    }

    public    bool bIsAnEstimate
    {
        get
        {
            return m_bIsAnEstimate;
        }
        set
        {
            m_bIsAnEstimate = value;
        }
    }

    public    bool bIsDeleted
    {
        get
        {
            return m_bIsDeleted;
        }
        set
        {
            m_bIsDeleted = value;
        }
    }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsGrowthRates()
    {
        m_iGrowthRateID = 0;
    }

    public clsGrowthRates(int iGrowthRateID)
    {
        m_iGrowthRateID = iGrowthRateID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iGrowthRateID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@strTitle", m_strTitle),
                        new SqlParameter("@strDescription", m_strDescription),
                        new SqlParameter("@dblPrice", m_dblPrice),
                        new SqlParameter("@iProductCategoryListID", m_iProductCategoryListID),
                        new SqlParameter("@iBiomassListID", m_iBiomassListID),
                        new SqlParameter("@iCarbonSourceCategoryListID", m_iCarbonSourceCategoryListID),
                        new SqlParameter("@dblInitialBiomassConcentration", m_dblInitialBiomassConcentration),
                        new SqlParameter("@dblFinalBiomassConcentration", m_dblFinalBiomassConcentration),
                        new SqlParameter("@dblMaxSpecificGrowth", m_dblMaxSpecificGrowth),
                        new SqlParameter("@dblLimitingNutrientConcentrationHalfLife", m_dblLimitingNutrientConcentrationHalfLife),
                        new SqlParameter("@dblFinalProductConcentration", m_dblFinalProductConcentration),
                        new SqlParameter("@dblRx", m_dblRx),
                        new SqlParameter("@strReferenceNotes", m_strReferenceNotes),
                        new SqlParameter("@bIsAnEstimate", m_bIsAnEstimate)                  
                  };

                  //### Add
                  m_iGrowthRateID = (int)clsDataAccess.ExecuteScalar("spGrowthRatesInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iGrowthRateID", m_iGrowthRateID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@strTitle", m_strTitle),
                         new SqlParameter("@strDescription", m_strDescription),
                         new SqlParameter("@dblPrice", m_dblPrice),
                         new SqlParameter("@iProductCategoryListID", m_iProductCategoryListID),
                         new SqlParameter("@iBiomassListID", m_iBiomassListID),
                         new SqlParameter("@iCarbonSourceCategoryListID", m_iCarbonSourceCategoryListID),
                         new SqlParameter("@dblInitialBiomassConcentration", m_dblInitialBiomassConcentration),
                         new SqlParameter("@dblFinalBiomassConcentration", m_dblFinalBiomassConcentration),
                         new SqlParameter("@dblMaxSpecificGrowth", m_dblMaxSpecificGrowth),
                         new SqlParameter("@dblLimitingNutrientConcentrationHalfLife", m_dblLimitingNutrientConcentrationHalfLife),
                         new SqlParameter("@dblFinalProductConcentration", m_dblFinalProductConcentration),
                         new SqlParameter("@dblRx", m_dblRx),
                         new SqlParameter("@strReferenceNotes", m_strReferenceNotes),
                         new SqlParameter("@bIsAnEstimate", m_bIsAnEstimate)
          };
          //### Update
          clsDataAccess.Execute("spGrowthRatesUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iGrowthRateID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iGrowthRateID", iGrowthRateID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spGrowthRatesDelete", sqlParameter);
        }

        public static DataTable GetGrowthRatesList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spGrowthRatesList", EmptySqlParameter);
        }
        public static DataTable GetGrowthRatesList(string strFilterExpression, string strSortExpression)
        {
            DataView dvGrowthRatesList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvGrowthRatesList = clsDataAccess.GetDataView("spGrowthRatesList", EmptySqlParameter);
            dvGrowthRatesList.RowFilter = strFilterExpression;
            dvGrowthRatesList.Sort = strSortExpression;

            return dvGrowthRatesList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iGrowthRateID", m_iGrowthRateID)
                        };
                DataRow drRecord = clsDataAccess.GetRecord("spGrowthRatesGetRecord", sqlParameter);

               m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
               m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

           if (drRecord["dtEdited"] != DBNull.Value)
               m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

           if (drRecord["iEditedBy"] != DBNull.Value)
               m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

                   m_strTitle = drRecord["strTitle"].ToString();
                   m_strDescription = drRecord["strDescription"].ToString();
                   m_dblPrice = Convert.ToDouble(drRecord["dblPrice"]);
               m_iProductCategoryListID = Convert.ToInt32(drRecord["iProductCategoryListID"]);
               m_iBiomassListID = Convert.ToInt32(drRecord["iBiomassListID"]);
               m_iCarbonSourceCategoryListID = Convert.ToInt32(drRecord["iCarbonSourceCategoryListID"]);
                   m_dblInitialBiomassConcentration = Convert.ToDouble(drRecord["dblInitialBiomassConcentration"]);
                   m_dblFinalBiomassConcentration = Convert.ToDouble(drRecord["dblFinalBiomassConcentration"]);
                   m_dblMaxSpecificGrowth = Convert.ToDouble(drRecord["dblMaxSpecificGrowth"]);
                   m_dblLimitingNutrientConcentrationHalfLife = Convert.ToDouble(drRecord["dblLimitingNutrientConcentrationHalfLife"]);
                   m_dblFinalProductConcentration = Convert.ToDouble(drRecord["dblFinalProductConcentration"]);
                   m_dblRx = Convert.ToDouble(drRecord["dblRx"]);
                   m_strReferenceNotes = drRecord["strReferenceNotes"].ToString();
                   m_bIsAnEstimate = Convert.ToBoolean(drRecord["bIsAnEstimate"]);
                   m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}