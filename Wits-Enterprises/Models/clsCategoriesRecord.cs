using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsCategories
/// </summary>
public class clsCategories
{
    #region MEMBER VARIABLES

    private int m_iCategoryID;
    private DateTime m_dtAdded;
    private int m_iAddedBy;
    private DateTime m_dtEdited;
    private int m_iEditedBy;
    private int m_iLevelID;
    private String m_strTitle;
    private String m_strDescription;
    private String m_strPathToImages;
    private String m_strMasterImage;
    private bool m_bIsFeatured;
    private bool m_bIsDeleted;

    #endregion

    #region PROPERTIES

    public int iCategoryID
    {
        get
        {
            return m_iCategoryID;
        }
    }

    public DateTime dtAdded
    {
        get
        {
            return m_dtAdded;
        }
        set
        {
            m_dtAdded = value;
        }
    }

    public int iAddedBy
    {
        get
        {
            return m_iAddedBy;
        }
        set
        {
            m_iAddedBy = value;
        }
    }

    public DateTime dtEdited
    {
        get
        {
            return m_dtEdited;
        }
        set
        {
            m_dtEdited = value;
        }
    }

    public int iEditedBy
    {
        get
        {
            return m_iEditedBy;
        }
        set
        {
            m_iEditedBy = value;
        }
    }

    public int iLevelID
    {
        get
        {
            return m_iLevelID;
        }
        set
        {
            m_iLevelID = value;
        }
    }

    public String strTitle
    {
        get
        {
            return m_strTitle;
        }
        set
        {
            m_strTitle = value;
        }
    }

    public String strDescription
    {
        get
        {
            return m_strDescription;
        }
        set
        {
            m_strDescription = value;
        }
    }

    public String strPathToImages
    {
        get
        {
            return m_strPathToImages;
        }
        set
        {
            m_strPathToImages = value;
        }
    }

    public String strMasterImage
    {
        get
        {
            return m_strMasterImage;
        }
        set
        {
            m_strMasterImage = value;
        }
    }

    public bool bIsFeatured
    {
        get
        {
            return m_bIsFeatured;
        }
        set
        {
            m_bIsFeatured = value;
        }
    }

    public bool bIsDeleted
    {
        get
        {
            return m_bIsDeleted;
        }
        set
        {
            m_bIsDeleted = value;
        }
    }


    #endregion

    #region CONSTRUCTORS

    public clsCategories()
    {
        m_iCategoryID = 0;
    }

    public clsCategories(int iCategoryID)
    {
        m_iCategoryID = iCategoryID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

    public virtual void Update()
    {
        try
        {
            if (iCategoryID == 0)
            {
                //### Assign values to the parameter list for each corresponding column in the DB
                SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@iLevelID", m_iLevelID),
                        new SqlParameter("@strTitle", m_strTitle),
                        new SqlParameter("@strDescription", m_strDescription),
                        new SqlParameter("@strPathToImages", m_strPathToImages),
                        new SqlParameter("@strMasterImage", m_strMasterImage),
                        new SqlParameter("@bIsFeatured", m_bIsFeatured)
                  };

                //### Add
                m_iCategoryID = (int)clsDataAccess.ExecuteScalar("spCategoriesInsert", sqlParametersInsert);
            }
            else
            {
                //### Assign values to the parameter list for each corresponding column in the DB
                SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iCategoryID", m_iCategoryID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@iLevelID", m_iLevelID),
                         new SqlParameter("@strTitle", m_strTitle),
                         new SqlParameter("@strDescription", m_strDescription),
                         new SqlParameter("@strPathToImages", m_strPathToImages),
                         new SqlParameter("@strMasterImage", m_strMasterImage),
                         new SqlParameter("@bIsFeatured", m_bIsFeatured)
                    };
                //### Update
                clsDataAccess.Execute("spCategoriesUpdate", sqlParametersUpdate);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public static void Delete(int iCategoryID)
    {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iCategoryID", iCategoryID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spCategoriesDelete", sqlParameter);
    }

    public static DataTable GetCategoriesList()
    {
        SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
        return clsDataAccess.GetDataTable("spCategoriesList", EmptySqlParameter);
    }

    public static DataTable GetCategoriesList(string strFilterExpression, string strSortExpression)
    {
        DataView dvCategoriesList = new DataView();

        SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
        dvCategoriesList = clsDataAccess.GetDataView("spCategoriesList", EmptySqlParameter);
        dvCategoriesList.RowFilter = strFilterExpression;
        dvCategoriesList.Sort = strSortExpression;

        return dvCategoriesList.ToTable();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns>All featured Categories</returns>
    public static DataTable GetFeaturedCategories()
    {
        SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
        return clsDataAccess.GetDataTable("spCategoriesFeaturedList", EmptySqlParameter);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="iCategoryTypeID">1 for Products And 2 For Services</param>
    /// <returns>The first Category with a Product/Service Linked to it</returns>
    //public static DataTable GetFirstCategory(int iCategoryTypeID)
    //{
    //    DataView dvCategoriesFirst = new DataView();

    //    SqlParameter[] EmptySqlParameter = new SqlParameter[] { 
    //        new SqlParameter("@iCategoryTypeID",iCategoryTypeID)
    //    };
    //    dvCategoriesFirst = clsDataAccess.GetDataView("spCategoriesgetFirst", EmptySqlParameter);
    //    return dvCategoriesFirst.ToTable();
    //}

    #endregion

    #region PROTECTED METHODS

    protected virtual void GetData()
    {
        try
        {
            //### Populate
            SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iCategoryID", m_iCategoryID)
                        };
            DataRow drRecord = clsDataAccess.GetRecord("spCategoriesGetRecord", sqlParameter);

            m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
            m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

            if (drRecord["dtEdited"] != DBNull.Value)
                m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

            if (drRecord["iEditedBy"] != DBNull.Value)
                m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

            m_iLevelID = Convert.ToInt32(drRecord["iLevelID"]);
            m_strTitle = drRecord["strTitle"].ToString();
            m_strDescription = drRecord["strDescription"].ToString();

            m_strMasterImage = drRecord["strMasterImage"].ToString();
            m_strPathToImages = drRecord["strPathToImages"].ToString();

            m_bIsFeatured = Convert.ToBoolean(drRecord["bIsFeatured"]);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion
}