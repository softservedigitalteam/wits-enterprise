
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsBiomassLists
/// </summary>
public class clsBiomassLists
{
    #region MEMBER VARIABLES

     private     int m_iBiomassListID;
     private     DateTime m_dtAdded;
     private     int m_iAddedBy;
     private     DateTime m_dtEdited;
     private     int m_iEditedBy;
     private     string m_strTitle;
     private     string m_strDescription;
     private     int m_iChemicalFormulaID;
     private     double m_dblAshContent;
     private     int m_iLevelID;
     private     double m_dblPrice;
     private     double m_dblDensity;
     private     bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

    public int iBiomassListID
    {
        get
        {
            return m_iBiomassListID;
        }
    }

    public    DateTime dtAdded
    {
        get
        {
            return m_dtAdded;
        }
        set
        {
            m_dtAdded = value;
        }
    }

    public    int iAddedBy
    {
        get
        {
            return m_iAddedBy;
        }
        set
        {
            m_iAddedBy = value;
        }
    }

    public    DateTime dtEdited
    {
        get
        {
            return m_dtEdited;
        }
        set
        {
            m_dtEdited = value;
        }
    }

    public    int iEditedBy
    {
        get
        {
            return m_iEditedBy;
        }
        set
        {
            m_iEditedBy = value;
        }
    }

    public    string strTitle
    {
        get
        {
            return m_strTitle;
        }
        set
        {
            m_strTitle = value;
        }
    }

    public    string strDescription
    {
        get
        {
            return m_strDescription;
        }
        set
        {
            m_strDescription = value;
        }
    }

    public    int iChemicalFormulaID
    {
        get
        {
            return m_iChemicalFormulaID;
        }
        set
        {
            m_iChemicalFormulaID = value;
        }
    }

    public    double dblAshContent
    {
        get
        {
            return m_dblAshContent;
        }
        set
        {
            m_dblAshContent = value;
        }
    }

    public    int iLevelID
    {
        get
        {
            return m_iLevelID;
        }
        set
        {
            m_iLevelID = value;
        }
    }

    public    double dblPrice
    {
        get
        {
            return m_dblPrice;
        }
        set
        {
            m_dblPrice = value;
        }
    }

    public    double dblDensity
    {
        get
        {
            return m_dblDensity;
        }
        set
        {
            m_dblDensity = value;
        }
    }

    public    bool bIsDeleted
    {
        get
        {
            return m_bIsDeleted;
        }
        set
        {
            m_bIsDeleted = value;
        }
    }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsBiomassLists()
    {
        m_iBiomassListID = 0;
    }

    public clsBiomassLists(int iBiomassListID)
    {
        m_iBiomassListID = iBiomassListID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iBiomassListID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@strTitle", m_strTitle),
                        new SqlParameter("@strDescription", m_strDescription),
                        new SqlParameter("@iChemicalFormulaID", m_iChemicalFormulaID),
                        new SqlParameter("@dblAshContent", m_dblAshContent),
                        new SqlParameter("@iLevelID", m_iLevelID),
                        new SqlParameter("@dblPrice", m_dblPrice),
                        new SqlParameter("@dblDensity", m_dblDensity)                  
                  };

                  //### Add
                  m_iBiomassListID = (int)clsDataAccess.ExecuteScalar("spBiomassListsInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iBiomassListID", m_iBiomassListID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@strTitle", m_strTitle),
                         new SqlParameter("@strDescription", m_strDescription),
                         new SqlParameter("@iChemicalFormulaID", m_iChemicalFormulaID),
                         new SqlParameter("@dblAshContent", m_dblAshContent),
                         new SqlParameter("@iLevelID", m_iLevelID),
                         new SqlParameter("@dblPrice", m_dblPrice),
                         new SqlParameter("@dblDensity", m_dblDensity)
          };
          //### Update
          clsDataAccess.Execute("spBiomassListsUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iBiomassListID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iBiomassListID", iBiomassListID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spBiomassListsDelete", sqlParameter);
        }

        public static DataTable GetBiomassListsList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spBiomassListsList", EmptySqlParameter);
        }
        public static DataTable GetBiomassListsList(string strFilterExpression, string strSortExpression)
        {
            DataView dvBiomassListsList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvBiomassListsList = clsDataAccess.GetDataView("spBiomassListsList", EmptySqlParameter);
            dvBiomassListsList.RowFilter = strFilterExpression;
            dvBiomassListsList.Sort = strSortExpression;

            return dvBiomassListsList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iBiomassListID", m_iBiomassListID)
                        };
                DataRow drRecord = clsDataAccess.GetRecord("spBiomassListsGetRecord", sqlParameter);

               m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
               m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

           if (drRecord["dtEdited"] != DBNull.Value)
               m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

           if (drRecord["iEditedBy"] != DBNull.Value)
               m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

                   m_strTitle = drRecord["strTitle"].ToString();
                   m_strDescription = drRecord["strDescription"].ToString();
               m_iChemicalFormulaID = Convert.ToInt32(drRecord["iChemicalFormulaID"]);
                   m_dblAshContent = Convert.ToDouble(drRecord["dblAshContent"]);
               m_iLevelID = Convert.ToInt32(drRecord["iLevelID"]);
                   m_dblPrice = Convert.ToDouble(drRecord["dblPrice"]);
                   m_dblDensity = Convert.ToDouble(drRecord["dblDensity"]);
                   m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}