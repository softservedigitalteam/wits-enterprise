
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsCompressionTypes
/// </summary>
public class clsCompressionTypes
{
    #region MEMBER VARIABLES

     private     int m_iCompressionTypeID;
     private     DateTime m_dtAdded;
     private     int m_iAddedBy;
     private     DateTime m_dtEdited;
     private     int m_iEditedBy;
     private     string m_strTitle;
     private     string m_strDescription;
     private     double m_dblPrice;
     private     bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

    public int iCompressionTypeID
    {
        get
        {
            return m_iCompressionTypeID;
        }
    }

    public    DateTime dtAdded
    {
        get
        {
            return m_dtAdded;
        }
        set
        {
            m_dtAdded = value;
        }
    }

    public    int iAddedBy
    {
        get
        {
            return m_iAddedBy;
        }
        set
        {
            m_iAddedBy = value;
        }
    }

    public    DateTime dtEdited
    {
        get
        {
            return m_dtEdited;
        }
        set
        {
            m_dtEdited = value;
        }
    }

    public    int iEditedBy
    {
        get
        {
            return m_iEditedBy;
        }
        set
        {
            m_iEditedBy = value;
        }
    }

    public    string strTitle
    {
        get
        {
            return m_strTitle;
        }
        set
        {
            m_strTitle = value;
        }
    }

    public    string strDescription
    {
        get
        {
            return m_strDescription;
        }
        set
        {
            m_strDescription = value;
        }
    }

    public    double dblPrice
    {
        get
        {
            return m_dblPrice;
        }
        set
        {
            m_dblPrice = value;
        }
    }

    public    bool bIsDeleted
    {
        get
        {
            return m_bIsDeleted;
        }
        set
        {
            m_bIsDeleted = value;
        }
    }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsCompressionTypes()
    {
        m_iCompressionTypeID = 0;
    }

    public clsCompressionTypes(int iCompressionTypeID)
    {
        m_iCompressionTypeID = iCompressionTypeID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iCompressionTypeID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@strTitle", m_strTitle),
                        new SqlParameter("@strDescription", m_strDescription),
                        new SqlParameter("@dblPrice", m_dblPrice)                  
                  };

                  //### Add
                  m_iCompressionTypeID = (int)clsDataAccess.ExecuteScalar("spCompressionTypesInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iCompressionTypeID", m_iCompressionTypeID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@strTitle", m_strTitle),
                         new SqlParameter("@strDescription", m_strDescription),
                         new SqlParameter("@dblPrice", m_dblPrice)
          };
          //### Update
          clsDataAccess.Execute("spCompressionTypesUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iCompressionTypeID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iCompressionTypeID", iCompressionTypeID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spCompressionTypesDelete", sqlParameter);
        }

        public static DataTable GetCompressionTypesList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spCompressionTypesList", EmptySqlParameter);
        }
        public static DataTable GetCompressionTypesList(string strFilterExpression, string strSortExpression)
        {
            DataView dvCompressionTypesList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvCompressionTypesList = clsDataAccess.GetDataView("spCompressionTypesList", EmptySqlParameter);
            dvCompressionTypesList.RowFilter = strFilterExpression;
            dvCompressionTypesList.Sort = strSortExpression;

            return dvCompressionTypesList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iCompressionTypeID", m_iCompressionTypeID)
                        };
                DataRow drRecord = clsDataAccess.GetRecord("spCompressionTypesGetRecord", sqlParameter);

               m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
               m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

           if (drRecord["dtEdited"] != DBNull.Value)
               m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

           if (drRecord["iEditedBy"] != DBNull.Value)
               m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

                   m_strTitle = drRecord["strTitle"].ToString();
                   m_strDescription = drRecord["strDescription"].ToString();
                   m_dblPrice = Convert.ToDouble(drRecord["dblPrice"]);
                   m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}