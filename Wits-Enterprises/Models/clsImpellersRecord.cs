
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsImpellers
/// </summary>
public class clsImpellers
{
    #region MEMBER VARIABLES

     private     int m_iImpellerID;
     private     DateTime m_dtAdded;
     private     int m_iAddedBy;
     private     DateTime m_dtEdited;
     private     int m_iEditedBy;
     private     string m_strTitle;
     private     string m_strDescription;
     private     double m_dblPrice;
     private     double m_dblNumberOfTanks;
     private     double m_dblLengthByDiameterOfReaction;
     private     double m_dblImpellerSpeed;
     private     double m_dblNumberOfImpellerBlades;
     private     double m_dblDiameterOfImpellerByDiameterOfReactor;
     private     double m_dblImpellerBladeWidthByReactorLength;
     private     double m_dblPowerPerUnitVolume;
     private     bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

    public int iImpellerID
    {
        get
        {
            return m_iImpellerID;
        }
    }

    public    DateTime dtAdded
    {
        get
        {
            return m_dtAdded;
        }
        set
        {
            m_dtAdded = value;
        }
    }

    public    int iAddedBy
    {
        get
        {
            return m_iAddedBy;
        }
        set
        {
            m_iAddedBy = value;
        }
    }

    public    DateTime dtEdited
    {
        get
        {
            return m_dtEdited;
        }
        set
        {
            m_dtEdited = value;
        }
    }

    public    int iEditedBy
    {
        get
        {
            return m_iEditedBy;
        }
        set
        {
            m_iEditedBy = value;
        }
    }

    public    string strTitle
    {
        get
        {
            return m_strTitle;
        }
        set
        {
            m_strTitle = value;
        }
    }

    public    string strDescription
    {
        get
        {
            return m_strDescription;
        }
        set
        {
            m_strDescription = value;
        }
    }

    public    double dblPrice
    {
        get
        {
            return m_dblPrice;
        }
        set
        {
            m_dblPrice = value;
        }
    }

    public    double dblNumberOfTanks
    {
        get
        {
            return m_dblNumberOfTanks;
        }
        set
        {
            m_dblNumberOfTanks = value;
        }
    }

    public    double dblLengthByDiameterOfReaction
    {
        get
        {
            return m_dblLengthByDiameterOfReaction;
        }
        set
        {
            m_dblLengthByDiameterOfReaction = value;
        }
    }

    public    double dblImpellerSpeed
    {
        get
        {
            return m_dblImpellerSpeed;
        }
        set
        {
            m_dblImpellerSpeed = value;
        }
    }

    public    double dblNumberOfImpellerBlades
    {
        get
        {
            return m_dblNumberOfImpellerBlades;
        }
        set
        {
            m_dblNumberOfImpellerBlades = value;
        }
    }

    public    double dblDiameterOfImpellerByDiameterOfReactor
    {
        get
        {
            return m_dblDiameterOfImpellerByDiameterOfReactor;
        }
        set
        {
            m_dblDiameterOfImpellerByDiameterOfReactor = value;
        }
    }

    public    double dblImpellerBladeWidthByReactorLength
    {
        get
        {
            return m_dblImpellerBladeWidthByReactorLength;
        }
        set
        {
            m_dblImpellerBladeWidthByReactorLength = value;
        }
    }

    public    double dblPowerPerUnitVolume
    {
        get
        {
            return m_dblPowerPerUnitVolume;
        }
        set
        {
            m_dblPowerPerUnitVolume = value;
        }
    }

    public    bool bIsDeleted
    {
        get
        {
            return m_bIsDeleted;
        }
        set
        {
            m_bIsDeleted = value;
        }
    }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsImpellers()
    {
        m_iImpellerID = 0;
    }

    public clsImpellers(int iImpellerID)
    {
        m_iImpellerID = iImpellerID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iImpellerID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@strTitle", m_strTitle),
                        new SqlParameter("@strDescription", m_strDescription),
                        new SqlParameter("@dblPrice", m_dblPrice),
                        new SqlParameter("@dblNumberOfTanks", m_dblNumberOfTanks),
                        new SqlParameter("@dblLengthByDiameterOfReaction", m_dblLengthByDiameterOfReaction),
                        new SqlParameter("@dblImpellerSpeed", m_dblImpellerSpeed),
                        new SqlParameter("@dblNumberOfImpellerBlades", m_dblNumberOfImpellerBlades),
                        new SqlParameter("@dblDiameterOfImpellerByDiameterOfReactor", m_dblDiameterOfImpellerByDiameterOfReactor),
                        new SqlParameter("@dblImpellerBladeWidthByReactorLength", m_dblImpellerBladeWidthByReactorLength),
                        new SqlParameter("@dblPowerPerUnitVolume", m_dblPowerPerUnitVolume)                  
                  };

                  //### Add
                  m_iImpellerID = (int)clsDataAccess.ExecuteScalar("spImpellersInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iImpellerID", m_iImpellerID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@strTitle", m_strTitle),
                         new SqlParameter("@strDescription", m_strDescription),
                         new SqlParameter("@dblPrice", m_dblPrice),
                         new SqlParameter("@dblNumberOfTanks", m_dblNumberOfTanks),
                         new SqlParameter("@dblLengthByDiameterOfReaction", m_dblLengthByDiameterOfReaction),
                         new SqlParameter("@dblImpellerSpeed", m_dblImpellerSpeed),
                         new SqlParameter("@dblNumberOfImpellerBlades", m_dblNumberOfImpellerBlades),
                         new SqlParameter("@dblDiameterOfImpellerByDiameterOfReactor", m_dblDiameterOfImpellerByDiameterOfReactor),
                         new SqlParameter("@dblImpellerBladeWidthByReactorLength", m_dblImpellerBladeWidthByReactorLength),
                         new SqlParameter("@dblPowerPerUnitVolume", m_dblPowerPerUnitVolume)
          };
          //### Update
          clsDataAccess.Execute("spImpellersUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iImpellerID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iImpellerID", iImpellerID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spImpellersDelete", sqlParameter);
        }

        public static DataTable GetImpellersList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spImpellersList", EmptySqlParameter);
        }
        public static DataTable GetImpellersList(string strFilterExpression, string strSortExpression)
        {
            DataView dvImpellersList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvImpellersList = clsDataAccess.GetDataView("spImpellersList", EmptySqlParameter);
            dvImpellersList.RowFilter = strFilterExpression;
            dvImpellersList.Sort = strSortExpression;

            return dvImpellersList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iImpellerID", m_iImpellerID)
                        };
                DataRow drRecord = clsDataAccess.GetRecord("spImpellersGetRecord", sqlParameter);

               m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
               m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

           if (drRecord["dtEdited"] != DBNull.Value)
               m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

           if (drRecord["iEditedBy"] != DBNull.Value)
               m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

                   m_strTitle = drRecord["strTitle"].ToString();
                   m_strDescription = drRecord["strDescription"].ToString();
                   m_dblPrice = Convert.ToDouble(drRecord["dblPrice"]);
                   m_dblNumberOfTanks = Convert.ToDouble(drRecord["dblNumberOfTanks"]);
                   m_dblLengthByDiameterOfReaction = Convert.ToDouble(drRecord["dblLengthByDiameterOfReaction"]);
                   m_dblImpellerSpeed = Convert.ToDouble(drRecord["dblImpellerSpeed"]);
                   m_dblNumberOfImpellerBlades = Convert.ToDouble(drRecord["dblNumberOfImpellerBlades"]);
                   m_dblDiameterOfImpellerByDiameterOfReactor = Convert.ToDouble(drRecord["dblDiameterOfImpellerByDiameterOfReactor"]);
                   m_dblImpellerBladeWidthByReactorLength = Convert.ToDouble(drRecord["dblImpellerBladeWidthByReactorLength"]);
                   m_dblPowerPerUnitVolume = Convert.ToDouble(drRecord["dblPowerPerUnitVolume"]);
                   m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}