
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsCoolants
/// </summary>
public class clsCoolants
{
    #region MEMBER VARIABLES

     private     int m_iCoolantID;
     private     DateTime m_dtAdded;
     private     int m_iAddedBy;
     private     DateTime m_dtEdited;
     private     int m_iEditedBy;
     private     string m_strTitle;
     private     string m_strDescription;
     private     double m_dblPrice;
     private     double m_dblOutletBiomassTemp;
     private     double m_dblEfficiency;
     private     bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

    public int iCoolantID
    {
        get
        {
            return m_iCoolantID;
        }
    }

    public    DateTime dtAdded
    {
        get
        {
            return m_dtAdded;
        }
        set
        {
            m_dtAdded = value;
        }
    }

    public    int iAddedBy
    {
        get
        {
            return m_iAddedBy;
        }
        set
        {
            m_iAddedBy = value;
        }
    }

    public    DateTime dtEdited
    {
        get
        {
            return m_dtEdited;
        }
        set
        {
            m_dtEdited = value;
        }
    }

    public    int iEditedBy
    {
        get
        {
            return m_iEditedBy;
        }
        set
        {
            m_iEditedBy = value;
        }
    }

    public    string strTitle
    {
        get
        {
            return m_strTitle;
        }
        set
        {
            m_strTitle = value;
        }
    }

    public    string strDescription
    {
        get
        {
            return m_strDescription;
        }
        set
        {
            m_strDescription = value;
        }
    }

    public    double dblPrice
    {
        get
        {
            return m_dblPrice;
        }
        set
        {
            m_dblPrice = value;
        }
    }

    public    double dblOutletBiomassTemp
    {
        get
        {
            return m_dblOutletBiomassTemp;
        }
        set
        {
            m_dblOutletBiomassTemp = value;
        }
    }

    public    double dblEfficiency
    {
        get
        {
            return m_dblEfficiency;
        }
        set
        {
            m_dblEfficiency = value;
        }
    }

    public    bool bIsDeleted
    {
        get
        {
            return m_bIsDeleted;
        }
        set
        {
            m_bIsDeleted = value;
        }
    }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsCoolants()
    {
        m_iCoolantID = 0;
    }

    public clsCoolants(int iCoolantID)
    {
        m_iCoolantID = iCoolantID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iCoolantID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@strTitle", m_strTitle),
                        new SqlParameter("@strDescription", m_strDescription),
                        new SqlParameter("@dblPrice", m_dblPrice),
                        new SqlParameter("@dblOutletBiomassTemp", m_dblOutletBiomassTemp),
                        new SqlParameter("@dblEfficiency", m_dblEfficiency)                  
                  };

                  //### Add
                  m_iCoolantID = (int)clsDataAccess.ExecuteScalar("spCoolantsInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iCoolantID", m_iCoolantID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@strTitle", m_strTitle),
                         new SqlParameter("@strDescription", m_strDescription),
                         new SqlParameter("@dblPrice", m_dblPrice),
                         new SqlParameter("@dblOutletBiomassTemp", m_dblOutletBiomassTemp),
                         new SqlParameter("@dblEfficiency", m_dblEfficiency)
          };
          //### Update
          clsDataAccess.Execute("spCoolantsUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iCoolantID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iCoolantID", iCoolantID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spCoolantsDelete", sqlParameter);
        }

        public static DataTable GetCoolantsList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spCoolantsList", EmptySqlParameter);
        }
        public static DataTable GetCoolantsList(string strFilterExpression, string strSortExpression)
        {
            DataView dvCoolantsList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvCoolantsList = clsDataAccess.GetDataView("spCoolantsList", EmptySqlParameter);
            dvCoolantsList.RowFilter = strFilterExpression;
            dvCoolantsList.Sort = strSortExpression;

            return dvCoolantsList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iCoolantID", m_iCoolantID)
                        };
                DataRow drRecord = clsDataAccess.GetRecord("spCoolantsGetRecord", sqlParameter);

               m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
               m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

           if (drRecord["dtEdited"] != DBNull.Value)
               m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

           if (drRecord["iEditedBy"] != DBNull.Value)
               m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

                   m_strTitle = drRecord["strTitle"].ToString();
                   m_strDescription = drRecord["strDescription"].ToString();
                   m_dblPrice = Convert.ToDouble(drRecord["dblPrice"]);
                   m_dblOutletBiomassTemp = Convert.ToDouble(drRecord["dblOutletBiomassTemp"]);
                   m_dblEfficiency = Convert.ToDouble(drRecord["dblEfficiency"]);
                   m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}