
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsCompressions
/// </summary>
public class clsCompressions
{
    #region MEMBER VARIABLES

     private     int m_iCompressionID;
     private     DateTime m_dtAdded;
     private     int m_iAddedBy;
     private     DateTime m_dtEdited;
     private     int m_iEditedBy;
     private     string m_strTitle;
     private     string m_strDescription;
     private     double m_dblPrice;
     private     double m_dblInitialPressure;
     private     double m_dblCompressedPressure;
     private     int m_iCompressionTypeID;
     private     bool m_bIsMultistaged;
     private     bool m_bIsCooling;
     private     double m_dblAntiFoam;
     private     bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

    public int iCompressionID
    {
        get
        {
            return m_iCompressionID;
        }
    }

    public    DateTime dtAdded
    {
        get
        {
            return m_dtAdded;
        }
        set
        {
            m_dtAdded = value;
        }
    }

    public    int iAddedBy
    {
        get
        {
            return m_iAddedBy;
        }
        set
        {
            m_iAddedBy = value;
        }
    }

    public    DateTime dtEdited
    {
        get
        {
            return m_dtEdited;
        }
        set
        {
            m_dtEdited = value;
        }
    }

    public    int iEditedBy
    {
        get
        {
            return m_iEditedBy;
        }
        set
        {
            m_iEditedBy = value;
        }
    }

    public    string strTitle
    {
        get
        {
            return m_strTitle;
        }
        set
        {
            m_strTitle = value;
        }
    }

    public    string strDescription
    {
        get
        {
            return m_strDescription;
        }
        set
        {
            m_strDescription = value;
        }
    }

    public    double dblPrice
    {
        get
        {
            return m_dblPrice;
        }
        set
        {
            m_dblPrice = value;
        }
    }

    public    double dblInitialPressure
    {
        get
        {
            return m_dblInitialPressure;
        }
        set
        {
            m_dblInitialPressure = value;
        }
    }

    public    double dblCompressedPressure
    {
        get
        {
            return m_dblCompressedPressure;
        }
        set
        {
            m_dblCompressedPressure = value;
        }
    }

    public    int iCompressionTypeID
    {
        get
        {
            return m_iCompressionTypeID;
        }
        set
        {
            m_iCompressionTypeID = value;
        }
    }

    public    bool bIsMultistaged
    {
        get
        {
            return m_bIsMultistaged;
        }
        set
        {
            m_bIsMultistaged = value;
        }
    }

    public    bool bIsCooling
    {
        get
        {
            return m_bIsCooling;
        }
        set
        {
            m_bIsCooling = value;
        }
    }

    public    double dblAntiFoam
    {
        get
        {
            return m_dblAntiFoam;
        }
        set
        {
            m_dblAntiFoam = value;
        }
    }

    public    bool bIsDeleted
    {
        get
        {
            return m_bIsDeleted;
        }
        set
        {
            m_bIsDeleted = value;
        }
    }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsCompressions()
    {
        m_iCompressionID = 0;
    }

    public clsCompressions(int iCompressionID)
    {
        m_iCompressionID = iCompressionID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iCompressionID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@strTitle", m_strTitle),
                        new SqlParameter("@strDescription", m_strDescription),
                        new SqlParameter("@dblPrice", m_dblPrice),
                        new SqlParameter("@dblInitialPressure", m_dblInitialPressure),
                        new SqlParameter("@dblCompressedPressure", m_dblCompressedPressure),
                        new SqlParameter("@iCompressionTypeID", m_iCompressionTypeID),
                        new SqlParameter("@bIsMultistaged", m_bIsMultistaged),
                        new SqlParameter("@bIsCooling", m_bIsCooling),
                        new SqlParameter("@dblAntiFoam", m_dblAntiFoam)                  
                  };

                  //### Add
                  m_iCompressionID = (int)clsDataAccess.ExecuteScalar("spCompressionsInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iCompressionID", m_iCompressionID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@strTitle", m_strTitle),
                         new SqlParameter("@strDescription", m_strDescription),
                         new SqlParameter("@dblPrice", m_dblPrice),
                         new SqlParameter("@dblInitialPressure", m_dblInitialPressure),
                         new SqlParameter("@dblCompressedPressure", m_dblCompressedPressure),
                         new SqlParameter("@iCompressionTypeID", m_iCompressionTypeID),
                         new SqlParameter("@bIsMultistaged", m_bIsMultistaged),
                         new SqlParameter("@bIsCooling", m_bIsCooling),
                         new SqlParameter("@dblAntiFoam", m_dblAntiFoam)
          };
          //### Update
          clsDataAccess.Execute("spCompressionsUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iCompressionID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iCompressionID", iCompressionID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spCompressionsDelete", sqlParameter);
        }

        public static DataTable GetCompressionsList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spCompressionsList", EmptySqlParameter);
        }
        public static DataTable GetCompressionsList(string strFilterExpression, string strSortExpression)
        {
            DataView dvCompressionsList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvCompressionsList = clsDataAccess.GetDataView("spCompressionsList", EmptySqlParameter);
            dvCompressionsList.RowFilter = strFilterExpression;
            dvCompressionsList.Sort = strSortExpression;

            return dvCompressionsList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iCompressionID", m_iCompressionID)
                        };
                DataRow drRecord = clsDataAccess.GetRecord("spCompressionsGetRecord", sqlParameter);

               m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
               m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

           if (drRecord["dtEdited"] != DBNull.Value)
               m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

           if (drRecord["iEditedBy"] != DBNull.Value)
               m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

                   m_strTitle = drRecord["strTitle"].ToString();
                   m_strDescription = drRecord["strDescription"].ToString();
                   m_dblPrice = Convert.ToDouble(drRecord["dblPrice"]);
                   m_dblInitialPressure = Convert.ToDouble(drRecord["dblInitialPressure"]);
                   m_dblCompressedPressure = Convert.ToDouble(drRecord["dblCompressedPressure"]);
               m_iCompressionTypeID = Convert.ToInt32(drRecord["iCompressionTypeID"]);
                   m_bIsMultistaged = Convert.ToBoolean(drRecord["bIsMultistaged"]);
                   m_bIsCooling = Convert.ToBoolean(drRecord["bIsCooling"]);
                   m_dblAntiFoam = Convert.ToDouble(drRecord["dblAntiFoam"]);
                   m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}