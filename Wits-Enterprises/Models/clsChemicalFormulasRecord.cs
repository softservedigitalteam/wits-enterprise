
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsChemicalFormulas
/// </summary>
public class clsChemicalFormulas
{
    #region MEMBER VARIABLES

     private     int m_iChemicalFormulaID;
     private     DateTime m_dtAdded;
     private     int m_iAddedBy;
     private     DateTime m_dtEdited;
     private     int m_iEditedBy;
     private     string m_strTitle;
     private     string m_strPhaseConsistency;
     private     double m_dblCarbon;
     private     double m_dblHydrogenH;
     private     double m_dblOxygenO;
     private     double m_dblNitrogenN;
     private     double m_dblPhosphorusP;
     private     double m_dblSulfurS;
     private     double m_dblSodiumNa;
     private     double m_dblChlorineCl;
     private     double m_dblPotassiumK;
     private     double m_dblIronFe;
     private     double m_dblMagnesiumMg;
     private     double m_dblCalciumCa;
     private     double m_dblSiliconSi;
     private     double m_dblAluminiumAl;
     private     double m_dblCopperCu;
     private     bool m_bIsAnEstimate;
     private     bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

    public int iChemicalFormulaID
    {
        get
        {
            return m_iChemicalFormulaID;
        }
    }

    public    DateTime dtAdded
    {
        get
        {
            return m_dtAdded;
        }
        set
        {
            m_dtAdded = value;
        }
    }

    public    int iAddedBy
    {
        get
        {
            return m_iAddedBy;
        }
        set
        {
            m_iAddedBy = value;
        }
    }

    public    DateTime dtEdited
    {
        get
        {
            return m_dtEdited;
        }
        set
        {
            m_dtEdited = value;
        }
    }

    public    int iEditedBy
    {
        get
        {
            return m_iEditedBy;
        }
        set
        {
            m_iEditedBy = value;
        }
    }

    public    string strTitle
    {
        get
        {
            return m_strTitle;
        }
        set
        {
            m_strTitle = value;
        }
    }

    public    string strPhaseConsistency
    {
        get
        {
            return m_strPhaseConsistency;
        }
        set
        {
            m_strPhaseConsistency = value;
        }
    }

    public    double dblCarbon
    {
        get
        {
            return m_dblCarbon;
        }
        set
        {
            m_dblCarbon = value;
        }
    }

    public    double dblHydrogenH
    {
        get
        {
            return m_dblHydrogenH;
        }
        set
        {
            m_dblHydrogenH = value;
        }
    }

    public    double dblOxygenO
    {
        get
        {
            return m_dblOxygenO;
        }
        set
        {
            m_dblOxygenO = value;
        }
    }

    public    double dblNitrogenN
    {
        get
        {
            return m_dblNitrogenN;
        }
        set
        {
            m_dblNitrogenN = value;
        }
    }

    public    double dblPhosphorusP
    {
        get
        {
            return m_dblPhosphorusP;
        }
        set
        {
            m_dblPhosphorusP = value;
        }
    }

    public    double dblSulfurS
    {
        get
        {
            return m_dblSulfurS;
        }
        set
        {
            m_dblSulfurS = value;
        }
    }

    public    double dblSodiumNa
    {
        get
        {
            return m_dblSodiumNa;
        }
        set
        {
            m_dblSodiumNa = value;
        }
    }

    public    double dblChlorineCl
    {
        get
        {
            return m_dblChlorineCl;
        }
        set
        {
            m_dblChlorineCl = value;
        }
    }

    public    double dblPotassiumK
    {
        get
        {
            return m_dblPotassiumK;
        }
        set
        {
            m_dblPotassiumK = value;
        }
    }

    public    double dblIronFe
    {
        get
        {
            return m_dblIronFe;
        }
        set
        {
            m_dblIronFe = value;
        }
    }

    public    double dblMagnesiumMg
    {
        get
        {
            return m_dblMagnesiumMg;
        }
        set
        {
            m_dblMagnesiumMg = value;
        }
    }

    public    double dblCalciumCa
    {
        get
        {
            return m_dblCalciumCa;
        }
        set
        {
            m_dblCalciumCa = value;
        }
    }

    public    double dblSiliconSi
    {
        get
        {
            return m_dblSiliconSi;
        }
        set
        {
            m_dblSiliconSi = value;
        }
    }

    public    double dblAluminiumAl
    {
        get
        {
            return m_dblAluminiumAl;
        }
        set
        {
            m_dblAluminiumAl = value;
        }
    }

    public    double dblCopperCu
    {
        get
        {
            return m_dblCopperCu;
        }
        set
        {
            m_dblCopperCu = value;
        }
    }

    public    bool bIsAnEstimate
    {
        get
        {
            return m_bIsAnEstimate;
        }
        set
        {
            m_bIsAnEstimate = value;
        }
    }

    public    bool bIsDeleted
    {
        get
        {
            return m_bIsDeleted;
        }
        set
        {
            m_bIsDeleted = value;
        }
    }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsChemicalFormulas()
    {
        m_iChemicalFormulaID = 0;
    }

    public clsChemicalFormulas(int iChemicalFormulaID)
    {
        m_iChemicalFormulaID = iChemicalFormulaID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iChemicalFormulaID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@strTitle", m_strTitle),
                        new SqlParameter("@strPhaseConsistency", m_strPhaseConsistency),
                        new SqlParameter("@dblCarbon", m_dblCarbon),
                        new SqlParameter("@dblHydrogenH", m_dblHydrogenH),
                        new SqlParameter("@dblOxygenO", m_dblOxygenO),
                        new SqlParameter("@dblNitrogenN", m_dblNitrogenN),
                        new SqlParameter("@dblPhosphorusP", m_dblPhosphorusP),
                        new SqlParameter("@dblSulfurS", m_dblSulfurS),
                        new SqlParameter("@dblSodiumNa", m_dblSodiumNa),
                        new SqlParameter("@dblChlorineCl", m_dblChlorineCl),
                        new SqlParameter("@dblPotassiumK", m_dblPotassiumK),
                        new SqlParameter("@dblIronFe", m_dblIronFe),
                        new SqlParameter("@dblMagnesiumMg", m_dblMagnesiumMg),
                        new SqlParameter("@dblCalciumCa", m_dblCalciumCa),
                        new SqlParameter("@dblSiliconSi", m_dblSiliconSi),
                        new SqlParameter("@dblAluminiumAl", m_dblAluminiumAl),
                        new SqlParameter("@dblCopperCu", m_dblCopperCu),
                        new SqlParameter("@bIsAnEstimate", m_bIsAnEstimate)                  
                  };

                  //### Add
                  m_iChemicalFormulaID = (int)clsDataAccess.ExecuteScalar("spChemicalFormulasInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iChemicalFormulaID", m_iChemicalFormulaID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@strTitle", m_strTitle),
                         new SqlParameter("@strPhaseConsistency", m_strPhaseConsistency),
                         new SqlParameter("@dblCarbon", m_dblCarbon),
                         new SqlParameter("@dblHydrogenH", m_dblHydrogenH),
                         new SqlParameter("@dblOxygenO", m_dblOxygenO),
                         new SqlParameter("@dblNitrogenN", m_dblNitrogenN),
                         new SqlParameter("@dblPhosphorusP", m_dblPhosphorusP),
                         new SqlParameter("@dblSulfurS", m_dblSulfurS),
                         new SqlParameter("@dblSodiumNa", m_dblSodiumNa),
                         new SqlParameter("@dblChlorineCl", m_dblChlorineCl),
                         new SqlParameter("@dblPotassiumK", m_dblPotassiumK),
                         new SqlParameter("@dblIronFe", m_dblIronFe),
                         new SqlParameter("@dblMagnesiumMg", m_dblMagnesiumMg),
                         new SqlParameter("@dblCalciumCa", m_dblCalciumCa),
                         new SqlParameter("@dblSiliconSi", m_dblSiliconSi),
                         new SqlParameter("@dblAluminiumAl", m_dblAluminiumAl),
                         new SqlParameter("@dblCopperCu", m_dblCopperCu),
                         new SqlParameter("@bIsAnEstimate", m_bIsAnEstimate)
          };
          //### Update
          clsDataAccess.Execute("spChemicalFormulasUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iChemicalFormulaID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iChemicalFormulaID", iChemicalFormulaID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spChemicalFormulasDelete", sqlParameter);
        }

        public static DataTable GetChemicalFormulasList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spChemicalFormulasList", EmptySqlParameter);
        }
        public static DataTable GetChemicalFormulasList(string strFilterExpression, string strSortExpression)
        {
            DataView dvChemicalFormulasList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvChemicalFormulasList = clsDataAccess.GetDataView("spChemicalFormulasList", EmptySqlParameter);
            dvChemicalFormulasList.RowFilter = strFilterExpression;
            dvChemicalFormulasList.Sort = strSortExpression;

            return dvChemicalFormulasList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iChemicalFormulaID", m_iChemicalFormulaID)
                        };
                DataRow drRecord = clsDataAccess.GetRecord("spChemicalFormulasGetRecord", sqlParameter);

               m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
               m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

           if (drRecord["dtEdited"] != DBNull.Value)
               m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

           if (drRecord["iEditedBy"] != DBNull.Value)
               m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

                   m_strTitle = drRecord["strTitle"].ToString();
                   m_strPhaseConsistency = drRecord["strPhaseConsistency"].ToString();
                   m_dblCarbon = Convert.ToDouble(drRecord["dblCarbon"]);
                   m_dblHydrogenH = Convert.ToDouble(drRecord["dblHydrogenH"]);
                   m_dblOxygenO = Convert.ToDouble(drRecord["dblOxygenO"]);
                   m_dblNitrogenN = Convert.ToDouble(drRecord["dblNitrogenN"]);
                   m_dblPhosphorusP = Convert.ToDouble(drRecord["dblPhosphorusP"]);
                   m_dblSulfurS = Convert.ToDouble(drRecord["dblSulfurS"]);
                   m_dblSodiumNa = Convert.ToDouble(drRecord["dblSodiumNa"]);
                   m_dblChlorineCl = Convert.ToDouble(drRecord["dblChlorineCl"]);
                   m_dblPotassiumK = Convert.ToDouble(drRecord["dblPotassiumK"]);
                   m_dblIronFe = Convert.ToDouble(drRecord["dblIronFe"]);
                   m_dblMagnesiumMg = Convert.ToDouble(drRecord["dblMagnesiumMg"]);
                   m_dblCalciumCa = Convert.ToDouble(drRecord["dblCalciumCa"]);
                   m_dblSiliconSi = Convert.ToDouble(drRecord["dblSiliconSi"]);
                   m_dblAluminiumAl = Convert.ToDouble(drRecord["dblAluminiumAl"]);
                   m_dblCopperCu = Convert.ToDouble(drRecord["dblCopperCu"]);
                   m_bIsAnEstimate = Convert.ToBoolean(drRecord["bIsAnEstimate"]);
                   m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}