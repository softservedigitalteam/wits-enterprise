
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsSubscriptions
/// </summary>
public class clsSubscriptions
{
    #region MEMBER VARIABLES

     private     int m_iSubscriptionID;
     private     DateTime m_dtAdded;
     private     int m_iAddedBy;
     private     DateTime m_dtEdited;
     private     int m_iEditedBy;
     private     string m_strTitle;
     private     int m_iSystemUserID;
     private     int m_iSubscriptionTypeID;
     private     bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

    public int iSubscriptionID
    {
        get
        {
            return m_iSubscriptionID;
        }
    }

    public    DateTime dtAdded
    {
        get
        {
            return m_dtAdded;
        }
        set
        {
            m_dtAdded = value;
        }
    }

    public    int iAddedBy
    {
        get
        {
            return m_iAddedBy;
        }
        set
        {
            m_iAddedBy = value;
        }
    }

    public    DateTime dtEdited
    {
        get
        {
            return m_dtEdited;
        }
        set
        {
            m_dtEdited = value;
        }
    }

    public    int iEditedBy
    {
        get
        {
            return m_iEditedBy;
        }
        set
        {
            m_iEditedBy = value;
        }
    }

    public    string strTitle
    {
        get
        {
            return m_strTitle;
        }
        set
        {
            m_strTitle = value;
        }
    }

    public    int iSystemUserID
    {
        get
        {
            return m_iSystemUserID;
        }
        set
        {
            m_iSystemUserID = value;
        }
    }

    public    int iSubscriptionTypeID
    {
        get
        {
            return m_iSubscriptionTypeID;
        }
        set
        {
            m_iSubscriptionTypeID = value;
        }
    }

    public    bool bIsDeleted
    {
        get
        {
            return m_bIsDeleted;
        }
        set
        {
            m_bIsDeleted = value;
        }
    }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsSubscriptions()
    {
        m_iSubscriptionID = 0;
    }

    public clsSubscriptions(int iSubscriptionID)
    {
        m_iSubscriptionID = iSubscriptionID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iSubscriptionID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@strTitle", m_strTitle),
                        new SqlParameter("@iSystemUserID", m_iSystemUserID),
                        new SqlParameter("@iSubscriptionTypeID", m_iSubscriptionTypeID)                  
                  };

                  //### Add
                  m_iSubscriptionID = (int)clsDataAccess.ExecuteScalar("spSubscriptionsInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iSubscriptionID", m_iSubscriptionID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@strTitle", m_strTitle),
                         new SqlParameter("@iSystemUserID", m_iSystemUserID),
                         new SqlParameter("@iSubscriptionTypeID", m_iSubscriptionTypeID)
          };
          //### Update
          clsDataAccess.Execute("spSubscriptionsUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iSubscriptionID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iSubscriptionID", iSubscriptionID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spSubscriptionsDelete", sqlParameter);
        }

        public static DataTable GetSubscriptionsList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spSubscriptionsList", EmptySqlParameter);
        }
        public static DataTable GetSubscriptionsList(string strFilterExpression, string strSortExpression)
        {
            DataView dvSubscriptionsList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvSubscriptionsList = clsDataAccess.GetDataView("spSubscriptionsList", EmptySqlParameter);
            dvSubscriptionsList.RowFilter = strFilterExpression;
            dvSubscriptionsList.Sort = strSortExpression;

            return dvSubscriptionsList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iSubscriptionID", m_iSubscriptionID)
                        };
                DataRow drRecord = clsDataAccess.GetRecord("spSubscriptionsGetRecord", sqlParameter);

               m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
               m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

           if (drRecord["dtEdited"] != DBNull.Value)
               m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

           if (drRecord["iEditedBy"] != DBNull.Value)
               m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

                   m_strTitle = drRecord["strTitle"].ToString();
               m_iSystemUserID = Convert.ToInt32(drRecord["iSystemUserID"]);
               m_iSubscriptionTypeID = Convert.ToInt32(drRecord["iSubscriptionTypeID"]);
                   m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}