
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsTestCaseHistoryLists
/// </summary>
public class clsTestCaseHistoryLists
{
    #region MEMBER VARIABLES

     private     int m_iTestCaseHistoryListID;
     private     DateTime m_dtAdded;
     private     int m_iAddedBy;
     private     DateTime m_dtEdited;
     private     int m_iEditedBy;
     private     string m_strTitle;
     private     bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

    public int iTestCaseHistoryListID
    {
        get
        {
            return m_iTestCaseHistoryListID;
        }
    }

    public    DateTime dtAdded
    {
        get
        {
            return m_dtAdded;
        }
        set
        {
            m_dtAdded = value;
        }
    }

    public    int iAddedBy
    {
        get
        {
            return m_iAddedBy;
        }
        set
        {
            m_iAddedBy = value;
        }
    }

    public    DateTime dtEdited
    {
        get
        {
            return m_dtEdited;
        }
        set
        {
            m_dtEdited = value;
        }
    }

    public    int iEditedBy
    {
        get
        {
            return m_iEditedBy;
        }
        set
        {
            m_iEditedBy = value;
        }
    }

    public    string strTitle
    {
        get
        {
            return m_strTitle;
        }
        set
        {
            m_strTitle = value;
        }
    }

    public    bool bIsDeleted
    {
        get
        {
            return m_bIsDeleted;
        }
        set
        {
            m_bIsDeleted = value;
        }
    }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsTestCaseHistoryLists()
    {
        m_iTestCaseHistoryListID = 0;
    }

    public clsTestCaseHistoryLists(int iTestCaseHistoryListID)
    {
        m_iTestCaseHistoryListID = iTestCaseHistoryListID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iTestCaseHistoryListID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@strTitle", m_strTitle)                  
                  };

                  //### Add
                  m_iTestCaseHistoryListID = (int)clsDataAccess.ExecuteScalar("spTestCaseHistoryListsInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iTestCaseHistoryListID", m_iTestCaseHistoryListID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@strTitle", m_strTitle)
          };
          //### Update
          clsDataAccess.Execute("spTestCaseHistoryListsUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iTestCaseHistoryListID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iTestCaseHistoryListID", iTestCaseHistoryListID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spTestCaseHistoryListsDelete", sqlParameter);
        }

        public static DataTable GetTestCaseHistoryListsList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spTestCaseHistoryListsList", EmptySqlParameter);
        }
        public static DataTable GetTestCaseHistoryListsList(string strFilterExpression, string strSortExpression)
        {
            DataView dvTestCaseHistoryListsList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvTestCaseHistoryListsList = clsDataAccess.GetDataView("spTestCaseHistoryListsList", EmptySqlParameter);
            dvTestCaseHistoryListsList.RowFilter = strFilterExpression;
            dvTestCaseHistoryListsList.Sort = strSortExpression;

            return dvTestCaseHistoryListsList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iTestCaseHistoryListID", m_iTestCaseHistoryListID)
                        };
                DataRow drRecord = clsDataAccess.GetRecord("spTestCaseHistoryListsGetRecord", sqlParameter);

               m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
               m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

           if (drRecord["dtEdited"] != DBNull.Value)
               m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

           if (drRecord["iEditedBy"] != DBNull.Value)
               m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

                   m_strTitle = drRecord["strTitle"].ToString();
                   m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}