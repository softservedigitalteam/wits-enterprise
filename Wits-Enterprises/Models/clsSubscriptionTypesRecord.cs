
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsSubscriptionTypes
/// </summary>
public class clsSubscriptionTypes
{
    #region MEMBER VARIABLES

     private     int m_iSubscriptionTypeID;
     private     DateTime m_dtAdded;
     private     int m_iAddedBy;
     private     DateTime m_dtEdited;
     private     int m_iEditedBy;
     private     string m_strTitle;
     private     double m_dblMonthlyCost;
     private     bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

    public int iSubscriptionTypeID
    {
        get
        {
            return m_iSubscriptionTypeID;
        }
    }

    public    DateTime dtAdded
    {
        get
        {
            return m_dtAdded;
        }
        set
        {
            m_dtAdded = value;
        }
    }

    public    int iAddedBy
    {
        get
        {
            return m_iAddedBy;
        }
        set
        {
            m_iAddedBy = value;
        }
    }

    public    DateTime dtEdited
    {
        get
        {
            return m_dtEdited;
        }
        set
        {
            m_dtEdited = value;
        }
    }

    public    int iEditedBy
    {
        get
        {
            return m_iEditedBy;
        }
        set
        {
            m_iEditedBy = value;
        }
    }

    public    string strTitle
    {
        get
        {
            return m_strTitle;
        }
        set
        {
            m_strTitle = value;
        }
    }

    public    double dblMonthlyCost
    {
        get
        {
            return m_dblMonthlyCost;
        }
        set
        {
            m_dblMonthlyCost = value;
        }
    }

    public    bool bIsDeleted
    {
        get
        {
            return m_bIsDeleted;
        }
        set
        {
            m_bIsDeleted = value;
        }
    }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsSubscriptionTypes()
    {
        m_iSubscriptionTypeID = 0;
    }

    public clsSubscriptionTypes(int iSubscriptionTypeID)
    {
        m_iSubscriptionTypeID = iSubscriptionTypeID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iSubscriptionTypeID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@strTitle", m_strTitle),
                        new SqlParameter("@dblMonthlyCost", m_dblMonthlyCost)                  
                  };

                  //### Add
                  m_iSubscriptionTypeID = (int)clsDataAccess.ExecuteScalar("spSubscriptionTypesInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iSubscriptionTypeID", m_iSubscriptionTypeID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@strTitle", m_strTitle),
                         new SqlParameter("@dblMonthlyCost", m_dblMonthlyCost)
          };
          //### Update
          clsDataAccess.Execute("spSubscriptionTypesUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iSubscriptionTypeID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iSubscriptionTypeID", iSubscriptionTypeID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spSubscriptionTypesDelete", sqlParameter);
        }

        public static DataTable GetSubscriptionTypesList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spSubscriptionTypesList", EmptySqlParameter);
        }
        public static DataTable GetSubscriptionTypesList(string strFilterExpression, string strSortExpression)
        {
            DataView dvSubscriptionTypesList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvSubscriptionTypesList = clsDataAccess.GetDataView("spSubscriptionTypesList", EmptySqlParameter);
            dvSubscriptionTypesList.RowFilter = strFilterExpression;
            dvSubscriptionTypesList.Sort = strSortExpression;

            return dvSubscriptionTypesList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iSubscriptionTypeID", m_iSubscriptionTypeID)
                        };
                DataRow drRecord = clsDataAccess.GetRecord("spSubscriptionTypesGetRecord", sqlParameter);

               m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
               m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

           if (drRecord["dtEdited"] != DBNull.Value)
               m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

           if (drRecord["iEditedBy"] != DBNull.Value)
               m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

                   m_strTitle = drRecord["strTitle"].ToString();
                   m_dblMonthlyCost = Convert.ToDouble(drRecord["dblMonthlyCost"]);
                   m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}