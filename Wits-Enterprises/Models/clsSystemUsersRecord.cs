
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsSystemUsers
/// </summary>
public class clsSystemUsers
{
    #region MEMBER VARIABLES

     private     int m_iSystemUserID;
     private     DateTime m_dtAdded;
     private     int m_iAddedBy;
     private     DateTime m_dtEdited;
     private     int m_iEditedBy;
     private     string m_strFirstName;
     private     string m_strLastName;
     private     string m_strContactNumber;
     private     string m_strEmailAddress;
     private     string m_strPassword;
     private     bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

    public int iSystemUserID
    {
        get
        {
            return m_iSystemUserID;
        }
    }

    public    DateTime dtAdded
    {
        get
        {
            return m_dtAdded;
        }
        set
        {
            m_dtAdded = value;
        }
    }

    public    int iAddedBy
    {
        get
        {
            return m_iAddedBy;
        }
        set
        {
            m_iAddedBy = value;
        }
    }

    public    DateTime dtEdited
    {
        get
        {
            return m_dtEdited;
        }
        set
        {
            m_dtEdited = value;
        }
    }

    public    int iEditedBy
    {
        get
        {
            return m_iEditedBy;
        }
        set
        {
            m_iEditedBy = value;
        }
    }

    public    string strFirstName
    {
        get
        {
            return m_strFirstName;
        }
        set
        {
            m_strFirstName = value;
        }
    }

    public    string strLastName
    {
        get
        {
            return m_strLastName;
        }
        set
        {
            m_strLastName = value;
        }
    }

    public    string strContactNumber
    {
        get
        {
            return m_strContactNumber;
        }
        set
        {
            m_strContactNumber = value;
        }
    }

    public    string strEmailAddress
    {
        get
        {
            return m_strEmailAddress;
        }
        set
        {
            m_strEmailAddress = value;
        }
    }

    public    string strPassword
    {
        get
        {
            return m_strPassword;
        }
        set
        {
            m_strPassword = value;
        }
    }

    public    bool bIsDeleted
    {
        get
        {
            return m_bIsDeleted;
        }
        set
        {
            m_bIsDeleted = value;
        }
    }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsSystemUsers()
    {
        m_iSystemUserID = 0;
    }

    public clsSystemUsers(int iSystemUserID)
    {
        m_iSystemUserID = iSystemUserID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iSystemUserID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@strFirstName", m_strFirstName),
                        new SqlParameter("@strLastName", m_strLastName),
                        new SqlParameter("@strContactNumber", m_strContactNumber),
                        new SqlParameter("@strEmailAddress", m_strEmailAddress),
                        new SqlParameter("@strPassword", m_strPassword)                  
                  };

                  //### Add
                  m_iSystemUserID = (int)clsDataAccess.ExecuteScalar("spSystemUsersInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iSystemUserID", m_iSystemUserID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@strFirstName", m_strFirstName),
                         new SqlParameter("@strLastName", m_strLastName),
                         new SqlParameter("@strContactNumber", m_strContactNumber),
                         new SqlParameter("@strEmailAddress", m_strEmailAddress),
                         new SqlParameter("@strPassword", m_strPassword)
          };
          //### Update
          clsDataAccess.Execute("spSystemUsersUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iSystemUserID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iSystemUserID", iSystemUserID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spSystemUsersDelete", sqlParameter);
        }

        public static DataTable GetSystemUsersList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spSystemUsersList", EmptySqlParameter);
        }
        public static DataTable GetSystemUsersList(string strFilterExpression, string strSortExpression)
        {
            DataView dvSystemUsersList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvSystemUsersList = clsDataAccess.GetDataView("spSystemUsersList", EmptySqlParameter);
            dvSystemUsersList.RowFilter = strFilterExpression;
            dvSystemUsersList.Sort = strSortExpression;

            return dvSystemUsersList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iSystemUserID", m_iSystemUserID)
                        };
                DataRow drRecord = clsDataAccess.GetRecord("spSystemUsersGetRecord", sqlParameter);

               m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
               m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

           if (drRecord["dtEdited"] != DBNull.Value)
               m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

           if (drRecord["iEditedBy"] != DBNull.Value)
               m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

                   m_strFirstName = drRecord["strFirstName"].ToString();
                   m_strLastName = drRecord["strLastName"].ToString();
                   m_strContactNumber = drRecord["strContactNumber"].ToString();
                   m_strEmailAddress = drRecord["strEmailAddress"].ToString();
                   m_strPassword = drRecord["strPassword"].ToString();
                   m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}