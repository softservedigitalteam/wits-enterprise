USE [master]
GO
/****** Object:  Database [CMS]    Script Date: 2016-06-09 10:34:56 AM ******/
CREATE DATABASE [CMS]
 CONTAINMENT = NONE

ALTER DATABASE [CMS] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [CMS].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [CMS] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [CMS] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [CMS] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [CMS] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [CMS] SET ARITHABORT OFF 
GO
ALTER DATABASE [CMS] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [CMS] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [CMS] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [CMS] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [CMS] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [CMS] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [CMS] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [CMS] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [CMS] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [CMS] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [CMS] SET  DISABLE_BROKER 
GO
ALTER DATABASE [CMS] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [CMS] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [CMS] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [CMS] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [CMS] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [CMS] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [CMS] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [CMS] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [CMS] SET  MULTI_USER 
GO
ALTER DATABASE [CMS] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [CMS] SET DB_CHAINING OFF 
GO
ALTER DATABASE [CMS] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [CMS] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [CMS]
GO
/****** Object:  User [FormaxUser]    Script Date: 2016-06-09 10:34:56 AM ******/
CREATE USER [FormaxUser] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [DruffUser]    Script Date: 2016-06-09 10:34:56 AM ******/
CREATE USER [DruffUser] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [Druff]    Script Date: 2016-06-09 10:34:56 AM ******/
CREATE USER [Druff] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [CDUser]    Script Date: 2016-06-09 10:34:56 AM ******/
CREATE USER [CDUser] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[CDUser]
GO
/****** Object:  User [CBUser]    Script Date: 2016-06-09 10:34:56 AM ******/
CREATE USER [CBUser] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[CBUser]
GO
ALTER ROLE [db_owner] ADD MEMBER [FormaxUser]
GO
ALTER ROLE [db_owner] ADD MEMBER [Druff]
GO
ALTER ROLE [db_owner] ADD MEMBER [CDUser]
GO
ALTER ROLE [db_owner] ADD MEMBER [CBUser]
GO
/****** Object:  Schema [CBUser]    Script Date: 2016-06-09 10:34:56 AM ******/
CREATE SCHEMA [CBUser]
GO
/****** Object:  Schema [CDUser]    Script Date: 2016-06-09 10:34:56 AM ******/
CREATE SCHEMA [CDUser]
GO
/****** Object:  StoredProcedure [dbo].[spCategoriesDelete]    Script Date: 2016-06-09 10:34:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<System Generated>
-- =============================================
Create PROCEDURE [dbo].[spCategoriesDelete] (@iCategoryID int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE tblCategories SET bIsDeleted = 1 WHERE iCategoryID = @iCategoryID

END

SET ANSI_NULLS ON




GO
/****** Object:  StoredProcedure [dbo].[spCategoriesFeaturedList]    Script Date: 2016-06-09 10:34:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<System Generated>
-- =============================================
CREATE PROCEDURE [dbo].[spCategoriesFeaturedList]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM tblCategories
    WHERE bIsDeleted = 0 AND bIsFeatured = 1

END





GO
/****** Object:  StoredProcedure [dbo].[spCategoriesGetRecord]    Script Date: 2016-06-09 10:34:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<System Generated>
-- =============================================
CREATE PROCEDURE [dbo].[spCategoriesGetRecord] (@iCategoryID int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT TOP(1) * FROM tblCategories  WHERE iCategoryID = @iCategoryID AND bIsDeleted = 0
END

SET ANSI_NULLS ON




GO
/****** Object:  StoredProcedure [dbo].[spCategoriesInsert]    Script Date: 2016-06-09 10:34:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		<System Generated>
-- =============================================
CREATE PROCEDURE [dbo].[spCategoriesInsert] 
(
	@dtAdded datetime,
	@iAddedBy int,
	@iLevelID int,
	@strTitle varchar(150),
	@strDescription varchar(1500),
	@strPathToImages varchar(1500),
	@strMasterImage varchar(1500),
	@bIsFeatured bit
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO tblCategories
	(
		dtAdded,
		iAddedBy,
		iLevelID,
		strTitle,
		strDescription,
		strPathToImages,
		strMasterImage,
		bIsFeatured,
		bIsDeleted

	) 
	VALUES
	(
		@dtAdded,
		@iAddedBy,
		@iLevelID,
		@strTitle,
		@strDescription,
		@strPathToImages,
		@strMasterImage,
		@bIsFeatured,
		0

	)
	SELECT MAX(iCategoryID) as iCategoryID FROM tblCategories

END

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON






GO
/****** Object:  StoredProcedure [dbo].[spCategoriesList]    Script Date: 2016-06-09 10:34:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<System Generated>
-- =============================================
CREATE PROCEDURE [dbo].[spCategoriesList]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM tblCategories
    WHERE bIsDeleted = 0

END




GO
/****** Object:  StoredProcedure [dbo].[spCategoriesUpdate]    Script Date: 2016-06-09 10:34:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		<System Generated>
-- =============================================
CREATE PROCEDURE [dbo].[spCategoriesUpdate] 
(
	@iCategoryID int,
	@dtEdited datetime,
	@iEditedBy int,
	@iLevelID int,
	@strTitle varchar(150),
	@strDescription varchar(1500), 
	@strPathToImages varchar(1500),
	@strMasterImage varchar(1500),
	@bIsFeatured bit
)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	UPDATE tblCategories  SET 
 dtEdited = @dtEdited,
 iEditedBy = @iEditedBy,
 iLevelID = @iLevelID,
 strTitle = @strTitle,
 strDescription = @strDescription,
 strPathToImages = @strPathToImages,
 strMasterImage = @strMasterImage,
 bIsFeatured = @bIsFeatured 
	
	WHERE iCategoryID= @iCategoryID

END




SET ANSI_NULLS ON






GO
/****** Object:  StoredProcedure [dbo].[spInitialiseAccountUser]    Script Date: 2016-06-09 10:34:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		System setup
-- Description:	Stored proc to initialise user on login
-- =============================================
CREATE PROCEDURE [dbo].[spInitialiseAccountUser](@strEmail varchar(80),@strPassword varchar(100))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT TOP(1) *
	FROM tblAccountUsers
	WHERE 
		strEmailAddress = @strEmail AND 
		strPassword = @strPassword AND 
		bIsDeleted = 0
END




GO
/****** Object:  StoredProcedure [dbo].[spInitialiseUser]    Script Date: 2016-06-09 10:34:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		System setup
-- Description:	Stored proc to initialise user on login
-- =============================================
CREATE PROCEDURE [dbo].[spInitialiseUser](@strEmail varchar(80),@strPassword varchar(100))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT TOP(1) *
	FROM tblUsers 
	WHERE 
		strEmailAddress = @strEmail AND 
		strPassword = @strPassword AND 
		bIsDeleted = 0
END



GO
/****** Object:  StoredProcedure [dbo].[spUsersDelete]    Script Date: 2016-06-09 10:34:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<System Generated>
-- =============================================
CREATE PROCEDURE [dbo].[spUsersDelete] (@iUserID int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE tblUsers SET bIsDeleted = 1 WHERE iUserID = @iUserID

END



GO
/****** Object:  StoredProcedure [dbo].[spUsersGetRecord]    Script Date: 2016-06-09 10:34:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<System Generated>
-- =============================================
CREATE PROCEDURE [dbo].[spUsersGetRecord] (@iUserID int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT TOP(1) * FROM tblUsers WHERE iUserID = @iUserID AND bIsDeleted = 0
END



GO
/****** Object:  StoredProcedure [dbo].[spUsersInsert]    Script Date: 2016-06-09 10:34:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<System Generated>
-- =============================================
CREATE PROCEDURE [dbo].[spUsersInsert] 
(
	@dtAdded datetime,
	@iAddedBy int,
	@strFirstName varchar(50),
	@strSurname varchar(50),
	@strEmailAddress varchar(250),
	@strPassword varchar(50)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO tblUsers
	(
		dtAdded,
		iAddedBy,
		strFirstName,
		strSurname,
		strEmailAddress,
		strPassword,
		bIsDeleted
	) 
	VALUES
	(
		@dtAdded,
		@iAddedBy,
		@strFirstName,
		@strSurname,
		@strEmailAddress,
		@strPassword,
		0
	)
	
	SELECT MAX(iUserID) as iUserID FROM tblUsers

END


GO
/****** Object:  StoredProcedure [dbo].[spUsersList]    Script Date: 2016-06-09 10:34:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<System Generated>
-- =============================================
CREATE PROCEDURE [dbo].[spUsersList]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM tblUsers
    WHERE bIsDeleted = 0

END


GO
/****** Object:  StoredProcedure [dbo].[spUsersUpdate]    Script Date: 2016-06-09 10:34:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<System Generated>
-- =============================================
CREATE PROCEDURE [dbo].[spUsersUpdate] 
(
	@iUserID int,
	@dtEdited datetime,
	@iEditedBy int,
	@strFirstName varchar(50),
	@strSurname varchar(50),
	@strEmailAddress varchar(250),
	@strPassword varchar(50)
)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE tblUsers SET 
		dtEdited = @dtEdited,
		iEditedBy = @iEditedBy,
		strFirstName = @strFirstName,
		strSurname = @strSurname,
		strEmailAddress = @strEmailAddress,
		strPassword = @strPassword
	WHERE 
		iUserID= @iUserID

END


GO
/****** Object:  Table [dbo].[tblCategories]    Script Date: 2016-06-09 10:34:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblCategories](
	[iCategoryID] [int] IDENTITY(1,1) NOT NULL,
	[dtAdded] [datetime] NOT NULL,
	[iAddedBy] [int] NOT NULL,
	[dtEdited] [datetime] NULL,
	[iEditedBy] [int] NULL,
	[iLevelID] [int] NOT NULL,
	[strTitle] [varchar](120) NOT NULL,
	[strDescription] [varchar](1500) NULL,
	[strPathToImages] [varchar](1500) NOT NULL,
	[strMasterImage] [varchar](1500) NOT NULL,
	[bIsFeatured] [bit] NOT NULL,
	[bIsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_tblCategories] PRIMARY KEY CLUSTERED 
(
	[iCategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblUsers]    Script Date: 2016-06-09 10:34:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblUsers](
	[iUserID] [int] IDENTITY(1,1) NOT NULL,
	[dtAdded] [datetime] NOT NULL,
	[iAddedBy] [int] NOT NULL,
	[dtEdited] [datetime] NULL,
	[iEditedBy] [int] NULL,
	[strFirstName] [varchar](50) NOT NULL,
	[strSurname] [varchar](50) NOT NULL,
	[strEmailAddress] [varchar](250) NOT NULL,
	[strPassword] [varchar](50) NOT NULL,
	[bIsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_tblUsers] PRIMARY KEY CLUSTERED 
(
	[iUserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
USE [master]
GO
ALTER DATABASE [CMS] SET  READ_WRITE 
GO
