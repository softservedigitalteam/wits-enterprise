﻿
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Wits_Enterprises.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Login()
        {
            return View();
        }
        public class LoginDetails
        {
            [Required]
            [StringLength(100, MinimumLength = 1)]
            [Display(Name = "Email")]
            [EmailAddress]
            public string strEmail { get; set; }
            [Required]
            [StringLength(50)]
            [DataType(DataType.Password)]
            public string strPassword { get; set; }
        }
    }
}