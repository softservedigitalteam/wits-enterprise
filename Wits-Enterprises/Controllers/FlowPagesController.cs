﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Wits_Enterprises.Controllers
{
    public class FlowPagesController : Controller
    {
        // GET: FlowPages
        public ActionResult Dashboard()
        {
            return View();
        }
        public ActionResult GenericFlowsheet()
        {
            GenericFlowsheetInfo info = new GenericFlowsheetInfo();
            return View(info);
        }

        [HttpPost]
        public ActionResult GenericFlowsheet(GenericFlowsheetInfo gfi)
        {
            //return RedirectToAction("Agitation", "FlowPages");
            if (gfi.strSelectedProductLocation == "Microbial Growth")
            {
                return RedirectToAction("MicrobialGrowthConditions2", "FlowPages");
            }
            else if (gfi.strSelectedProductLocation == null)
            {
                GenericFlowsheetInfo info = new GenericFlowsheetInfo();
                return View(info);
            }
            else
            {
                return RedirectToAction("MicrobialGrowthConditions", "FlowPages");
            }
        }
        public ActionResult MicrobialGrowthConditions()
        {
            MicrobialGrowthConditionsInfo info = new MicrobialGrowthConditionsInfo();
            return View(info);
        }
        public ActionResult MicrobialGrowthConditions2()
        {
            MicrobialGrowthConditionsInfo info2 = new MicrobialGrowthConditionsInfo();
            return View(info2);
        }
        public ActionResult Agitation()
        {
            return View();
        }
        public ActionResult CellDisruption()
        {
            return View();
        }
        public ActionResult SolidLiquidSeperation()
        {
            return View();
        }
        public ActionResult ConcentrationPurification()
        {
            return View();
        }
        public ActionResult Formulation()
        {
            return View();
        }
        public ActionResult WasteWaterTreatment()
        {
            return View();
        }
        public ActionResult Results()
        {
            return View();
        }
        public class GenericFlowsheetInfo
        {
            public string strSelectedProductLocation { get; set; }
            public string strOxygen { get; set; }
            public string strProcessTime { get; set; }
        }

        public class MicrobialGrowthConditionsInfo
        {
            public string strMaintenanceType { get; set; }
        }
    }
}