
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;

/// <summary>
/// Summary description for clsMaintenanceCoeficcientsView
/// </summary>
public partial class CMS_clsMaintenanceCoeficcientsView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtMaintenanceCoeficcientsList;

    List<clsMaintenanceCoeficcients> glstMaintenanceCoeficcients;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
        }
            PopulateFormData();

    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string FilterExpression ="(strTitle) LIKE '%" + txtSearch.Text + "%'";
        DataTable dtMaintenanceCoeficcientsList = clsMaintenanceCoeficcients.GetMaintenanceCoeficcientsList(FilterExpression, "");

        Session["dtMaintenanceCoeficcientsList"] = dtMaintenanceCoeficcientsList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmMaintenanceCoefficientsAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsMaintenanceCoeficcientsList object
        try
        {
            dtMaintenanceCoeficcientsList = new DataTable();

            if (Session["dtMaintenanceCoeficcientsList"] == null)
                dtMaintenanceCoeficcientsList = clsMaintenanceCoeficcients.GetMaintenanceCoeficcientsList();
            else
                dtMaintenanceCoeficcientsList = (DataTable)Session["dtMaintenanceCoeficcientsList"];

            dgrGrid.DataSource = dtMaintenanceCoeficcientsList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["MaintenanceCoeficcientsView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["MaintenanceCoeficcientsView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["MaintenanceCoeficcientsView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iMaintenanceCoeficcientID = int.Parse((sender as LinkButton).CommandArgument);

        clsMaintenanceCoeficcients.Delete(iMaintenanceCoeficcientID);

        PopulateFormData();
        Session["dtMaintenanceCoeficcientsList"] = null;
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["MaintenanceCoeficcientsView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtMaintenanceCoeficcientsList = new DataTable();

        if (Session["dtMaintenanceCoeficcientsList"] == null)
            dtMaintenanceCoeficcientsList = clsMaintenanceCoeficcients.GetMaintenanceCoeficcientsList();
        else
            dtMaintenanceCoeficcientsList = (DataTable)Session["dtMaintenanceCoeficcientsList"];

        DataView dvTemp = dtMaintenanceCoeficcientsList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtMaintenanceCoeficcientsList = dvTemp.ToTable();
        Session["dtMaintenanceCoeficcientsList"] = dtMaintenanceCoeficcientsList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iMaintenanceCoeficcientID
            int iMaintenanceCoeficcientID = 0;
            iMaintenanceCoeficcientID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmMaintenanceCoefficientsAddModify.aspx?action=edit&iMaintenanceCoeficcientID=" + iMaintenanceCoeficcientID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    #endregion

    #region MaintenanceCoeficcients FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strTitle) LIKE '%" + prefixText + "%'";
        DataTable dtMaintenanceCoeficcients = clsMaintenanceCoeficcients.GetMaintenanceCoeficcientsList(FilterExpression, "");
        List<string> glstMaintenanceCoeficcients = new List<string>();

        if (dtMaintenanceCoeficcients.Rows.Count > 0)
        {
            foreach (DataRow dtrMaintenanceCoeficcients in dtMaintenanceCoeficcients.Rows)
            {
                glstMaintenanceCoeficcients.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrMaintenanceCoeficcients["strTitle"].ToString(), dtrMaintenanceCoeficcients["iMaintenanceCoeficcientID"].ToString()));
            }
        }
        else
            glstMaintenanceCoeficcients.Add("No MaintenanceCoeficcients Available.");
        strReturnList = glstMaintenanceCoeficcients.ToArray();
        return strReturnList;
    }

    #endregion
}
