
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;

/// <summary>
/// Summary description for clsSubscriptionsView
/// </summary>
public partial class CMS_clsSubscriptionsView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtSubscriptionsList;

    List<clsSubscriptions> glstSubscriptions;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
        }
            PopulateFormData();

    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string FilterExpression ="(strTitle) LIKE '%" + txtSearch.Text + "%'";
        DataTable dtSubscriptionsList = clsSubscriptions.GetSubscriptionsList(FilterExpression, "");

        Session["dtSubscriptionsList"] = dtSubscriptionsList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmSubscriptionsAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsSubscriptionsList object
        try
        {
            dtSubscriptionsList = new DataTable();

            if (Session["dtSubscriptionsList"] == null)
                dtSubscriptionsList = clsSubscriptions.GetSubscriptionsList();
            else
                dtSubscriptionsList = (DataTable)Session["dtSubscriptionsList"];

            dgrGrid.DataSource = dtSubscriptionsList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["SubscriptionsView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["SubscriptionsView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["SubscriptionsView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iSubscriptionID = int.Parse((sender as LinkButton).CommandArgument);

        clsSubscriptions.Delete(iSubscriptionID);

        PopulateFormData();
        Session["dtSubscriptionsList"] = null;
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["SubscriptionsView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtSubscriptionsList = new DataTable();

        if (Session["dtSubscriptionsList"] == null)
            dtSubscriptionsList = clsSubscriptions.GetSubscriptionsList();
        else
            dtSubscriptionsList = (DataTable)Session["dtSubscriptionsList"];

        DataView dvTemp = dtSubscriptionsList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtSubscriptionsList = dvTemp.ToTable();
        Session["dtSubscriptionsList"] = dtSubscriptionsList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iSubscriptionID
            int iSubscriptionID = 0;
            iSubscriptionID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmSubscriptionsAddModify.aspx?action=edit&iSubscriptionID=" + iSubscriptionID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    #endregion

    #region Subscriptions FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strTitle) LIKE '%" + prefixText + "%'";
        DataTable dtSubscriptions = clsSubscriptions.GetSubscriptionsList(FilterExpression, "");
        List<string> glstSubscriptions = new List<string>();

        if (dtSubscriptions.Rows.Count > 0)
        {
            foreach (DataRow dtrSubscriptions in dtSubscriptions.Rows)
            {
                glstSubscriptions.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrSubscriptions["strTitle"].ToString(), dtrSubscriptions["iSubscriptionID"].ToString()));
            }
        }
        else
            glstSubscriptions.Add("No Subscriptions Available.");
        strReturnList = glstSubscriptions.ToArray();
        return strReturnList;
    }

    #endregion
}
