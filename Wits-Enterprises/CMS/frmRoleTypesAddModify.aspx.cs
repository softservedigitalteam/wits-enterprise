using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CMS_frmRoleTypesAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsRoleTypes clsRoleTypes;
    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        if (!IsPostBack)
        {

            //### If the iRoleTypeID is passed through then we want to instantiate the object with that iRoleTypeID
            if ((Request.QueryString["iRoleTypeID"] != "") && (Request.QueryString["iRoleTypeID"] != null))
            {
                clsRoleTypes = new clsRoleTypes(Convert.ToInt32(Request.QueryString["iRoleTypeID"]));

                //### Populate the form
                popFormData();
            }
            else
            {
                clsRoleTypes = new clsRoleTypes();
            }
            Session["clsRoleTypes"] = clsRoleTypes;
        }
        else
        {
            clsRoleTypes = (clsRoleTypes)Session["clsRoleTypes"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmRoleTypesView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

       bCanSave = clsValidation.IsNullOrEmpty(txtTitle, bCanSave);

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">RoleType added successfully</div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - RoleType not added</div></div>";
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        txtTitle.Text = "";
        clsValidation.SetValid(txtTitle);
    }

    #endregion 

    #region POPULATE DATA METHODS

    private void popFormData()
    {
         txtTitle.Text = clsRoleTypes.strTitle;
    }
    
    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        clsRoleTypes.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsRoleTypes.iAddedBy = clsUsers.iUserID;
        clsRoleTypes.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsRoleTypes.iEditedBy = clsUsers.iUserID;
        clsRoleTypes.strTitle = txtTitle.Text;

        clsRoleTypes.Update();

        Session["dtRoleTypesList"] = null;

        //### Go back to view page
        Response.Redirect("frmRoleTypesView.aspx");
    }

    #endregion
}