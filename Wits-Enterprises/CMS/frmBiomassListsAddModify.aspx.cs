using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CMS_frmBiomassListsAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsBiomassLists clsBiomassLists;
    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        if (!IsPostBack)
        {
             popChemicalFormula();
             //popLevel();

            //### If the iBiomassListID is passed through then we want to instantiate the object with that iBiomassListID
            if ((Request.QueryString["iBiomassListID"] != "") && (Request.QueryString["iBiomassListID"] != null))
            {
                clsBiomassLists = new clsBiomassLists(Convert.ToInt32(Request.QueryString["iBiomassListID"]));

                //### Populate the form
                popFormData();
            }
            else
            {
                PopBiomassCategories(0);
                clsBiomassLists = new clsBiomassLists();
            }
            Session["clsBiomassLists"] = clsBiomassLists;
        }
        else
        {
            clsBiomassLists = (clsBiomassLists)Session["clsBiomassLists"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmBiomassListsView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

       bCanSave = clsValidation.IsNullOrEmpty(txtTitle, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(lstChemicalFormula, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtAshContent, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtPrice, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtDensity, bCanSave);

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">BiomassList added successfully</div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - BiomassList not added</div></div>";
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        txtTitle.Text = "";
        clsValidation.SetValid(txtTitle);
        txtDescription.Text = "";
        clsValidation.SetValid(txtDescription);
        lstChemicalFormula.SelectedValue = "0";
        clsValidation.SetValid(lstChemicalFormula);
        txtAshContent.Text = "";
        clsValidation.SetValid(txtAshContent);
        txtPrice.Text = "";
        clsValidation.SetValid(txtPrice);
        txtDensity.Text = "";
        clsValidation.SetValid(txtDensity);
        //lstLevel.SelectedValue = "0";
        //clsValidation.SetValid(lstLevel);
    }

    #endregion 

    #region POPULATE DATA METHODS

    private void popFormData()
    {
         txtTitle.Text = clsBiomassLists.strTitle;
         txtDescription.Text = clsBiomassLists.strDescription;
         lstChemicalFormula.SelectedValue = clsBiomassLists.iChemicalFormulaID.ToString();
        txtAshContent.Text = clsBiomassLists.dblAshContent.ToString();
        txtPrice.Text = clsBiomassLists.dblPrice.ToString();
        txtDensity.Text = clsBiomassLists.dblDensity.ToString();
        txtAshContent.Text = clsBiomassLists.dblAshContent.ToString();
        //lstLevel.SelectedValue = clsBiomassLists.iLevelID.ToString();
        PopBiomassCategories(clsBiomassLists.iLevelID);
    }
    
    private void popChemicalFormula()
    {
         DataTable dtChemicalFormulasList = new DataTable();
         lstChemicalFormula.DataSource = clsChemicalFormulas.GetChemicalFormulasList();

         //### Populates the drop down list with PK and TITLE;
         lstChemicalFormula.DataValueField = "iChemicalFormulaID";
         lstChemicalFormula.DataTextField = "strTitle";

         //### Bind the data to the list;
         lstChemicalFormula.DataBind();

         //### Add default select option;
         lstChemicalFormula.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    }

    /// <summary>
    /// Populate the Tree view with the category types
    /// </summary>
    private void PopBiomassCategories(int clsBiomassiCategoryID)
    {
        treeBiomassCategories.Nodes.Clear();
        TreeNode Mainnode = new TreeNode();
        bool bIsSelected = false;
        Mainnode.Value = "0";
        Mainnode.Text = "Top level category";
        if (clsBiomassiCategoryID == 0)
        {
            Mainnode.Selected = true;
        }
        treeBiomassCategories.Nodes.Add(Mainnode);

        DataTable DT = new DataTable();
        DT = clsBiomassLists.GetBiomassListsList("iLevelID = 0", "strTitle");
        foreach (DataRow r in DT.Rows)
        {
            int iBiomassListID = 0;
            iBiomassListID = int.Parse(r["iBiomassListID"].ToString());
            TreeNode node = new TreeNode(r["strTitle"].ToString(), r["iBiomassListID"].ToString());
            if (clsBiomassiCategoryID == int.Parse(r["iBiomassListID"].ToString()))
            {
                node.Selected = true;
            }

            //### Now we call the PopChildCategories method to build the sub categories
            PopChildCategories(ref node, int.Parse(r["iBiomassListID"].ToString()), clsBiomassiCategoryID, ref bIsSelected);
            if (bIsSelected)
            {
                node.Expanded = true;
            }
            else
            {
                node.Expanded = false;
            }
            treeBiomassCategories.Nodes.Add(node);
        }
    }

    /// <summary>
    /// Populate the Subcategories for the Category Treeview.
    /// This Method will call itself to build the Subcategory of the current Subcategory
    /// </summary>
    /// <param name="ChildNode">The current Top level category</param>
    /// <param name="iBiomassListID">The ID of the Category</param>
    /// <param name="clsBiomassiCategoryID">The ID of the clsCategories.iCategoryID</param>
    private void PopChildCategories(ref TreeNode ChildNode, int iBiomassListID, int clsBiomassiCategoryID, ref bool bIsSelected)
    {
        DataTable DT = new DataTable();
        DT = clsBiomassLists.GetBiomassListsList("iLevelID = " + iBiomassListID.ToString(), "strTitle");

        foreach (DataRow r in DT.Rows)
        {
            TreeNode node = new TreeNode(r["strTitle"].ToString(), r["iBiomassListID"].ToString());
            if (clsBiomassiCategoryID == int.Parse(r["iBiomassListID"].ToString()))
            {
                node.Selected = true;
                bIsSelected = true;
            }
            if (bIsSelected)
            {
                node.Expanded = true;
            }
            ChildNode.ChildNodes.Add(node);

            //### Now we call this methode again to build the subsubcategories
            PopChildCategories(ref node, int.Parse(r["iBiomassListID"].ToString()), clsBiomassiCategoryID, ref bIsSelected);
        }
    }

    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        clsBiomassLists.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsBiomassLists.iAddedBy = clsUsers.iUserID;
        clsBiomassLists.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsBiomassLists.iEditedBy = clsUsers.iUserID;
        clsBiomassLists.strTitle = txtTitle.Text;
        clsBiomassLists.strDescription = txtDescription.Text;
        clsBiomassLists.iChemicalFormulaID = Convert.ToInt32(lstChemicalFormula.SelectedValue.ToString());
        clsBiomassLists.dblAshContent = Convert.ToDouble(txtAshContent.Text.ToString());
        int iLevelID;

        if (treeBiomassCategories.Nodes.Count != 0)
        {
            if (treeBiomassCategories.SelectedNode != null)
            {
                iLevelID = int.Parse(treeBiomassCategories.SelectedNode.Value);
            }
            else
            {
                iLevelID = 0;
            }
        }
        else
        {
            iLevelID = 0;
        }

        clsBiomassLists.iLevelID = iLevelID;
        clsBiomassLists.dblPrice = Convert.ToDouble(txtPrice.Text.ToString());
        clsBiomassLists.dblDensity = Convert.ToDouble(txtDensity.Text.ToString());

        //clsBiomassLists.iLevelID = Convert.ToInt32(lstLevel.SelectedValue.ToString());

        clsBiomassLists.Update();

        Session["dtBiomassListsList"] = null;

        //### Go back to view page
        Response.Redirect("frmBiomassListsView.aspx");
    }

    #endregion
}