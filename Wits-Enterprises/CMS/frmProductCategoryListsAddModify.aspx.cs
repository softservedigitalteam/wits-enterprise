using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CMS_frmProductCategoryListsAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsProductCategoryLists clsProductCategoryLists;
    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        if (!IsPostBack)
        {
             popChemicalFormula();
             //popLevel();

            //### If the iProductCategoryListID is passed through then we want to instantiate the object with that iProductCategoryListID
            if ((Request.QueryString["iProductCategoryListID"] != "") && (Request.QueryString["iProductCategoryListID"] != null))
            {
                clsProductCategoryLists = new clsProductCategoryLists(Convert.ToInt32(Request.QueryString["iProductCategoryListID"]));

                //### Populate the form
                popFormData();
            }
            else
            {
                PopProductCategories(0);
                clsProductCategoryLists = new clsProductCategoryLists();
            }
            Session["clsProductCategoryLists"] = clsProductCategoryLists;
        }
        else
        {
            clsProductCategoryLists = (clsProductCategoryLists)Session["clsProductCategoryLists"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmProductCategoryListsView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

       bCanSave = clsValidation.IsNullOrEmpty(txtTitle, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(lstChemicalFormula, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtPrice, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtDensity, bCanSave);

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">ProductCategoryList added successfully</div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - ProductCategoryList not added</div></div>";
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        txtTitle.Text = "";
        clsValidation.SetValid(txtTitle);
        txtDescription.Text = "";
        clsValidation.SetValid(txtDescription);
        lstChemicalFormula.SelectedValue = "0";
        clsValidation.SetValid(lstChemicalFormula);
        txtPrice.Text = "";
        clsValidation.SetValid(txtPrice);
        txtDensity.Text = "";
        clsValidation.SetValid(txtDensity);
        //lstLevel.SelectedValue = "0";
        //clsValidation.SetValid(lstLevel);
    }

    #endregion 

    #region POPULATE DATA METHODS

    private void popFormData()
    {
         txtTitle.Text = clsProductCategoryLists.strTitle;
         txtDescription.Text = clsProductCategoryLists.strDescription;
         lstChemicalFormula.SelectedValue = clsProductCategoryLists.iChemicalFormulaID.ToString();
        txtPrice.Text = clsProductCategoryLists.dblPrice.ToString();
        txtDensity.Text = clsProductCategoryLists.dblDensity.ToString();
        //lstLevel.SelectedValue = clsProductCategoryLists.iLevelID.ToString();
        PopProductCategories(clsProductCategoryLists.iLevelID);
    }
    
    private void popChemicalFormula()
    {
         DataTable dtChemicalFormulasList = new DataTable();
         lstChemicalFormula.DataSource = clsChemicalFormulas.GetChemicalFormulasList();

         //### Populates the drop down list with PK and TITLE;
         lstChemicalFormula.DataValueField = "iChemicalFormulaID";
         lstChemicalFormula.DataTextField = "strTitle";

         //### Bind the data to the list;
         lstChemicalFormula.DataBind();

         //### Add default select option;
         lstChemicalFormula.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    }

    /// <summary>
    /// Populate the Tree view with the category types
    /// </summary>
    private void PopProductCategories(int clsProductCategoriesiCategoryID)
    {
        treeProductCategories.Nodes.Clear();
        TreeNode Mainnode = new TreeNode();
        bool bIsSelected = false;
        Mainnode.Value = "0";
        Mainnode.Text = "Top level category";
        if (clsProductCategoriesiCategoryID == 0)
        {
            Mainnode.Selected = true;
        }
        treeProductCategories.Nodes.Add(Mainnode);

        DataTable DT = new DataTable();
        DT = clsProductCategoryLists.GetProductCategoryListsList("iLevelID = 0", "strTitle");
        foreach (DataRow r in DT.Rows)
        {
            int iProductCategoryListID = 0;
            iProductCategoryListID = int.Parse(r["iProductCategoryListID"].ToString());
            TreeNode node = new TreeNode(r["strTitle"].ToString(), r["iProductCategoryListID"].ToString());
            if (clsProductCategoriesiCategoryID == int.Parse(r["iProductCategoryListID"].ToString()))
            {
                node.Selected = true;
            }

            //### Now we call the PopChildCategories method to build the sub categories
            PopChildCategories(ref node, int.Parse(r["iProductCategoryListID"].ToString()), clsProductCategoriesiCategoryID, ref bIsSelected);
            if (bIsSelected)
            {
                node.Expanded = true;
            }
            else
            {
                node.Expanded = false;
            }
            treeProductCategories.Nodes.Add(node);
        }
    }

    /// <summary>
    /// Populate the Subcategories for the Category Treeview.
    /// This Method will call itself to build the Subcategory of the current Subcategory
    /// </summary>
    /// <param name="ChildNode">The current Top level category</param>
    /// <param name="iProductCategoryListID">The ID of the Category</param>
    /// <param name="clsProductCategoriesiCategoryID">The ID of the clsCategories.iCategoryID</param>
    private void PopChildCategories(ref TreeNode ChildNode, int iProductCategoryListID, int clsProductCategoriesiCategoryID, ref bool bIsSelected)
    {
        DataTable DT = new DataTable();
        DT = clsProductCategoryLists.GetProductCategoryListsList("iLevelID = " + iProductCategoryListID.ToString(), "strTitle");

        foreach (DataRow r in DT.Rows)
        {
            TreeNode node = new TreeNode(r["strTitle"].ToString(), r["iProductCategoryListID"].ToString());
            if (clsProductCategoriesiCategoryID == int.Parse(r["iProductCategoryListID"].ToString()))
            {
                node.Selected = true;
                bIsSelected = true;
            }
            if (bIsSelected)
            {
                node.Expanded = true;
            }
            ChildNode.ChildNodes.Add(node);

            //### Now we call this methode again to build the subsubcategories
            PopChildCategories(ref node, int.Parse(r["iProductCategoryListID"].ToString()), clsProductCategoriesiCategoryID, ref bIsSelected);
        }
    }

    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        clsProductCategoryLists.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsProductCategoryLists.iAddedBy = clsUsers.iUserID;
        clsProductCategoryLists.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsProductCategoryLists.iEditedBy = clsUsers.iUserID;
        clsProductCategoryLists.strTitle = txtTitle.Text;
        clsProductCategoryLists.strDescription = txtDescription.Text;
        clsProductCategoryLists.iChemicalFormulaID = Convert.ToInt32(lstChemicalFormula.SelectedValue.ToString());
        int iLevelID;

        if (treeProductCategories.Nodes.Count != 0)
        {
            if (treeProductCategories.SelectedNode != null)
            {
                iLevelID = int.Parse(treeProductCategories.SelectedNode.Value);
            }
            else
            {
                iLevelID = 0;
            }
        }
        else
        {
            iLevelID = 0;
        }

        clsProductCategoryLists.iLevelID = iLevelID;
        clsProductCategoryLists.dblPrice = Convert.ToDouble(txtPrice.Text.ToString());
        clsProductCategoryLists.dblDensity = Convert.ToDouble(txtDensity.Text.ToString());

        //clsProductCategoryLists.iLevelID = Convert.ToInt32(lstLevel.SelectedValue.ToString());

        clsProductCategoryLists.Update();

        Session["dtProductCategoryListsList"] = null;

        //### Go back to view page
        Response.Redirect("frmProductCategoryListsView.aspx");
    }

    #endregion
}