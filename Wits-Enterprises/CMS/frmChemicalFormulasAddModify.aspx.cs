using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CMS_frmChemicalFormulasAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsChemicalFormulas clsChemicalFormulas;
    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        if (!IsPostBack)
        {

            //### If the iChemicalFormulaID is passed through then we want to instantiate the object with that iChemicalFormulaID
            if ((Request.QueryString["iChemicalFormulaID"] != "") && (Request.QueryString["iChemicalFormulaID"] != null))
            {
                clsChemicalFormulas = new clsChemicalFormulas(Convert.ToInt32(Request.QueryString["iChemicalFormulaID"]));

                //### Populate the form
                popFormData();
            }
            else
            {
                clsChemicalFormulas = new clsChemicalFormulas();
            }
            Session["clsChemicalFormulas"] = clsChemicalFormulas;
        }
        else
        {
            clsChemicalFormulas = (clsChemicalFormulas)Session["clsChemicalFormulas"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmChemicalFormulasView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

       bCanSave = clsValidation.IsNullOrEmpty(txtTitle, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(txtPhaseConsistency, bCanSave);

        bCanSave = clsValidation.IsNullOrEmpty(txtCarbon, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtHydrogenH, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtOxygenO, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtNitrogenN, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtPhosphorusP, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtSulferS, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtSodiumNa, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtChlorineCl, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtPotassiumK, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtIronFe, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtMagnesiumMg, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtCalciumCa, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtSiliconSi, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtAluminiumAl, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtCopperCu, bCanSave);

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">ChemicalFormula added successfully</div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - ChemicalFormula not added</div></div>";
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        txtTitle.Text = "";
        clsValidation.SetValid(txtTitle);
        txtPhaseConsistency.Text = "";
        clsValidation.SetValid(txtPhaseConsistency);

        txtCarbon.Text = "";
        clsValidation.SetValid(txtCarbon);
        txtHydrogenH.Text = "";
        clsValidation.SetValid(txtHydrogenH);
        txtOxygenO.Text = "";
        clsValidation.SetValid(txtOxygenO);
        txtNitrogenN.Text = "";
        clsValidation.SetValid(txtNitrogenN);
        txtPhosphorusP.Text = "";
        clsValidation.SetValid(txtPhosphorusP);
        txtSulferS.Text = "";
        clsValidation.SetValid(txtSulferS);
        txtSodiumNa.Text = "";
        clsValidation.SetValid(txtSodiumNa);
        txtChlorineCl.Text = "";
        clsValidation.SetValid(txtChlorineCl);
        txtPotassiumK.Text = "";
        clsValidation.SetValid(txtPotassiumK);
        txtIronFe.Text = "";
        clsValidation.SetValid(txtIronFe);

        txtMagnesiumMg.Text = "";
        clsValidation.SetValid(txtMagnesiumMg);
        txtCalciumCa.Text = "";
        clsValidation.SetValid(txtCalciumCa);
        txtSiliconSi.Text = "";
        clsValidation.SetValid(txtSiliconSi);
        txtAluminiumAl.Text = "";
        clsValidation.SetValid(txtAluminiumAl);
        txtCopperCu.Text = "";
        clsValidation.SetValid(txtCopperCu);
    }

    #endregion 

    #region POPULATE DATA METHODS

    private void popFormData()
    {
         txtTitle.Text = clsChemicalFormulas.strTitle;
         txtPhaseConsistency.Text = clsChemicalFormulas.strPhaseConsistency;

        txtCarbon.Text = clsChemicalFormulas.dblCarbon.ToString();
        txtHydrogenH.Text = clsChemicalFormulas.dblHydrogenH.ToString();
        txtOxygenO.Text = clsChemicalFormulas.dblOxygenO.ToString();
        txtNitrogenN.Text = clsChemicalFormulas.dblNitrogenN.ToString();
        txtPhosphorusP.Text = clsChemicalFormulas.dblPhosphorusP.ToString();
        txtSulferS.Text = clsChemicalFormulas.dblSulfurS.ToString();
        txtSodiumNa.Text = clsChemicalFormulas.dblSodiumNa.ToString();
        txtChlorineCl.Text = clsChemicalFormulas.dblChlorineCl.ToString();
        txtPotassiumK.Text = clsChemicalFormulas.dblPotassiumK.ToString();
        txtIronFe.Text = clsChemicalFormulas.dblIronFe.ToString();

        txtMagnesiumMg.Text = clsChemicalFormulas.dblMagnesiumMg.ToString();
        txtCalciumCa.Text = clsChemicalFormulas.dblCalciumCa.ToString();
        txtSiliconSi.Text = clsChemicalFormulas.dblSiliconSi.ToString();
        txtAluminiumAl.Text = clsChemicalFormulas.dblAluminiumAl.ToString();
        txtCopperCu.Text = clsChemicalFormulas.dblCopperCu.ToString();
        lstAnEstimate.SelectedValue = clsChemicalFormulas.bIsAnEstimate.ToString();
    }
    
    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        clsChemicalFormulas.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsChemicalFormulas.iAddedBy = clsUsers.iUserID;
        clsChemicalFormulas.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsChemicalFormulas.iEditedBy = clsUsers.iUserID;
        clsChemicalFormulas.strTitle = txtTitle.Text;
        clsChemicalFormulas.strPhaseConsistency = txtPhaseConsistency.Text;

        clsChemicalFormulas.dblCarbon = Convert.ToDouble(txtCarbon.Text);
        clsChemicalFormulas.dblHydrogenH = Convert.ToDouble(txtHydrogenH.Text);
        clsChemicalFormulas.dblOxygenO = Convert.ToDouble(txtOxygenO.Text);
        clsChemicalFormulas.dblNitrogenN = Convert.ToDouble(txtNitrogenN.Text);
        clsChemicalFormulas.dblPhosphorusP = Convert.ToDouble(txtPhosphorusP.Text);
        clsChemicalFormulas.dblSulfurS = Convert.ToDouble(txtSulferS.Text);
        clsChemicalFormulas.dblSodiumNa = Convert.ToDouble(txtSodiumNa.Text);
        clsChemicalFormulas.dblChlorineCl = Convert.ToDouble(txtChlorineCl.Text);
        clsChemicalFormulas.dblPotassiumK = Convert.ToDouble(txtPotassiumK.Text);
        clsChemicalFormulas.dblIronFe = Convert.ToDouble(txtIronFe.Text);

        clsChemicalFormulas.dblMagnesiumMg = Convert.ToDouble(txtMagnesiumMg.Text);
        clsChemicalFormulas.dblCalciumCa = Convert.ToDouble(txtCalciumCa.Text);
        clsChemicalFormulas.dblSiliconSi = Convert.ToDouble(txtSiliconSi.Text);
        clsChemicalFormulas.dblAluminiumAl = Convert.ToDouble(txtAluminiumAl.Text);
        clsChemicalFormulas.dblCopperCu = Convert.ToDouble(txtCopperCu.Text);
        clsChemicalFormulas.bIsAnEstimate = Convert.ToBoolean(lstAnEstimate.SelectedItem.Value);

        clsChemicalFormulas.Update();

        Session["dtChemicalFormulasList"] = null;

        //### Go back to view page
        Response.Redirect("frmChemicalFormulasView.aspx");
    }

    #endregion
}