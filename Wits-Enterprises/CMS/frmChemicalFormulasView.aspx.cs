
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;

/// <summary>
/// Summary description for clsChemicalFormulasView
/// </summary>
public partial class CMS_clsChemicalFormulasView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtChemicalFormulasList;

    List<clsChemicalFormulas> glstChemicalFormulas;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
        }
            PopulateFormData();

    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string FilterExpression ="(strTitle +' '+ strPhaseConsistency) LIKE '%" + txtSearch.Text + "%'";
        DataTable dtChemicalFormulasList = clsChemicalFormulas.GetChemicalFormulasList(FilterExpression, "");

        Session["dtChemicalFormulasList"] = dtChemicalFormulasList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmChemicalFormulasAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsChemicalFormulasList object
        try
        {
            dtChemicalFormulasList = new DataTable();

            if (Session["dtChemicalFormulasList"] == null)
                dtChemicalFormulasList = clsChemicalFormulas.GetChemicalFormulasList();
            else
                dtChemicalFormulasList = (DataTable)Session["dtChemicalFormulasList"];

            dgrGrid.DataSource = dtChemicalFormulasList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["ChemicalFormulasView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["ChemicalFormulasView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["ChemicalFormulasView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iChemicalFormulaID = int.Parse((sender as LinkButton).CommandArgument);

        clsChemicalFormulas.Delete(iChemicalFormulaID);

        PopulateFormData();
        Session["dtChemicalFormulasList"] = null;
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["ChemicalFormulasView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtChemicalFormulasList = new DataTable();

        if (Session["dtChemicalFormulasList"] == null)
            dtChemicalFormulasList = clsChemicalFormulas.GetChemicalFormulasList();
        else
            dtChemicalFormulasList = (DataTable)Session["dtChemicalFormulasList"];

        DataView dvTemp = dtChemicalFormulasList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtChemicalFormulasList = dvTemp.ToTable();
        Session["dtChemicalFormulasList"] = dtChemicalFormulasList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iChemicalFormulaID
            int iChemicalFormulaID = 0;
            iChemicalFormulaID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmChemicalFormulasAddModify.aspx?action=edit&iChemicalFormulaID=" + iChemicalFormulaID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    #endregion

    #region ChemicalFormulas FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strTitle +' '+ strPhaseConsistency) LIKE '%" + prefixText + "%'";
        DataTable dtChemicalFormulas = clsChemicalFormulas.GetChemicalFormulasList(FilterExpression, "");
        List<string> glstChemicalFormulas = new List<string>();

        if (dtChemicalFormulas.Rows.Count > 0)
        {
            foreach (DataRow dtrChemicalFormulas in dtChemicalFormulas.Rows)
            {
                glstChemicalFormulas.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrChemicalFormulas["strTitle"].ToString() +' '+"" + dtrChemicalFormulas["strPhaseConsistency"].ToString(), dtrChemicalFormulas["iChemicalFormulaID"].ToString()));
            }
        }
        else
            glstChemicalFormulas.Add("No ChemicalFormulas Available.");
        strReturnList = glstChemicalFormulas.ToArray();
        return strReturnList;
    }

    #endregion
}
