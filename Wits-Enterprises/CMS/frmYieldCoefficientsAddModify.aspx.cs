using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CMS_frmYieldCoefficientsAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsYieldCoeficcients clsYieldCoeficcients;
    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        if (!IsPostBack)
        {
             popProductCategoryList();
             popBiomassList();
             popCarbonSourceCategoryList();

            //### If the iYieldCoeficcientID is passed through then we want to instantiate the object with that iYieldCoeficcientID
            if ((Request.QueryString["iYieldCoeficcientID"] != "") && (Request.QueryString["iYieldCoeficcientID"] != null))
            {
                clsYieldCoeficcients = new clsYieldCoeficcients(Convert.ToInt32(Request.QueryString["iYieldCoeficcientID"]));

                //### Populate the form
                popFormData();
            }
            else
            {
                clsYieldCoeficcients = new clsYieldCoeficcients();
            }
            Session["clsYieldCoeficcients"] = clsYieldCoeficcients;
        }
        else
        {
            clsYieldCoeficcients = (clsYieldCoeficcients)Session["clsYieldCoeficcients"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmYieldCoefficientsView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

       bCanSave = clsValidation.IsNullOrEmpty(txtTitle, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtPrice, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(lstProductCategoryList, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(lstBiomassList, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(lstCarbonSourceCategoryList, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtPs, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtXs, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtXo, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtProductProductivity, bCanSave);

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">YieldCoeficcient added successfully</div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - YieldCoeficcient not added</div></div>";
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        txtTitle.Text = "";
        clsValidation.SetValid(txtTitle);
        txtDescription.Text = "";
        clsValidation.SetValid(txtDescription);
        txtPrice.Text = "";
        clsValidation.SetValid(txtPrice);
        lstProductCategoryList.SelectedValue = "0";
        clsValidation.SetValid(lstProductCategoryList);
        lstBiomassList.SelectedValue = "0";
        clsValidation.SetValid(lstBiomassList);
        lstCarbonSourceCategoryList.SelectedValue = "0";
        clsValidation.SetValid(lstCarbonSourceCategoryList);
        txtPs.Text = "";
        clsValidation.SetValid(txtPs);
        txtXs.Text = "";
        clsValidation.SetValid(txtXs);
        txtXo.Text = "";
        clsValidation.SetValid(txtXo);
        txtProductProductivity.Text = "";
        clsValidation.SetValid(txtProductProductivity);
        txtReferenceNotes.Text = "";
        clsValidation.SetValid(txtReferenceNotes);
    }

    #endregion 

    #region POPULATE DATA METHODS

    private void popFormData()
    {
         txtTitle.Text = clsYieldCoeficcients.strTitle;
         txtDescription.Text = clsYieldCoeficcients.strDescription;
        txtPrice.Text = clsYieldCoeficcients.dblPrice.ToString();
        lstProductCategoryList.SelectedValue = clsYieldCoeficcients.iProductCategoryListID.ToString();
         lstBiomassList.SelectedValue = clsYieldCoeficcients.iBiomassListID.ToString();
         lstCarbonSourceCategoryList.SelectedValue = clsYieldCoeficcients.iCarbonSourceCategoryListID.ToString();
        txtPs.Text = clsYieldCoeficcients.dblPs.ToString();
        txtXs.Text = clsYieldCoeficcients.dblXs.ToString();
        txtXo.Text = clsYieldCoeficcients.dblXo.ToString();
        txtProductProductivity.Text = clsYieldCoeficcients.dblProductProductivity.ToString();
        txtReferenceNotes.Text = clsYieldCoeficcients.strReferenceNotes;
    }
    
    private void popProductCategoryList()
    {
         DataTable dtProductCategoryListsList = new DataTable();
         lstProductCategoryList.DataSource = clsProductCategoryLists.GetProductCategoryListsList();

         //### Populates the drop down list with PK and TITLE;
         lstProductCategoryList.DataValueField = "iProductCategoryListID";
         lstProductCategoryList.DataTextField = "strTitle";

         //### Bind the data to the list;
         lstProductCategoryList.DataBind();

         //### Add default select option;
         lstProductCategoryList.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    }
    
    private void popBiomassList()
    {
         DataTable dtBiomassListsList = new DataTable();
         lstBiomassList.DataSource = clsBiomassLists.GetBiomassListsList();

         //### Populates the drop down list with PK and TITLE;
         lstBiomassList.DataValueField = "iBiomassListID";
         lstBiomassList.DataTextField = "strTitle";

         //### Bind the data to the list;
         lstBiomassList.DataBind();

         //### Add default select option;
         lstBiomassList.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    }
    
    private void popCarbonSourceCategoryList()
    {
         DataTable dtCarbonSourceCategoryListsList = new DataTable();
         lstCarbonSourceCategoryList.DataSource = clsCarbonSourceCategoryLists.GetCarbonSourceCategoryListsList();

         //### Populates the drop down list with PK and TITLE;
         lstCarbonSourceCategoryList.DataValueField = "iCarbonSourceCategoryListID";
         lstCarbonSourceCategoryList.DataTextField = "strTitle";

         //### Bind the data to the list;
         lstCarbonSourceCategoryList.DataBind();

         //### Add default select option;
         lstCarbonSourceCategoryList.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    }
    
    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        clsYieldCoeficcients.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsYieldCoeficcients.iAddedBy = clsUsers.iUserID;
        clsYieldCoeficcients.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsYieldCoeficcients.iEditedBy = clsUsers.iUserID;
        clsYieldCoeficcients.strTitle = txtTitle.Text;
        clsYieldCoeficcients.strDescription = txtDescription.Text;
        clsYieldCoeficcients.dblPrice = Convert.ToDouble(txtPrice.Text);
        clsYieldCoeficcients.iProductCategoryListID = Convert.ToInt32(lstProductCategoryList.SelectedValue.ToString());
        clsYieldCoeficcients.iBiomassListID = Convert.ToInt32(lstBiomassList.SelectedValue.ToString());
        clsYieldCoeficcients.iCarbonSourceCategoryListID = Convert.ToInt32(lstCarbonSourceCategoryList.SelectedValue.ToString());
        clsYieldCoeficcients.dblPs = Convert.ToDouble(txtPs.Text);
        clsYieldCoeficcients.dblXs = Convert.ToDouble(txtXs.Text);
        clsYieldCoeficcients.dblXo = Convert.ToDouble(txtXo.Text);
        clsYieldCoeficcients.dblProductProductivity = Convert.ToDouble(txtProductProductivity.Text);
        clsYieldCoeficcients.strReferenceNotes = txtReferenceNotes.Text;

        clsYieldCoeficcients.Update();

        Session["dtYieldCoeficcientsList"] = null;

        //### Go back to view page
        Response.Redirect("frmYieldCoefficientsView.aspx");
    }

    #endregion
}