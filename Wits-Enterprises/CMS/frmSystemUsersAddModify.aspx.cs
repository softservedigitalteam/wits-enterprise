using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CMS_frmSystemUsersAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsSystemUsers clsSystemUsers;
    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        if (!IsPostBack)
        {

            //### If the iSystemUserID is passed through then we want to instantiate the object with that iSystemUserID
            if ((Request.QueryString["iSystemUserID"] != "") && (Request.QueryString["iSystemUserID"] != null))
            {
                clsSystemUsers = new clsSystemUsers(Convert.ToInt32(Request.QueryString["iSystemUserID"]));

                //### Populate the form
                popFormData();
            }
            else
            {
                clsSystemUsers = new clsSystemUsers();
            }
            Session["clsSystemUsers"] = clsSystemUsers;
        }
        else
        {
            clsSystemUsers = (clsSystemUsers)Session["clsSystemUsers"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmSystemUsersView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

       bCanSave = clsValidation.IsNullOrEmpty(txtFirstName, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(txtLastName, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(txtEmailAddress, bCanSave);

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">SystemUser added successfully</div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - SystemUser not added</div></div>";
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        txtFirstName.Text = "";
        clsValidation.SetValid(txtFirstName);
        txtLastName.Text = "";
        clsValidation.SetValid(txtLastName);
        txtContactNumber.Text = "";
        clsValidation.SetValid(txtContactNumber);
        txtEmailAddress.Text = "";
        clsValidation.SetValid(txtEmailAddress);
    }

    #endregion 

    #region POPULATE DATA METHODS

    private void popFormData()
    {
         txtFirstName.Text = clsSystemUsers.strFirstName;
         txtLastName.Text = clsSystemUsers.strLastName;
         txtContactNumber.Text = clsSystemUsers.strContactNumber;
         txtEmailAddress.Text = clsSystemUsers.strEmailAddress;
    }
    
    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        clsSystemUsers.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsSystemUsers.iAddedBy = clsUsers.iUserID;
        clsSystemUsers.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsSystemUsers.iEditedBy = clsUsers.iUserID;
        clsSystemUsers.strFirstName = txtFirstName.Text;
        clsSystemUsers.strLastName = txtLastName.Text;
        clsSystemUsers.strContactNumber = txtContactNumber.Text;
        clsSystemUsers.strEmailAddress = txtEmailAddress.Text;
        clsSystemUsers.strPassword = clsCommonFunctions.GetMd5Sum(txtEmailAddress.Text);
        clsSystemUsers.Update();

        Session["dtSystemUsersList"] = null;

        //### Go back to view page
        Response.Redirect("frmSystemUsersView.aspx");
    }

    #endregion
}