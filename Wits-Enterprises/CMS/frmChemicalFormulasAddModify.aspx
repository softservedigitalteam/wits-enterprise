<%@ Page Title="Chemical Formulas" Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="True" CodeBehind="frmChemicalFormulasAddModify.aspx.cs" Inherits="CMS_frmChemicalFormulasAddModify" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div id="mandatoryDiv" class="mandatoryInvalidDiv" runat="server" visible="false">
        <asp:Label ID="lblValidationMessage" runat="server"></asp:Label>
    </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Title:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtTitle" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,50)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Phase Consistency:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtPhaseConsistency" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,50)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Carbon:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtCarbon" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,50)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Hydrogen:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtHydrogenH" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,50)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Oxygen:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtOxygenO" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,50)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Nitrogen:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtNitrogenN" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,50)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Phosphorus:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtPhosphorusP" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,50)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Sulfur:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtSulferS" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,50)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Sodium:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtSodiumNa" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,50)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Chlorine:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtChlorineCl" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,50)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Potassium:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtPotassiumK" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,50)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Iron:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtIronFe" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,50)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Magnesium:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtMagnesiumMg" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,50)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Calcium:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtCalciumCa" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,50)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Silicon:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtSiliconSi" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,50)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Aluminium:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtAluminiumAl" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,50)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Copper:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtCopperCu" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,50)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Is An Estimate?:</div>
         <div class="fieldDiv">
             <asp:DropDownList ID="lstAnEstimate" runat="server" CssClass="roundedCornerDropDownList" onblur="setValid(this, 0);">
                 <asp:ListItem Text="True" Value="True"></asp:ListItem>
                 <asp:ListItem Text="False" Value="False"></asp:ListItem>
             </asp:DropDownList>

         </div>
         <br class="clearingSpacer" />
     </div>
    
    <div class="Line"></div>
    <div class="buttonsRightDiv">
        <asp:LinkButton ID="lnkbtnBack" runat="server" CssClass="backButton" onclick="lnkbtnBack_Click" OnClientClick='history.go(-1);return false;' />
        <asp:LinkButton ID="lnkbtnSave" runat="server" CssClass="saveButton" onclick="lnkbtnSave_Click" />
        <asp:LinkButton ID="lnkbtnClear" runat="server" CssClass="clearButton" onclick="lnkbtnClear_Click" />
    </div>
</asp:Content>
