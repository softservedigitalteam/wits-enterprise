using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CMS_frmCoolantsAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsCoolants clsCoolants;
    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        if (!IsPostBack)
        {

            //### If the iCoolantID is passed through then we want to instantiate the object with that iCoolantID
            if ((Request.QueryString["iCoolantID"] != "") && (Request.QueryString["iCoolantID"] != null))
            {
                clsCoolants = new clsCoolants(Convert.ToInt32(Request.QueryString["iCoolantID"]));

                //### Populate the form
                popFormData();
            }
            else
            {
                clsCoolants = new clsCoolants();
            }
            Session["clsCoolants"] = clsCoolants;
        }
        else
        {
            clsCoolants = (clsCoolants)Session["clsCoolants"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmCoolantsView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

       bCanSave = clsValidation.IsNullOrEmpty(txtTitle, bCanSave);

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">Coolant added successfully</div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - Coolant not added</div></div>";
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        txtTitle.Text = "";
        clsValidation.SetValid(txtTitle);
        txtDescription.Text = "";
        clsValidation.SetValid(txtDescription);
        txtPrice.Text = "";
        clsValidation.SetValid(txtPrice);
        txtOutletBiomassTemperature.Text = "";
        clsValidation.SetValid(txtOutletBiomassTemperature);
        txtEfficiency.Text = "";
        clsValidation.SetValid(txtEfficiency);
    }

    #endregion 

    #region POPULATE DATA METHODS

    private void popFormData()
    {
         txtTitle.Text = clsCoolants.strTitle;
         txtDescription.Text = clsCoolants.strDescription;
        txtPrice.Text = clsCoolants.dblPrice.ToString();
        txtOutletBiomassTemperature.Text = clsCoolants.dblOutletBiomassTemp.ToString();
        txtEfficiency.Text = clsCoolants.dblEfficiency.ToString();
    }
    
    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        clsCoolants.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsCoolants.iAddedBy = clsUsers.iUserID;
        clsCoolants.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsCoolants.iEditedBy = clsUsers.iUserID;
        clsCoolants.strTitle = txtTitle.Text;
        clsCoolants.strDescription = txtDescription.Text;
        clsCoolants.dblPrice = Convert.ToDouble(txtPrice.Text);
        clsCoolants.dblOutletBiomassTemp = Convert.ToDouble(txtOutletBiomassTemperature.Text);
        clsCoolants.dblEfficiency = Convert.ToDouble(txtEfficiency.Text);

        clsCoolants.Update();

        Session["dtCoolantsList"] = null;

        //### Go back to view page
        Response.Redirect("frmCoolantsView.aspx");
    }

    #endregion
}