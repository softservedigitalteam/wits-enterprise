using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CMS_frmTestCaseHistoryListsAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsTestCaseHistoryLists clsTestCaseHistoryLists;
    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        if (!IsPostBack)
        {

            //### If the iTestCaseHistoryListID is passed through then we want to instantiate the object with that iTestCaseHistoryListID
            if ((Request.QueryString["iTestCaseHistoryListID"] != "") && (Request.QueryString["iTestCaseHistoryListID"] != null))
            {
                clsTestCaseHistoryLists = new clsTestCaseHistoryLists(Convert.ToInt32(Request.QueryString["iTestCaseHistoryListID"]));

                //### Populate the form
                popFormData();
            }
            else
            {
                clsTestCaseHistoryLists = new clsTestCaseHistoryLists();
            }
            Session["clsTestCaseHistoryLists"] = clsTestCaseHistoryLists;
        }
        else
        {
            clsTestCaseHistoryLists = (clsTestCaseHistoryLists)Session["clsTestCaseHistoryLists"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmTestCaseHistoryListsView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

       bCanSave = clsValidation.IsNullOrEmpty(txtTitle, bCanSave);

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">TestCaseHistoryList added successfully</div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - TestCaseHistoryList not added</div></div>";
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        txtTitle.Text = "";
        clsValidation.SetValid(txtTitle);
    }

    #endregion 

    #region POPULATE DATA METHODS

    private void popFormData()
    {
         txtTitle.Text = clsTestCaseHistoryLists.strTitle;
    }
    
    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        clsTestCaseHistoryLists.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsTestCaseHistoryLists.iAddedBy = clsUsers.iUserID;
        clsTestCaseHistoryLists.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsTestCaseHistoryLists.iEditedBy = clsUsers.iUserID;
        clsTestCaseHistoryLists.strTitle = txtTitle.Text;

        clsTestCaseHistoryLists.Update();

        Session["dtTestCaseHistoryListsList"] = null;

        //### Go back to view page
        Response.Redirect("frmTestCaseHistoryListsView.aspx");
    }

    #endregion
}