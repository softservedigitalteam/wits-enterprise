
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;

/// <summary>
/// Summary description for clsAgitationsView
/// </summary>
public partial class CMS_clsAgitationsView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtAgitationsList;

    List<clsAgitations> glstAgitations;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
        }
            PopulateFormData();

    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string FilterExpression ="(strTitle) LIKE '%" + txtSearch.Text + "%'";
        DataTable dtAgitationsList = clsAgitations.GetAgitationsList(FilterExpression, "");

        Session["dtAgitationsList"] = dtAgitationsList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmAgitationsAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsAgitationsList object
        try
        {
            dtAgitationsList = new DataTable();

            if (Session["dtAgitationsList"] == null)
                dtAgitationsList = clsAgitations.GetAgitationsList();
            else
                dtAgitationsList = (DataTable)Session["dtAgitationsList"];

            dgrGrid.DataSource = dtAgitationsList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["AgitationsView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["AgitationsView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["AgitationsView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iAgitationID = int.Parse((sender as LinkButton).CommandArgument);

        clsAgitations.Delete(iAgitationID);

        PopulateFormData();
        Session["dtAgitationsList"] = null;
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["AgitationsView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtAgitationsList = new DataTable();

        if (Session["dtAgitationsList"] == null)
            dtAgitationsList = clsAgitations.GetAgitationsList();
        else
            dtAgitationsList = (DataTable)Session["dtAgitationsList"];

        DataView dvTemp = dtAgitationsList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtAgitationsList = dvTemp.ToTable();
        Session["dtAgitationsList"] = dtAgitationsList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iAgitationID
            int iAgitationID = 0;
            iAgitationID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmAgitationsAddModify.aspx?action=edit&iAgitationID=" + iAgitationID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    #endregion

    #region Agitations FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strTitle) LIKE '%" + prefixText + "%'";
        DataTable dtAgitations = clsAgitations.GetAgitationsList(FilterExpression, "");
        List<string> glstAgitations = new List<string>();

        if (dtAgitations.Rows.Count > 0)
        {
            foreach (DataRow dtrAgitations in dtAgitations.Rows)
            {
                glstAgitations.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrAgitations["strTitle"].ToString(), dtrAgitations["iAgitationID"].ToString()));
            }
        }
        else
            glstAgitations.Add("No Agitations Available.");
        strReturnList = glstAgitations.ToArray();
        return strReturnList;
    }

    #endregion
}
