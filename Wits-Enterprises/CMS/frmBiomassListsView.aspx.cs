
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;

/// <summary>
/// Summary description for clsBiomassListsView
/// </summary>
public partial class CMS_clsBiomassListsView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtBiomassListsList;

    List<clsBiomassLists> glstBiomassLists;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
        }
            PopulateFormData();

    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string FilterExpression ="(strTitle) LIKE '%" + txtSearch.Text + "%'";
        DataTable dtBiomassListsList = clsBiomassLists.GetBiomassListsList(FilterExpression, "");

        Session["dtBiomassListsList"] = dtBiomassListsList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmBiomassListsAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsBiomassListsList object
        try
        {
            dtBiomassListsList = new DataTable();

            if (Session["dtBiomassListsList"] == null)
                dtBiomassListsList = clsBiomassLists.GetBiomassListsList();
            else
                dtBiomassListsList = (DataTable)Session["dtBiomassListsList"];

            dgrGrid.DataSource = dtBiomassListsList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["BiomassListsView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["BiomassListsView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["BiomassListsView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iBiomassListID = int.Parse((sender as LinkButton).CommandArgument);

        clsBiomassLists.Delete(iBiomassListID);

        PopulateFormData();
        Session["dtBiomassListsList"] = null;
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["BiomassListsView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtBiomassListsList = new DataTable();

        if (Session["dtBiomassListsList"] == null)
            dtBiomassListsList = clsBiomassLists.GetBiomassListsList();
        else
            dtBiomassListsList = (DataTable)Session["dtBiomassListsList"];

        DataView dvTemp = dtBiomassListsList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtBiomassListsList = dvTemp.ToTable();
        Session["dtBiomassListsList"] = dtBiomassListsList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iBiomassListID
            int iBiomassListID = 0;
            iBiomassListID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmBiomassListsAddModify.aspx?action=edit&iBiomassListID=" + iBiomassListID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    #endregion

    #region BiomassLists FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strTitle) LIKE '%" + prefixText + "%'";
        DataTable dtBiomassLists = clsBiomassLists.GetBiomassListsList(FilterExpression, "");
        List<string> glstBiomassLists = new List<string>();

        if (dtBiomassLists.Rows.Count > 0)
        {
            foreach (DataRow dtrBiomassLists in dtBiomassLists.Rows)
            {
                glstBiomassLists.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrBiomassLists["strTitle"].ToString(), dtrBiomassLists["iBiomassListID"].ToString()));
            }
        }
        else
            glstBiomassLists.Add("No BiomassLists Available.");
        strReturnList = glstBiomassLists.ToArray();
        return strReturnList;
    }

    #endregion
}
