
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;

/// <summary>
/// Summary description for clsGrowthRatesView
/// </summary>
public partial class CMS_clsGrowthRatesView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtGrowthRatesList;

    List<clsGrowthRates> glstGrowthRates;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
        }
            PopulateFormData();

    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string FilterExpression ="(strTitle) LIKE '%" + txtSearch.Text + "%'";
        DataTable dtGrowthRatesList = clsGrowthRates.GetGrowthRatesList(FilterExpression, "");

        Session["dtGrowthRatesList"] = dtGrowthRatesList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmGrowthRatesAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsGrowthRatesList object
        try
        {
            dtGrowthRatesList = new DataTable();

            if (Session["dtGrowthRatesList"] == null)
                dtGrowthRatesList = clsGrowthRates.GetGrowthRatesList();
            else
                dtGrowthRatesList = (DataTable)Session["dtGrowthRatesList"];

            dgrGrid.DataSource = dtGrowthRatesList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["GrowthRatesView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["GrowthRatesView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["GrowthRatesView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iGrowthRateID = int.Parse((sender as LinkButton).CommandArgument);

        clsGrowthRates.Delete(iGrowthRateID);

        PopulateFormData();
        Session["dtGrowthRatesList"] = null;
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["GrowthRatesView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtGrowthRatesList = new DataTable();

        if (Session["dtGrowthRatesList"] == null)
            dtGrowthRatesList = clsGrowthRates.GetGrowthRatesList();
        else
            dtGrowthRatesList = (DataTable)Session["dtGrowthRatesList"];

        DataView dvTemp = dtGrowthRatesList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtGrowthRatesList = dvTemp.ToTable();
        Session["dtGrowthRatesList"] = dtGrowthRatesList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iGrowthRateID
            int iGrowthRateID = 0;
            iGrowthRateID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmGrowthRatesAddModify.aspx?action=edit&iGrowthRateID=" + iGrowthRateID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    #endregion

    #region GrowthRates FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strTitle) LIKE '%" + prefixText + "%'";
        DataTable dtGrowthRates = clsGrowthRates.GetGrowthRatesList(FilterExpression, "");
        List<string> glstGrowthRates = new List<string>();

        if (dtGrowthRates.Rows.Count > 0)
        {
            foreach (DataRow dtrGrowthRates in dtGrowthRates.Rows)
            {
                glstGrowthRates.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrGrowthRates["strTitle"].ToString(), dtrGrowthRates["iGrowthRateID"].ToString()));
            }
        }
        else
            glstGrowthRates.Add("No GrowthRates Available.");
        strReturnList = glstGrowthRates.ToArray();
        return strReturnList;
    }

    #endregion
}
