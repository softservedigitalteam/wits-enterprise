using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CMS_frmSubscriptionTypesAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsSubscriptionTypes clsSubscriptionTypes;
    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        if (!IsPostBack)
        {

            //### If the iSubscriptionTypeID is passed through then we want to instantiate the object with that iSubscriptionTypeID
            if ((Request.QueryString["iSubscriptionTypeID"] != "") && (Request.QueryString["iSubscriptionTypeID"] != null))
            {
                clsSubscriptionTypes = new clsSubscriptionTypes(Convert.ToInt32(Request.QueryString["iSubscriptionTypeID"]));

                //### Populate the form
                popFormData();
            }
            else
            {
                clsSubscriptionTypes = new clsSubscriptionTypes();
            }
            Session["clsSubscriptionTypes"] = clsSubscriptionTypes;
        }
        else
        {
            clsSubscriptionTypes = (clsSubscriptionTypes)Session["clsSubscriptionTypes"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmSubscriptionTypesView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

       bCanSave = clsValidation.IsNullOrEmpty(txtTitle, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtMonthlyCost, bCanSave);

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">SubscriptionType added successfully</div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - SubscriptionType not added</div></div>";
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        txtTitle.Text = "";
        clsValidation.SetValid(txtTitle);
        txtMonthlyCost.Text = "";
        clsValidation.SetValid(txtMonthlyCost);
    }

    #endregion 

    #region POPULATE DATA METHODS

    private void popFormData()
    {
         txtTitle.Text = clsSubscriptionTypes.strTitle;
        txtMonthlyCost.Text = clsSubscriptionTypes.dblMonthlyCost.ToString();
    }
    
    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        clsSubscriptionTypes.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsSubscriptionTypes.iAddedBy = clsUsers.iUserID;
        clsSubscriptionTypes.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsSubscriptionTypes.iEditedBy = clsUsers.iUserID;
        clsSubscriptionTypes.strTitle = txtTitle.Text;
        clsSubscriptionTypes.dblMonthlyCost = Convert.ToDouble(txtMonthlyCost.Text);

        clsSubscriptionTypes.Update();

        Session["dtSubscriptionTypesList"] = null;

        //### Go back to view page
        Response.Redirect("frmSubscriptionTypesView.aspx");
    }

    #endregion
}