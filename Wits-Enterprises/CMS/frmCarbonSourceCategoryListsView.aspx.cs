
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;

/// <summary>
/// Summary description for clsCarbonSourceCategoryListsView
/// </summary>
public partial class CMS_clsCarbonSourceCategoryListsView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtCarbonSourceCategoryListsList;

    List<clsCarbonSourceCategoryLists> glstCarbonSourceCategoryLists;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
        }
            PopulateFormData();

    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string FilterExpression ="(strTitle) LIKE '%" + txtSearch.Text + "%'";
        DataTable dtCarbonSourceCategoryListsList = clsCarbonSourceCategoryLists.GetCarbonSourceCategoryListsList(FilterExpression, "");

        Session["dtCarbonSourceCategoryListsList"] = dtCarbonSourceCategoryListsList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmCarbonSourceCategoryListsAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsCarbonSourceCategoryListsList object
        try
        {
            dtCarbonSourceCategoryListsList = new DataTable();

            if (Session["dtCarbonSourceCategoryListsList"] == null)
                dtCarbonSourceCategoryListsList = clsCarbonSourceCategoryLists.GetCarbonSourceCategoryListsList();
            else
                dtCarbonSourceCategoryListsList = (DataTable)Session["dtCarbonSourceCategoryListsList"];

            dgrGrid.DataSource = dtCarbonSourceCategoryListsList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["CarbonSourceCategoryListsView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["CarbonSourceCategoryListsView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["CarbonSourceCategoryListsView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iCarbonSourceCategoryListID = int.Parse((sender as LinkButton).CommandArgument);

        clsCarbonSourceCategoryLists.Delete(iCarbonSourceCategoryListID);

        PopulateFormData();
        Session["dtCarbonSourceCategoryListsList"] = null;
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["CarbonSourceCategoryListsView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtCarbonSourceCategoryListsList = new DataTable();

        if (Session["dtCarbonSourceCategoryListsList"] == null)
            dtCarbonSourceCategoryListsList = clsCarbonSourceCategoryLists.GetCarbonSourceCategoryListsList();
        else
            dtCarbonSourceCategoryListsList = (DataTable)Session["dtCarbonSourceCategoryListsList"];

        DataView dvTemp = dtCarbonSourceCategoryListsList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtCarbonSourceCategoryListsList = dvTemp.ToTable();
        Session["dtCarbonSourceCategoryListsList"] = dtCarbonSourceCategoryListsList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iCarbonSourceCategoryListID
            int iCarbonSourceCategoryListID = 0;
            iCarbonSourceCategoryListID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmCarbonSourceCategoryListsAddModify.aspx?action=edit&iCarbonSourceCategoryListID=" + iCarbonSourceCategoryListID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    #endregion

    #region CarbonSourceCategoryLists FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strTitle) LIKE '%" + prefixText + "%'";
        DataTable dtCarbonSourceCategoryLists = clsCarbonSourceCategoryLists.GetCarbonSourceCategoryListsList(FilterExpression, "");
        List<string> glstCarbonSourceCategoryLists = new List<string>();

        if (dtCarbonSourceCategoryLists.Rows.Count > 0)
        {
            foreach (DataRow dtrCarbonSourceCategoryLists in dtCarbonSourceCategoryLists.Rows)
            {
                glstCarbonSourceCategoryLists.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrCarbonSourceCategoryLists["strTitle"].ToString(), dtrCarbonSourceCategoryLists["iCarbonSourceCategoryListID"].ToString()));
            }
        }
        else
            glstCarbonSourceCategoryLists.Add("No CarbonSourceCategoryLists Available.");
        strReturnList = glstCarbonSourceCategoryLists.ToArray();
        return strReturnList;
    }

    #endregion
}
