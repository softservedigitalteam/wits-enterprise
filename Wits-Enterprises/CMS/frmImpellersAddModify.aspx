<%@ Page Title="Impellers" Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="True" CodeBehind="frmImpellersAddModify.aspx.cs" Inherits="CMS_frmImpellersAddModify" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div id="mandatoryDiv" class="mandatoryInvalidDiv" runat="server" visible="false">
        <asp:Label ID="lblValidationMessage" runat="server"></asp:Label>
    </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Title:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtTitle" runat="server" CssClass="roundedCornerTextBox" onKeyUp="return SetMaxLength(this,100)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Description:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtDescription" runat="server" CssClass="roundedCornerTextBoxMultiLine8" onKeyUp="return SetMaxLength(this,2500)" TextMode="MultiLine" Rows="8" />
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Price:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtPrice" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,50)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Number of Tanks:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtNumberOfTanks" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,50)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>
    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Length By Diameter of Reaction:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtLengthByDiameterOfReaction" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,50)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>
    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Impeller Speed:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtImpellerSpeed" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,50)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>
    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Number of Impeller Blades:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtNumberOfImpellerBlades" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,50)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>
    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Diameter of Impeller By Diameter of Reactor:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtDiameterOfImpellerByDiameterReactor" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,50)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>
    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Impeller Blade Width By Reactor Length:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtImpellerBladeWidthByReactorLength" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,50)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>
    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Power Per Unit Volume:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtPowerPerUnitVolume" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,50)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>
    
    <div class="Line"></div>
    <div class="buttonsRightDiv">
        <asp:LinkButton ID="lnkbtnBack" runat="server" CssClass="backButton" onclick="lnkbtnBack_Click" OnClientClick='history.go(-1);return false;' />
        <asp:LinkButton ID="lnkbtnSave" runat="server" CssClass="saveButton" onclick="lnkbtnSave_Click" />
        <asp:LinkButton ID="lnkbtnClear" runat="server" CssClass="clearButton" onclick="lnkbtnClear_Click" />
    </div>
</asp:Content>
