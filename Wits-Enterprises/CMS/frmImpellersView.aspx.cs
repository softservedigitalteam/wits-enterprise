
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;

/// <summary>
/// Summary description for clsImpellersView
/// </summary>
public partial class CMS_clsImpellersView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtImpellersList;

    List<clsImpellers> glstImpellers;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
        }
            PopulateFormData();

    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string FilterExpression ="(strTitle) LIKE '%" + txtSearch.Text + "%'";
        DataTable dtImpellersList = clsImpellers.GetImpellersList(FilterExpression, "");

        Session["dtImpellersList"] = dtImpellersList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmImpellersAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsImpellersList object
        try
        {
            dtImpellersList = new DataTable();

            if (Session["dtImpellersList"] == null)
                dtImpellersList = clsImpellers.GetImpellersList();
            else
                dtImpellersList = (DataTable)Session["dtImpellersList"];

            dgrGrid.DataSource = dtImpellersList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["ImpellersView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["ImpellersView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["ImpellersView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iImpellerID = int.Parse((sender as LinkButton).CommandArgument);

        clsImpellers.Delete(iImpellerID);

        PopulateFormData();
        Session["dtImpellersList"] = null;
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["ImpellersView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtImpellersList = new DataTable();

        if (Session["dtImpellersList"] == null)
            dtImpellersList = clsImpellers.GetImpellersList();
        else
            dtImpellersList = (DataTable)Session["dtImpellersList"];

        DataView dvTemp = dtImpellersList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtImpellersList = dvTemp.ToTable();
        Session["dtImpellersList"] = dtImpellersList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iImpellerID
            int iImpellerID = 0;
            iImpellerID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmImpellersAddModify.aspx?action=edit&iImpellerID=" + iImpellerID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    #endregion

    #region Impellers FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strTitle) LIKE '%" + prefixText + "%'";
        DataTable dtImpellers = clsImpellers.GetImpellersList(FilterExpression, "");
        List<string> glstImpellers = new List<string>();

        if (dtImpellers.Rows.Count > 0)
        {
            foreach (DataRow dtrImpellers in dtImpellers.Rows)
            {
                glstImpellers.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrImpellers["strTitle"].ToString(), dtrImpellers["iImpellerID"].ToString()));
            }
        }
        else
            glstImpellers.Add("No Impellers Available.");
        strReturnList = glstImpellers.ToArray();
        return strReturnList;
    }

    #endregion
}
