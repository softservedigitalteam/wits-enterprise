
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;

/// <summary>
/// Summary description for clsSubscriptionTypesView
/// </summary>
public partial class CMS_clsSubscriptionTypesView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtSubscriptionTypesList;

    List<clsSubscriptionTypes> glstSubscriptionTypes;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
        }
            PopulateFormData();

    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string FilterExpression ="(strTitle) LIKE '%" + txtSearch.Text + "%'";
        DataTable dtSubscriptionTypesList = clsSubscriptionTypes.GetSubscriptionTypesList(FilterExpression, "");

        Session["dtSubscriptionTypesList"] = dtSubscriptionTypesList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmSubscriptionTypesAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsSubscriptionTypesList object
        try
        {
            dtSubscriptionTypesList = new DataTable();

            if (Session["dtSubscriptionTypesList"] == null)
                dtSubscriptionTypesList = clsSubscriptionTypes.GetSubscriptionTypesList();
            else
                dtSubscriptionTypesList = (DataTable)Session["dtSubscriptionTypesList"];

            dgrGrid.DataSource = dtSubscriptionTypesList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["SubscriptionTypesView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["SubscriptionTypesView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["SubscriptionTypesView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iSubscriptionTypeID = int.Parse((sender as LinkButton).CommandArgument);

        clsSubscriptionTypes.Delete(iSubscriptionTypeID);

        PopulateFormData();
        Session["dtSubscriptionTypesList"] = null;
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["SubscriptionTypesView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtSubscriptionTypesList = new DataTable();

        if (Session["dtSubscriptionTypesList"] == null)
            dtSubscriptionTypesList = clsSubscriptionTypes.GetSubscriptionTypesList();
        else
            dtSubscriptionTypesList = (DataTable)Session["dtSubscriptionTypesList"];

        DataView dvTemp = dtSubscriptionTypesList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtSubscriptionTypesList = dvTemp.ToTable();
        Session["dtSubscriptionTypesList"] = dtSubscriptionTypesList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iSubscriptionTypeID
            int iSubscriptionTypeID = 0;
            iSubscriptionTypeID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmSubscriptionTypesAddModify.aspx?action=edit&iSubscriptionTypeID=" + iSubscriptionTypeID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    #endregion

    #region SubscriptionTypes FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strTitle) LIKE '%" + prefixText + "%'";
        DataTable dtSubscriptionTypes = clsSubscriptionTypes.GetSubscriptionTypesList(FilterExpression, "");
        List<string> glstSubscriptionTypes = new List<string>();

        if (dtSubscriptionTypes.Rows.Count > 0)
        {
            foreach (DataRow dtrSubscriptionTypes in dtSubscriptionTypes.Rows)
            {
                glstSubscriptionTypes.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrSubscriptionTypes["strTitle"].ToString(), dtrSubscriptionTypes["iSubscriptionTypeID"].ToString()));
            }
        }
        else
            glstSubscriptionTypes.Add("No SubscriptionTypes Available.");
        strReturnList = glstSubscriptionTypes.ToArray();
        return strReturnList;
    }

    #endregion
}
