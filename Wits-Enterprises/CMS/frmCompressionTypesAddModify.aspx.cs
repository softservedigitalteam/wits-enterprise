using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CMS_frmCompressionTypesAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsCompressionTypes clsCompressionTypes;
    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        if (!IsPostBack)
        {

            //### If the iCompressionTypeID is passed through then we want to instantiate the object with that iCompressionTypeID
            if ((Request.QueryString["iCompressionTypeID"] != "") && (Request.QueryString["iCompressionTypeID"] != null))
            {
                clsCompressionTypes = new clsCompressionTypes(Convert.ToInt32(Request.QueryString["iCompressionTypeID"]));

                //### Populate the form
                popFormData();
            }
            else
            {
                clsCompressionTypes = new clsCompressionTypes();
            }
            Session["clsCompressionTypes"] = clsCompressionTypes;
        }
        else
        {
            clsCompressionTypes = (clsCompressionTypes)Session["clsCompressionTypes"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmCompressionTypesView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

       bCanSave = clsValidation.IsNullOrEmpty(txtTitle, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtPrice, bCanSave);

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">CompressionType added successfully</div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - CompressionType not added</div></div>";
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        txtTitle.Text = "";
        clsValidation.SetValid(txtTitle);
        txtDescription.Text = "";
        clsValidation.SetValid(txtDescription);
        txtPrice.Text = "";
        clsValidation.SetValid(txtPrice);
    }

    #endregion 

    #region POPULATE DATA METHODS

    private void popFormData()
    {
         txtTitle.Text = clsCompressionTypes.strTitle;
         txtDescription.Text = clsCompressionTypes.strDescription;
        txtPrice.Text = clsCompressionTypes.dblPrice.ToString();
        
    }
    
    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        clsCompressionTypes.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsCompressionTypes.iAddedBy = clsUsers.iUserID;
        clsCompressionTypes.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsCompressionTypes.iEditedBy = clsUsers.iUserID;
        clsCompressionTypes.strTitle = txtTitle.Text;
        clsCompressionTypes.strDescription = txtDescription.Text;
        clsCompressionTypes.dblPrice = Convert.ToDouble(txtPrice.Text.ToString());

        clsCompressionTypes.Update();

        Session["dtCompressionTypesList"] = null;

        //### Go back to view page
        Response.Redirect("frmCompressionTypesView.aspx");
    }

    #endregion
}