using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CMS_frmImpellersAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsImpellers clsImpellers;
    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        if (!IsPostBack)
        {

            //### If the iImpellerID is passed through then we want to instantiate the object with that iImpellerID
            if ((Request.QueryString["iImpellerID"] != "") && (Request.QueryString["iImpellerID"] != null))
            {
                clsImpellers = new clsImpellers(Convert.ToInt32(Request.QueryString["iImpellerID"]));

                //### Populate the form
                popFormData();
            }
            else
            {
                clsImpellers = new clsImpellers();
            }
            Session["clsImpellers"] = clsImpellers;
        }
        else
        {
            clsImpellers = (clsImpellers)Session["clsImpellers"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmImpellersView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

       bCanSave = clsValidation.IsNullOrEmpty(txtTitle, bCanSave);

        bCanSave = clsValidation.IsNullOrEmpty(txtPrice, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtNumberOfTanks, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtLengthByDiameterOfReaction, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtImpellerSpeed, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtNumberOfImpellerBlades, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtDiameterOfImpellerByDiameterReactor, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtImpellerBladeWidthByReactorLength, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtPowerPerUnitVolume, bCanSave);

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">Impeller added successfully</div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - Impeller not added</div></div>";
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        txtTitle.Text = "";
        clsValidation.SetValid(txtTitle);
        txtDescription.Text = "";
        clsValidation.SetValid(txtDescription);

        txtPrice.Text = "";
        clsValidation.SetValid(txtPrice);
        txtNumberOfTanks.Text = "";
        clsValidation.SetValid(txtNumberOfTanks);
        txtLengthByDiameterOfReaction.Text = "";
        clsValidation.SetValid(txtLengthByDiameterOfReaction);
        txtImpellerSpeed.Text = "";
        clsValidation.SetValid(txtImpellerSpeed);
        txtNumberOfImpellerBlades.Text = "";
        clsValidation.SetValid(txtNumberOfImpellerBlades);
        txtDiameterOfImpellerByDiameterReactor.Text = "";
        clsValidation.SetValid(txtDiameterOfImpellerByDiameterReactor);
        txtImpellerBladeWidthByReactorLength.Text = "";
        clsValidation.SetValid(txtImpellerBladeWidthByReactorLength);
        txtPowerPerUnitVolume.Text = "";
        clsValidation.SetValid(txtPowerPerUnitVolume);
    }

    #endregion 

    #region POPULATE DATA METHODS

    private void popFormData()
    {
         txtTitle.Text = clsImpellers.strTitle;
         txtDescription.Text = clsImpellers.strDescription;

        txtPrice.Text = clsImpellers.dblPrice.ToString();
        txtNumberOfTanks.Text = clsImpellers.dblNumberOfTanks.ToString();
        txtLengthByDiameterOfReaction.Text = clsImpellers.dblLengthByDiameterOfReaction.ToString();
        txtImpellerSpeed.Text = clsImpellers.dblImpellerSpeed.ToString();
        txtNumberOfImpellerBlades.Text = clsImpellers.dblNumberOfImpellerBlades.ToString();
        txtDiameterOfImpellerByDiameterReactor.Text = clsImpellers.dblDiameterOfImpellerByDiameterOfReactor.ToString();
        txtImpellerBladeWidthByReactorLength.Text = clsImpellers.dblImpellerBladeWidthByReactorLength.ToString();
        txtPowerPerUnitVolume.Text = clsImpellers.dblPowerPerUnitVolume.ToString();
    }
    
    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        clsImpellers.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsImpellers.iAddedBy = clsUsers.iUserID;
        clsImpellers.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsImpellers.iEditedBy = clsUsers.iUserID;
        clsImpellers.strTitle = txtTitle.Text;
        clsImpellers.strDescription = txtDescription.Text;

        clsImpellers.dblPrice = Convert.ToDouble(txtPrice.Text);
        clsImpellers.dblNumberOfTanks = Convert.ToDouble(txtNumberOfTanks.Text);
        clsImpellers.dblLengthByDiameterOfReaction = Convert.ToDouble(txtLengthByDiameterOfReaction.Text);
        clsImpellers.dblImpellerSpeed = Convert.ToDouble(txtImpellerSpeed.Text);
        clsImpellers.dblNumberOfImpellerBlades = Convert.ToDouble(txtNumberOfImpellerBlades.Text);
        clsImpellers.dblDiameterOfImpellerByDiameterOfReactor = Convert.ToDouble(txtDiameterOfImpellerByDiameterReactor.Text);
        clsImpellers.dblImpellerBladeWidthByReactorLength = Convert.ToDouble(txtImpellerBladeWidthByReactorLength.Text);
        clsImpellers.dblPowerPerUnitVolume = Convert.ToDouble(txtPowerPerUnitVolume.Text);

        clsImpellers.Update();

        Session["dtImpellersList"] = null;

        //### Go back to view page
        Response.Redirect("frmImpellersView.aspx");
    }

    #endregion
}