
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;

/// <summary>
/// Summary description for clsYieldCoeficcientsView
/// </summary>
public partial class CMS_clsYieldCoeficcientsView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtYieldCoeficcientsList;

    List<clsYieldCoeficcients> glstYieldCoeficcients;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
        }
            PopulateFormData();

    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string FilterExpression ="(strTitle) LIKE '%" + txtSearch.Text + "%'";
        DataTable dtYieldCoeficcientsList = clsYieldCoeficcients.GetYieldCoeficcientsList(FilterExpression, "");

        Session["dtYieldCoeficcientsList"] = dtYieldCoeficcientsList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmYieldCoefficientsAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsYieldCoeficcientsList object
        try
        {
            dtYieldCoeficcientsList = new DataTable();

            if (Session["dtYieldCoeficcientsList"] == null)
                dtYieldCoeficcientsList = clsYieldCoeficcients.GetYieldCoeficcientsList();
            else
                dtYieldCoeficcientsList = (DataTable)Session["dtYieldCoeficcientsList"];

            dgrGrid.DataSource = dtYieldCoeficcientsList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["YieldCoeficcientsView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["YieldCoeficcientsView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["YieldCoeficcientsView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iYieldCoeficcientID = int.Parse((sender as LinkButton).CommandArgument);

        clsYieldCoeficcients.Delete(iYieldCoeficcientID);

        PopulateFormData();
        Session["dtYieldCoeficcientsList"] = null;
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["YieldCoeficcientsView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtYieldCoeficcientsList = new DataTable();

        if (Session["dtYieldCoeficcientsList"] == null)
            dtYieldCoeficcientsList = clsYieldCoeficcients.GetYieldCoeficcientsList();
        else
            dtYieldCoeficcientsList = (DataTable)Session["dtYieldCoeficcientsList"];

        DataView dvTemp = dtYieldCoeficcientsList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtYieldCoeficcientsList = dvTemp.ToTable();
        Session["dtYieldCoeficcientsList"] = dtYieldCoeficcientsList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iYieldCoeficcientID
            int iYieldCoeficcientID = 0;
            iYieldCoeficcientID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmYieldCoefficientsAddModify.aspx?action=edit&iYieldCoeficcientID=" + iYieldCoeficcientID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    #endregion

    #region YieldCoeficcients FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strTitle) LIKE '%" + prefixText + "%'";
        DataTable dtYieldCoeficcients = clsYieldCoeficcients.GetYieldCoeficcientsList(FilterExpression, "");
        List<string> glstYieldCoeficcients = new List<string>();

        if (dtYieldCoeficcients.Rows.Count > 0)
        {
            foreach (DataRow dtrYieldCoeficcients in dtYieldCoeficcients.Rows)
            {
                glstYieldCoeficcients.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrYieldCoeficcients["strTitle"].ToString(), dtrYieldCoeficcients["iYieldCoeficcientID"].ToString()));
            }
        }
        else
            glstYieldCoeficcients.Add("No YieldCoeficcients Available.");
        strReturnList = glstYieldCoeficcients.ToArray();
        return strReturnList;
    }

    #endregion
}
