using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CMS_frmCarbonSourceCategoryListsAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsCarbonSourceCategoryLists clsCarbonSourceCategoryLists;
    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        if (!IsPostBack)
        {
             popChemicalFormula();
             //popLevel();

            //### If the iCarbonSourceCategoryListID is passed through then we want to instantiate the object with that iCarbonSourceCategoryListID
            if ((Request.QueryString["iCarbonSourceCategoryListID"] != "") && (Request.QueryString["iCarbonSourceCategoryListID"] != null))
            {
                clsCarbonSourceCategoryLists = new clsCarbonSourceCategoryLists(Convert.ToInt32(Request.QueryString["iCarbonSourceCategoryListID"]));

                //### Populate the form
                popFormData();
            }
            else
            {
                PopCarbonSourceCategories(0);
                clsCarbonSourceCategoryLists = new clsCarbonSourceCategoryLists();
            }
            Session["clsCarbonSourceCategoryLists"] = clsCarbonSourceCategoryLists;
        }
        else
        {
            clsCarbonSourceCategoryLists = (clsCarbonSourceCategoryLists)Session["clsCarbonSourceCategoryLists"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmCarbonSourceCategoryListsView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

       bCanSave = clsValidation.IsNullOrEmpty(txtTitle, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(lstChemicalFormula, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtPrice, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtDensity, bCanSave);
        //bCanSave = clsValidation.IsNullOrEmpty(lstLevel, bCanSave);

        if (treeCarbonSourceCategories.SelectedNode == null)
        {
            bCanSave = false;
        }

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">CarbonSourceCategoryList added successfully</div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - CarbonSourceCategoryList not added</div></div>";
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        txtTitle.Text = "";
        clsValidation.SetValid(txtTitle);
        txtDescription.Text = "";
        clsValidation.SetValid(txtDescription);
        lstChemicalFormula.SelectedValue = "0";
        clsValidation.SetValid(lstChemicalFormula);
        txtPrice.Text = "";
        clsValidation.SetValid(txtPrice);
        txtDensity.Text = "";
        clsValidation.SetValid(txtDensity);
        //lstLevel.SelectedValue = "0";
        //clsValidation.SetValid(lstLevel);
    }

    #endregion 

    #region POPULATE DATA METHODS

    private void popFormData()
    {
         txtTitle.Text = clsCarbonSourceCategoryLists.strTitle;
        txtDescription.Text = clsCarbonSourceCategoryLists.strDescription;
         lstChemicalFormula.SelectedValue = clsCarbonSourceCategoryLists.iChemicalFormulaID.ToString();
        txtPrice.Text = clsCarbonSourceCategoryLists.dblPrice.ToString();
        txtDensity.Text = clsCarbonSourceCategoryLists.dblDensity.ToString();
         //lstLevel.SelectedValue = clsCarbonSourceCategoryLists.iLevelID.ToString();
        PopCarbonSourceCategories(clsCarbonSourceCategoryLists.iLevelID);
    }
    
    private void popChemicalFormula()
    {
         DataTable dtChemicalFormulasList = new DataTable();
         lstChemicalFormula.DataSource = clsChemicalFormulas.GetChemicalFormulasList();

         //### Populates the drop down list with PK and TITLE;
         lstChemicalFormula.DataValueField = "iChemicalFormulaID";
         lstChemicalFormula.DataTextField = "strTitle";

         //### Bind the data to the list;
         lstChemicalFormula.DataBind();

         //### Add default select option;
         lstChemicalFormula.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    }

    /// <summary>
    /// Populate the Tree view with the category types
    /// </summary>
    private void PopCarbonSourceCategories(int clsCarbonSourceCategoriesiCategoryID)
    {
        treeCarbonSourceCategories.Nodes.Clear();
        TreeNode Mainnode = new TreeNode();
        bool bIsSelected = false;
        Mainnode.Value = "0";
        Mainnode.Text = "Top level category";
        if (clsCarbonSourceCategoriesiCategoryID == 0)
        {
            Mainnode.Selected = true;
        }
        treeCarbonSourceCategories.Nodes.Add(Mainnode);

        DataTable DT = new DataTable();
        DT = clsCarbonSourceCategoryLists.GetCarbonSourceCategoryListsList("iLevelID = 0", "strTitle");
        foreach (DataRow r in DT.Rows)
        {
            int iCarbonSourceCategoryListID = 0;
            iCarbonSourceCategoryListID = int.Parse(r["iCarbonSourceCategoryListID"].ToString());
            TreeNode node = new TreeNode(r["strTitle"].ToString(), r["iCarbonSourceCategoryListID"].ToString());
            if (clsCarbonSourceCategoriesiCategoryID == int.Parse(r["iCarbonSourceCategoryListID"].ToString()))
            {
                node.Selected = true;
            }

            //### Now we call the PopChildCategories method to build the sub categories
            PopChildCategories(ref node, int.Parse(r["iCarbonSourceCategoryListID"].ToString()), clsCarbonSourceCategoriesiCategoryID, ref bIsSelected);
            if (bIsSelected)
            {
                node.Expanded = true;
            }
            else
            {
                node.Expanded = false;
            }
            treeCarbonSourceCategories.Nodes.Add(node);
        }
    }

    /// <summary>
    /// Populate the Subcategories for the Category Treeview.
    /// This Method will call itself to build the Subcategory of the current Subcategory
    /// </summary>
    /// <param name="ChildNode">The current Top level category</param>
    /// <param name="iCarbonSourceCategoryListID">The ID of the Category</param>
    /// <param name="clsCarbonSourceCategoriesiCategoryID">The ID of the clsCategories.iCategoryID</param>
    private void PopChildCategories(ref TreeNode ChildNode, int iCarbonSourceCategoryListID, int clsCarbonSourceCategoriesiCategoryID, ref bool bIsSelected)
    {
        DataTable DT = new DataTable();
        DT = clsCarbonSourceCategoryLists.GetCarbonSourceCategoryListsList("iLevelID = " + iCarbonSourceCategoryListID.ToString(), "strTitle");

        foreach (DataRow r in DT.Rows)
        {
            TreeNode node = new TreeNode(r["strTitle"].ToString(), r["iCarbonSourceCategoryListID"].ToString());
            if (clsCarbonSourceCategoriesiCategoryID == int.Parse(r["iCarbonSourceCategoryListID"].ToString()))
            {
                node.Selected = true;
                bIsSelected = true;
            }
            if (bIsSelected)
            {
                node.Expanded = true;
            }
            ChildNode.ChildNodes.Add(node);

            //### Now we call this methode again to build the subsubcategories
            PopChildCategories(ref node, int.Parse(r["iCarbonSourceCategoryListID"].ToString()), clsCarbonSourceCategoriesiCategoryID, ref bIsSelected);
        }
    }

    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        clsCarbonSourceCategoryLists.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsCarbonSourceCategoryLists.iAddedBy = clsUsers.iUserID;
        clsCarbonSourceCategoryLists.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsCarbonSourceCategoryLists.iEditedBy = clsUsers.iUserID;
        clsCarbonSourceCategoryLists.strTitle = txtTitle.Text;
        clsCarbonSourceCategoryLists.strDescription = txtDescription.Text;
        clsCarbonSourceCategoryLists.iChemicalFormulaID = Convert.ToInt32(lstChemicalFormula.SelectedValue.ToString());
        int iLevelID;

        if (treeCarbonSourceCategories.Nodes.Count != 0)
        {
            if (treeCarbonSourceCategories.SelectedNode != null)
            {
                iLevelID = int.Parse(treeCarbonSourceCategories.SelectedNode.Value);
            }
            else
            {
                iLevelID = 0;
            }
        }
        else
        {
            iLevelID = 0;
        }

        clsCarbonSourceCategoryLists.iLevelID = iLevelID;
        clsCarbonSourceCategoryLists.dblPrice = Convert.ToDouble(txtPrice.Text.ToString());
        clsCarbonSourceCategoryLists.dblDensity = Convert.ToDouble(txtDensity.Text.ToString());
        //clsCarbonSourceCategoryLists.iLevelID = Convert.ToInt32(lstLevel.SelectedValue.ToString());

        clsCarbonSourceCategoryLists.Update();

        Session["dtCarbonSourceCategoryListsList"] = null;

        //### Go back to view page
        Response.Redirect("frmCarbonSourceCategoryListsView.aspx");
    }

    #endregion
}