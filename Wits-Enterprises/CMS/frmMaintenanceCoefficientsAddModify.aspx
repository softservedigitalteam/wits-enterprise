<%@ Page Title="Maintenance Coeff" Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="True" CodeBehind="frmMaintenanceCoefficientsAddModify.aspx.cs" Inherits="CMS_frmMaintenanceCoefficientsAddModify" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div id="mandatoryDiv" class="mandatoryInvalidDiv" runat="server" visible="false">
        <asp:Label ID="lblValidationMessage" runat="server"></asp:Label>
    </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Title:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtTitle" runat="server" CssClass="roundedCornerTextBox" onKeyUp="return SetMaxLength(this,100)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Description:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtDescription" runat="server" CssClass="roundedCornerTextBoxMultiLine8" onKeyUp="return SetMaxLength(this,2500)" TextMode="MultiLine" Rows="8" />
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Price:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtPrice" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,50)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Product Category List:</div>
         <div class="fieldDiv">
             <asp:DropDownList ID="lstProductCategoryList" runat="server" CssClass="roundedCornerDropDownList" onblur="setValid(this, 0);"></asp:DropDownList>
         </div>
         <br class="clearingSpacer" />
     </div>
     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Biomass List:</div>
         <div class="fieldDiv">
             <asp:DropDownList ID="lstBiomassList" runat="server" CssClass="roundedCornerDropDownList" onblur="setValid(this, 0);"></asp:DropDownList>
         </div>
         <br class="clearingSpacer" />
     </div>
     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Carbon Source Category List:</div>
         <div class="fieldDiv">
             <asp:DropDownList ID="lstCarbonSourceCategoryList" runat="server" CssClass="roundedCornerDropDownList" onblur="setValid(this, 0);"></asp:DropDownList>
         </div>
         <br class="clearingSpacer" />
     </div>
    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Coefficient:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtCoefficient" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,50)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>
    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Time:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtTime" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,50)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>
     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Reference Notes:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtReferenceNotes" runat="server" CssClass="roundedCornerTextBoxMultiLine8" onKeyUp="return SetMaxLength(this,2500)" TextMode="MultiLine" Rows="8" />
         </div>
         <br class="clearingSpacer" />
     </div>
    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Is An Estimate?:</div>
         <div class="fieldDiv">
             <asp:DropDownList ID="lstAnEstimate" runat="server" CssClass="roundedCornerDropDownList" onblur="setValid(this, 0);">
                 <asp:ListItem Text="True" Value="True"></asp:ListItem>
                 <asp:ListItem Text="False" Value="False"></asp:ListItem>
             </asp:DropDownList>

         </div>
         <br class="clearingSpacer" />
     </div>

    
    <div class="Line"></div>
    <div class="buttonsRightDiv">
        <asp:LinkButton ID="lnkbtnBack" runat="server" CssClass="backButton" onclick="lnkbtnBack_Click" OnClientClick='history.go(-1);return false;' />
        <asp:LinkButton ID="lnkbtnSave" runat="server" CssClass="saveButton" onclick="lnkbtnSave_Click" />
        <asp:LinkButton ID="lnkbtnClear" runat="server" CssClass="clearButton" onclick="lnkbtnClear_Click" />
    </div>
</asp:Content>
