using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CMS_frmCompressionsAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsCompressions clsCompressions;
    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        if (!IsPostBack)
        {
             popCompressionType();

            //### If the iCompressionID is passed through then we want to instantiate the object with that iCompressionID
            if ((Request.QueryString["iCompressionID"] != "") && (Request.QueryString["iCompressionID"] != null))
            {
                clsCompressions = new clsCompressions(Convert.ToInt32(Request.QueryString["iCompressionID"]));

                //### Populate the form
                popFormData();
            }
            else
            {
                clsCompressions = new clsCompressions();
            }
            Session["clsCompressions"] = clsCompressions;
        }
        else
        {
            clsCompressions = (clsCompressions)Session["clsCompressions"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmCompressionsView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

       bCanSave = clsValidation.IsNullOrEmpty(txtTitle, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtPrice, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtInitialPressure, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtCompressedPressure, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(lstCompressionType, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtAntiFoam, bCanSave);

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">Compression added successfully</div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - Compression not added</div></div>";
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        txtTitle.Text = "";
        clsValidation.SetValid(txtTitle);
        txtDescription.Text = "";
        clsValidation.SetValid(txtDescription);
        txtPrice.Text = "";
        clsValidation.SetValid(txtPrice);
        txtInitialPressure.Text = "";
        clsValidation.SetValid(txtInitialPressure);
        txtCompressedPressure.Text = "";
        clsValidation.SetValid(txtCompressedPressure);
        lstCompressionType.SelectedValue = "0";
        clsValidation.SetValid(lstCompressionType);
        chkIsMultistaged.Checked = false;
        chkIsCooling.Checked = false;
        txtAntiFoam.Text = "";
        clsValidation.SetValid(txtAntiFoam);

    }

    #endregion 

    #region POPULATE DATA METHODS

    private void popFormData()
    {
         txtTitle.Text = clsCompressions.strTitle;
         txtDescription.Text = clsCompressions.strDescription;
        txtPrice.Text = clsCompressions.dblPrice.ToString();
        txtInitialPressure.Text = clsCompressions.dblInitialPressure.ToString();
        txtCompressedPressure.Text = clsCompressions.dblCompressedPressure.ToString();
        lstCompressionType.SelectedValue = clsCompressions.iCompressionTypeID.ToString();
        chkIsMultistaged.Checked = clsCompressions.bIsMultistaged;
        chkIsCooling.Checked = clsCompressions.bIsCooling;
        txtAntiFoam.Text = clsCompressions.dblAntiFoam.ToString();
    }
    
    private void popCompressionType()
    {
         DataTable dtCompressionTypesList = new DataTable();
         lstCompressionType.DataSource = clsCompressionTypes.GetCompressionTypesList();

         //### Populates the drop down list with PK and TITLE;
         lstCompressionType.DataValueField = "iCompressionTypeID";
         lstCompressionType.DataTextField = "strTitle";

         //### Bind the data to the list;
         lstCompressionType.DataBind();

         //### Add default select option;
         lstCompressionType.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    }
    
    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        clsCompressions.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsCompressions.iAddedBy = clsUsers.iUserID;
        clsCompressions.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsCompressions.iEditedBy = clsUsers.iUserID;
        clsCompressions.strTitle = txtTitle.Text;
        clsCompressions.strDescription = txtDescription.Text;
        clsCompressions.dblPrice = Convert.ToDouble(txtPrice.Text);
        clsCompressions.dblInitialPressure = Convert.ToDouble(txtInitialPressure.Text);
        clsCompressions.dblCompressedPressure = Convert.ToDouble(txtCompressedPressure.Text);
        clsCompressions.iCompressionTypeID = Convert.ToInt32(lstCompressionType.SelectedValue.ToString());
        clsCompressions.bIsMultistaged = chkIsMultistaged.Checked;
        clsCompressions.bIsCooling = chkIsCooling.Checked;
        clsCompressions.dblAntiFoam = Convert.ToDouble(txtAntiFoam.Text);

        clsCompressions.Update();

        Session["dtCompressionsList"] = null;

        //### Go back to view page
        Response.Redirect("frmCompressionsView.aspx");
    }

    #endregion
}