<%@ Page Title="System Users" Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="True" CodeBehind="frmSystemUsersAddModify.aspx.cs" Inherits="CMS_frmSystemUsersAddModify" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div id="mandatoryDiv" class="mandatoryInvalidDiv" runat="server" visible="false">
        <asp:Label ID="lblValidationMessage" runat="server"></asp:Label>
    </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">First Name:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtFirstName" runat="server" CssClass="roundedCornerTextBoxMultiLine2" onKeyUp="return SetMaxLength(this,250)" TextMode="MultiLine" Rows="2" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Last Name:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtLastName" runat="server" CssClass="roundedCornerTextBoxMultiLine2" onKeyUp="return SetMaxLength(this,250)" TextMode="MultiLine" Rows="2" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Contact Number:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtContactNumber" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,20)" />
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Email Address:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtEmailAddress" runat="server" CssClass="roundedCornerTextBox" onKeyUp="return SetMaxLength(this,120)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>


    
    <div class="Line"></div>
    <div class="buttonsRightDiv">
        <asp:LinkButton ID="lnkbtnBack" runat="server" CssClass="backButton" onclick="lnkbtnBack_Click" OnClientClick='history.go(-1);return false;' />
        <asp:LinkButton ID="lnkbtnSave" runat="server" CssClass="saveButton" onclick="lnkbtnSave_Click" />
        <asp:LinkButton ID="lnkbtnClear" runat="server" CssClass="clearButton" onclick="lnkbtnClear_Click" />
    </div>
</asp:Content>
