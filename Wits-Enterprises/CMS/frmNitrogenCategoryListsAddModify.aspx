<%@ Page Title="Nitrogen" Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="True" CodeBehind="frmNitrogenCategoryListsAddModify.aspx.cs" Inherits="CMS_frmNitrogenCategoryListsAddModify" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div id="mandatoryDiv" class="mandatoryInvalidDiv" runat="server" visible="false">
        <asp:Label ID="lblValidationMessage" runat="server"></asp:Label>
    </div>

    <div class="controlDiv">
        <div class="imageHolderCommonDiv">
        </div>
        <div class="labelDiv">
            Category:
        </div>
        <div class="fieldDiv">
            <asp:TreeView ID="treeNitrogenCategories" runat="server" ExpandImageUrl="~/CMS/images/imgPlus.png" CollapseImageUrl="~/CMS/images/imgMinus.png" CssClass="TreeView">
                <Nodes>
                    <asp:TreeNode Value="0" Text="Top level category"></asp:TreeNode>
                </Nodes>
                <NodeStyle ForeColor="Black" CssClass="TreeviewNode" />
                <SelectedNodeStyle CssClass="TreeviewSelected" />
            </asp:TreeView>
        </div>
        <br class="clearingSpacer" />
    </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Title:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtTitle" runat="server" CssClass="roundedCornerTextBox" onKeyUp="return SetMaxLength(this,100)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Description:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtDescription" runat="server" CssClass="roundedCornerTextBoxMultiLine8" onKeyUp="return SetMaxLength(this,2500)" TextMode="MultiLine" Rows="8" />
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">ChemicalFormula:</div>
         <div class="fieldDiv">
             <asp:DropDownList ID="lstChemicalFormula" runat="server" CssClass="roundedCornerDropDownList" onblur="setValid(this, 0);"></asp:DropDownList>
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Price:</div>
         <div class="fieldDiv">
            <asp:TextBox ID="txtPrice" runat="server" CssClass="roundedCornerTextBox" onKeyUp="return SetMaxLength(this,100)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Density:</div>
         <div class="fieldDiv">
            <asp:TextBox ID="txtDensity" runat="server" CssClass="roundedCornerTextBox" onKeyUp="return SetMaxLength(this,100)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>
     <%--<div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Level:</div>
         <div class="fieldDiv">
             <asp:DropDownList ID="lstLevel" runat="server" CssClass="roundedCornerDropDownList" ></asp:DropDownList>
         </div>
         <br class="clearingSpacer" />
     </div>--%>

    
    <div class="Line"></div>
    <div class="buttonsRightDiv">
        <asp:LinkButton ID="lnkbtnBack" runat="server" CssClass="backButton" onclick="lnkbtnBack_Click" OnClientClick='history.go(-1);return false;' />
        <asp:LinkButton ID="lnkbtnSave" runat="server" CssClass="saveButton" onclick="lnkbtnSave_Click" />
        <asp:LinkButton ID="lnkbtnClear" runat="server" CssClass="clearButton" onclick="lnkbtnClear_Click" />
    </div>
</asp:Content>
