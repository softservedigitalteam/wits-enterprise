using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CMS_frmGrowthRatesAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsGrowthRates clsGrowthRates;
    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        if (!IsPostBack)
        {
             popProductCategoryList();
             popBiomassList();
             popCarbonSourceCategoryList();

            //### If the iGrowthRateID is passed through then we want to instantiate the object with that iGrowthRateID
            if ((Request.QueryString["iGrowthRateID"] != "") && (Request.QueryString["iGrowthRateID"] != null))
            {
                clsGrowthRates = new clsGrowthRates(Convert.ToInt32(Request.QueryString["iGrowthRateID"]));

                //### Populate the form
                popFormData();
            }
            else
            {
                clsGrowthRates = new clsGrowthRates();
            }
            Session["clsGrowthRates"] = clsGrowthRates;
        }
        else
        {
            clsGrowthRates = (clsGrowthRates)Session["clsGrowthRates"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmGrowthRatesView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

       bCanSave = clsValidation.IsNullOrEmpty(txtTitle, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtPrice, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(lstProductCategoryList, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(lstBiomassList, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(lstCarbonSourceCategoryList, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtInitialBiomassConcentration, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtFinalBiomassConcentration, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtMaxSpecificGrowth, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtLimitingNutrientConcentrationHalfLife, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtFinalProductConcentration, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtRx, bCanSave);

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">GrowthRate added successfully</div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - GrowthRate not added</div></div>";
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        txtTitle.Text = "";
        clsValidation.SetValid(txtTitle);
        txtDescription.Text = "";
        clsValidation.SetValid(txtDescription);
        txtPrice.Text = "";
        clsValidation.SetValid(txtPrice);
        lstProductCategoryList.SelectedValue = "0";
        clsValidation.SetValid(lstProductCategoryList);
        lstBiomassList.SelectedValue = "0";
        clsValidation.SetValid(lstBiomassList);
        lstCarbonSourceCategoryList.SelectedValue = "0";
        clsValidation.SetValid(lstCarbonSourceCategoryList);
        txtInitialBiomassConcentration.Text = "";
        clsValidation.SetValid(txtInitialBiomassConcentration);
        txtFinalBiomassConcentration.Text = "";
        clsValidation.SetValid(txtFinalBiomassConcentration);
        txtMaxSpecificGrowth.Text = "";
        clsValidation.SetValid(txtMaxSpecificGrowth);
        txtLimitingNutrientConcentrationHalfLife.Text = "";
        clsValidation.SetValid(txtLimitingNutrientConcentrationHalfLife);
        txtFinalProductConcentration.Text = "";
        clsValidation.SetValid(txtFinalProductConcentration);
        txtRx.Text = "";
        clsValidation.SetValid(txtRx);
        txtReferenceNotes.Text = "";
        clsValidation.SetValid(txtReferenceNotes);
    }

    #endregion 

    #region POPULATE DATA METHODS

    private void popFormData()
    {
         txtTitle.Text = clsGrowthRates.strTitle;
         txtDescription.Text = clsGrowthRates.strDescription;
        txtPrice.Text = clsGrowthRates.dblPrice.ToString();
        lstProductCategoryList.SelectedValue = clsGrowthRates.iProductCategoryListID.ToString();
         lstBiomassList.SelectedValue = clsGrowthRates.iBiomassListID.ToString();
         lstCarbonSourceCategoryList.SelectedValue = clsGrowthRates.iCarbonSourceCategoryListID.ToString();
        txtInitialBiomassConcentration.Text = clsGrowthRates.dblInitialBiomassConcentration.ToString();
        txtFinalBiomassConcentration.Text = clsGrowthRates.dblFinalBiomassConcentration.ToString();
        txtMaxSpecificGrowth.Text = clsGrowthRates.dblMaxSpecificGrowth.ToString();
        txtLimitingNutrientConcentrationHalfLife.Text = clsGrowthRates.dblLimitingNutrientConcentrationHalfLife.ToString();
        txtFinalProductConcentration.Text = clsGrowthRates.dblFinalProductConcentration.ToString();
        txtRx.Text = clsGrowthRates.dblRx.ToString();
        txtReferenceNotes.Text = clsGrowthRates.strReferenceNotes;
        lstAnEstimate.SelectedValue = clsGrowthRates.bIsAnEstimate.ToString();
    }
    
    private void popProductCategoryList()
    {
         DataTable dtProductCategoryListsList = new DataTable();
         lstProductCategoryList.DataSource = clsProductCategoryLists.GetProductCategoryListsList();

         //### Populates the drop down list with PK and TITLE;
         lstProductCategoryList.DataValueField = "iProductCategoryListID";
         lstProductCategoryList.DataTextField = "strTitle";

         //### Bind the data to the list;
         lstProductCategoryList.DataBind();

         //### Add default select option;
         lstProductCategoryList.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    }
    
    private void popBiomassList()
    {
         DataTable dtBiomassListsList = new DataTable();
         lstBiomassList.DataSource = clsBiomassLists.GetBiomassListsList();

         //### Populates the drop down list with PK and TITLE;
         lstBiomassList.DataValueField = "iBiomassListID";
         lstBiomassList.DataTextField = "strTitle";

         //### Bind the data to the list;
         lstBiomassList.DataBind();

         //### Add default select option;
         lstBiomassList.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    }
    
    private void popCarbonSourceCategoryList()
    {
         DataTable dtCarbonSourceCategoryListsList = new DataTable();
         lstCarbonSourceCategoryList.DataSource = clsCarbonSourceCategoryLists.GetCarbonSourceCategoryListsList();

         //### Populates the drop down list with PK and TITLE;
         lstCarbonSourceCategoryList.DataValueField = "iCarbonSourceCategoryListID";
         lstCarbonSourceCategoryList.DataTextField = "strTitle";

         //### Bind the data to the list;
         lstCarbonSourceCategoryList.DataBind();

         //### Add default select option;
         lstCarbonSourceCategoryList.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    }
    
    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        clsGrowthRates.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsGrowthRates.iAddedBy = clsUsers.iUserID;
        clsGrowthRates.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsGrowthRates.iEditedBy = clsUsers.iUserID;
        clsGrowthRates.strTitle = txtTitle.Text;
        clsGrowthRates.strDescription = txtDescription.Text;
        clsGrowthRates.dblPrice = Convert.ToDouble(txtPrice.Text);
        clsGrowthRates.iProductCategoryListID = Convert.ToInt32(lstProductCategoryList.SelectedValue.ToString());
        clsGrowthRates.iBiomassListID = Convert.ToInt32(lstBiomassList.SelectedValue.ToString());
        clsGrowthRates.iCarbonSourceCategoryListID = Convert.ToInt32(lstCarbonSourceCategoryList.SelectedValue.ToString());
        clsGrowthRates.dblInitialBiomassConcentration = Convert.ToDouble(txtInitialBiomassConcentration.Text);
        clsGrowthRates.dblFinalBiomassConcentration = Convert.ToDouble(txtFinalBiomassConcentration.Text);
        clsGrowthRates.dblMaxSpecificGrowth = Convert.ToDouble(txtMaxSpecificGrowth.Text);
        clsGrowthRates.dblLimitingNutrientConcentrationHalfLife = Convert.ToDouble(txtLimitingNutrientConcentrationHalfLife.Text);
        clsGrowthRates.dblFinalProductConcentration = Convert.ToDouble(txtFinalProductConcentration.Text);
        clsGrowthRates.dblRx = Convert.ToDouble(txtRx.Text);
        clsGrowthRates.strReferenceNotes = txtReferenceNotes.Text;
        clsGrowthRates.bIsAnEstimate = Convert.ToBoolean(lstAnEstimate.SelectedValue);

        clsGrowthRates.Update();

        Session["dtGrowthRatesList"] = null;

        //### Go back to view page
        Response.Redirect("frmGrowthRatesView.aspx");
    }

    #endregion
}