using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CMS_frmSubscriptionsAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsSubscriptions clsSubscriptions;
    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        if (!IsPostBack)
        {
             popSystemUser();
             popSubscriptionType();

            //### If the iSubscriptionID is passed through then we want to instantiate the object with that iSubscriptionID
            if ((Request.QueryString["iSubscriptionID"] != "") && (Request.QueryString["iSubscriptionID"] != null))
            {
                clsSubscriptions = new clsSubscriptions(Convert.ToInt32(Request.QueryString["iSubscriptionID"]));

                //### Populate the form
                popFormData();
            }
            else
            {
                clsSubscriptions = new clsSubscriptions();
            }
            Session["clsSubscriptions"] = clsSubscriptions;
        }
        else
        {
            clsSubscriptions = (clsSubscriptions)Session["clsSubscriptions"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmSubscriptionsView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

       bCanSave = clsValidation.IsNullOrEmpty(txtTitle, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(lstSystemUser, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(lstSubscriptionType, bCanSave);

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">Subscription added successfully</div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - Subscription not added</div></div>";
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        txtTitle.Text = "";
        clsValidation.SetValid(txtTitle);
        lstSystemUser.SelectedValue = "0";
        clsValidation.SetValid(lstSystemUser);
        lstSubscriptionType.SelectedValue = "0";
        clsValidation.SetValid(lstSubscriptionType);
    }

    #endregion 

    #region POPULATE DATA METHODS

    private void popFormData()
    {
         txtTitle.Text = clsSubscriptions.strTitle;
         lstSystemUser.SelectedValue = clsSubscriptions.iSystemUserID.ToString();
         lstSubscriptionType.SelectedValue = clsSubscriptions.iSubscriptionTypeID.ToString();
    }
    
    private void popSystemUser()
    {
        DataTable dtSystemUsersList = clsSystemUsers.GetSystemUsersList();
        dtSystemUsersList.Columns.Add("strFullName");

        foreach (DataRow drSystemUserList in dtSystemUsersList.Rows)
        {
            drSystemUserList["strFullName"] = drSystemUserList["strFirstName"].ToString() + " " + drSystemUserList["strLastName"].ToString();
        }
        //lstSystemUser.DataSource = clsSystemUsers.GetSystemUsersList();

         lstSystemUser.DataSource = dtSystemUsersList;
         //### Populates the drop down list with PK and TITLE;
         lstSystemUser.DataValueField = "iSystemUserID";
         lstSystemUser.DataTextField = "strFullName";

         //### Bind the data to the list;
         lstSystemUser.DataBind();

         //### Add default select option;
         lstSystemUser.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    }
    
    private void popSubscriptionType()
    {
         DataTable dtSubscriptionTypesList = new DataTable();
         lstSubscriptionType.DataSource = clsSubscriptionTypes.GetSubscriptionTypesList();

         //### Populates the drop down list with PK and TITLE;
         lstSubscriptionType.DataValueField = "iSubscriptionTypeID";
         lstSubscriptionType.DataTextField = "strTitle";

         //### Bind the data to the list;
         lstSubscriptionType.DataBind();

         //### Add default select option;
         lstSubscriptionType.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    }
    
    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        clsSubscriptions.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsSubscriptions.iAddedBy = clsUsers.iUserID;
        clsSubscriptions.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsSubscriptions.iEditedBy = clsUsers.iUserID;
        clsSubscriptions.strTitle = txtTitle.Text;
        clsSubscriptions.iSystemUserID = Convert.ToInt32(lstSystemUser.SelectedValue.ToString());
        clsSubscriptions.iSubscriptionTypeID = Convert.ToInt32(lstSubscriptionType.SelectedValue.ToString());

        clsSubscriptions.Update();

        Session["dtSubscriptionsList"] = null;

        //### Go back to view page
        Response.Redirect("frmSubscriptionsView.aspx");
    }

    #endregion
}