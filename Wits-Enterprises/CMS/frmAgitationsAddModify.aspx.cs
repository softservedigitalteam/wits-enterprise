using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CMS_frmAgitationsAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsAgitations clsAgitations;
    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        if (!IsPostBack)
        {
             popImpeller();

            //### If the iAgitationID is passed through then we want to instantiate the object with that iAgitationID
            if ((Request.QueryString["iAgitationID"] != "") && (Request.QueryString["iAgitationID"] != null))
            {
                clsAgitations = new clsAgitations(Convert.ToInt32(Request.QueryString["iAgitationID"]));

                //### Populate the form
                popFormData();
            }
            else
            {
                clsAgitations = new clsAgitations();
            }
            Session["clsAgitations"] = clsAgitations;
        }
        else
        {
            clsAgitations = (clsAgitations)Session["clsAgitations"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmAgitationsView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

       bCanSave = clsValidation.IsNullOrEmpty(txtTitle, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtPrice, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(lstImpeller, bCanSave);

        bCanSave = clsValidation.IsNullOrEmpty(txtNumberOfTanks, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtAgitationEfficiency, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtPowerPerUnitVolume, bCanSave);



        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">Agitation added successfully</div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - Agitation not added</div></div>";
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        txtTitle.Text = "";
        clsValidation.SetValid(txtTitle);
        txtDescription.Text = "";
        clsValidation.SetValid(txtDescription);
        txtPrice.Text = "";
        clsValidation.SetValid(txtPrice);
        lstImpeller.SelectedValue = "0";
        clsValidation.SetValid(lstImpeller);

        txtNumberOfTanks.Text = "";
        clsValidation.SetValid(txtNumberOfTanks);
        txtAgitationEfficiency.Text = "";
        clsValidation.SetValid(txtAgitationEfficiency);
        txtPowerPerUnitVolume.Text = "";
        clsValidation.SetValid(txtPowerPerUnitVolume);
    }

    #endregion 

    #region POPULATE DATA METHODS

    private void popFormData()
    {
         txtTitle.Text = clsAgitations.strTitle;
         txtDescription.Text = clsAgitations.strDescription;
        txtPrice.Text = clsAgitations.dblPrice.ToString();
        lstImpeller.SelectedValue = clsAgitations.iImpellerID.ToString();

        txtNumberOfTanks.Text = clsAgitations.dblNumberOfTanks.ToString();
        txtAgitationEfficiency.Text = clsAgitations.dblAgitationEfficiency.ToString();
        txtPowerPerUnitVolume.Text = clsAgitations.dblPowerPerUnitVolume.ToString();
    }
    
    private void popImpeller()
    {
         DataTable dtImpellersList = new DataTable();
         lstImpeller.DataSource = clsImpellers.GetImpellersList();

         //### Populates the drop down list with PK and TITLE;
         lstImpeller.DataValueField = "iImpellerID";
         lstImpeller.DataTextField = "strTitle";

         //### Bind the data to the list;
         lstImpeller.DataBind();

         //### Add default select option;
         lstImpeller.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    }
    
    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        clsAgitations.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsAgitations.iAddedBy = clsUsers.iUserID;
        clsAgitations.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsAgitations.iEditedBy = clsUsers.iUserID;
        clsAgitations.strTitle = txtTitle.Text;
        clsAgitations.strDescription = txtDescription.Text;
        clsAgitations.dblPrice = Convert.ToDouble(txtPrice.Text);
        clsAgitations.iImpellerID = Convert.ToInt32(lstImpeller.SelectedValue.ToString());

        clsAgitations.dblNumberOfTanks = Convert.ToDouble(txtNumberOfTanks.Text);
        clsAgitations.dblAgitationEfficiency = Convert.ToDouble(txtAgitationEfficiency.Text);
        clsAgitations.dblPowerPerUnitVolume = Convert.ToDouble(txtPowerPerUnitVolume.Text);

        clsAgitations.Update();

        Session["dtAgitationsList"] = null;

        //### Go back to view page
        Response.Redirect("frmAgitationsView.aspx");
    }

    #endregion
}