
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;

/// <summary>
/// Summary description for clsCompressionTypesView
/// </summary>
public partial class CMS_clsCompressionTypesView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtCompressionTypesList;

    List<clsCompressionTypes> glstCompressionTypes;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
        }
            PopulateFormData();

    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string FilterExpression ="(strTitle) LIKE '%" + txtSearch.Text + "%'";
        DataTable dtCompressionTypesList = clsCompressionTypes.GetCompressionTypesList(FilterExpression, "");

        Session["dtCompressionTypesList"] = dtCompressionTypesList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmCompressionTypesAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsCompressionTypesList object
        try
        {
            dtCompressionTypesList = new DataTable();

            if (Session["dtCompressionTypesList"] == null)
                dtCompressionTypesList = clsCompressionTypes.GetCompressionTypesList();
            else
                dtCompressionTypesList = (DataTable)Session["dtCompressionTypesList"];

            dgrGrid.DataSource = dtCompressionTypesList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["CompressionTypesView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["CompressionTypesView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["CompressionTypesView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iCompressionTypeID = int.Parse((sender as LinkButton).CommandArgument);

        clsCompressionTypes.Delete(iCompressionTypeID);

        PopulateFormData();
        Session["dtCompressionTypesList"] = null;
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["CompressionTypesView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtCompressionTypesList = new DataTable();

        if (Session["dtCompressionTypesList"] == null)
            dtCompressionTypesList = clsCompressionTypes.GetCompressionTypesList();
        else
            dtCompressionTypesList = (DataTable)Session["dtCompressionTypesList"];

        DataView dvTemp = dtCompressionTypesList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtCompressionTypesList = dvTemp.ToTable();
        Session["dtCompressionTypesList"] = dtCompressionTypesList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iCompressionTypeID
            int iCompressionTypeID = 0;
            iCompressionTypeID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmCompressionTypesAddModify.aspx?action=edit&iCompressionTypeID=" + iCompressionTypeID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    #endregion

    #region CompressionTypes FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strTitle) LIKE '%" + prefixText + "%'";
        DataTable dtCompressionTypes = clsCompressionTypes.GetCompressionTypesList(FilterExpression, "");
        List<string> glstCompressionTypes = new List<string>();

        if (dtCompressionTypes.Rows.Count > 0)
        {
            foreach (DataRow dtrCompressionTypes in dtCompressionTypes.Rows)
            {
                glstCompressionTypes.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrCompressionTypes["strTitle"].ToString(), dtrCompressionTypes["iCompressionTypeID"].ToString()));
            }
        }
        else
            glstCompressionTypes.Add("No CompressionTypes Available.");
        strReturnList = glstCompressionTypes.ToArray();
        return strReturnList;
    }

    #endregion
}
