
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;

/// <summary>
/// Summary description for clsSystemUsersView
/// </summary>
public partial class CMS_clsSystemUsersView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtSystemUsersList;

    List<clsSystemUsers> glstSystemUsers;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
        }
            PopulateFormData();

    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string FilterExpression ="(strFirstName +' '+ strLastName +' '+ strEmailAddress) LIKE '%" + txtSearch.Text + "%'";
        DataTable dtSystemUsersList = clsSystemUsers.GetSystemUsersList(FilterExpression, "");

        Session["dtSystemUsersList"] = dtSystemUsersList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmSystemUsersAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsSystemUsersList object
        try
        {
            dtSystemUsersList = new DataTable();

            if (Session["dtSystemUsersList"] == null)
                dtSystemUsersList = clsSystemUsers.GetSystemUsersList();
            else
                dtSystemUsersList = (DataTable)Session["dtSystemUsersList"];

            dgrGrid.DataSource = dtSystemUsersList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["SystemUsersView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["SystemUsersView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["SystemUsersView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iSystemUserID = int.Parse((sender as LinkButton).CommandArgument);

        clsSystemUsers.Delete(iSystemUserID);

        PopulateFormData();
        Session["dtSystemUsersList"] = null;
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["SystemUsersView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtSystemUsersList = new DataTable();

        if (Session["dtSystemUsersList"] == null)
            dtSystemUsersList = clsSystemUsers.GetSystemUsersList();
        else
            dtSystemUsersList = (DataTable)Session["dtSystemUsersList"];

        DataView dvTemp = dtSystemUsersList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtSystemUsersList = dvTemp.ToTable();
        Session["dtSystemUsersList"] = dtSystemUsersList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iSystemUserID
            int iSystemUserID = 0;
            iSystemUserID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmSystemUsersAddModify.aspx?action=edit&iSystemUserID=" + iSystemUserID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    #endregion

    #region SystemUsers FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strFirstName +' '+ strLastName +' '+ strEmailAddress) LIKE '%" + prefixText + "%'";
        DataTable dtSystemUsers = clsSystemUsers.GetSystemUsersList(FilterExpression, "");
        List<string> glstSystemUsers = new List<string>();

        if (dtSystemUsers.Rows.Count > 0)
        {
            foreach (DataRow dtrSystemUsers in dtSystemUsers.Rows)
            {
                glstSystemUsers.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrSystemUsers["strFirstName"].ToString() +' '+"" + dtrSystemUsers["strLastName"].ToString() +' '+"" + dtrSystemUsers["strEmailAddress"].ToString(), dtrSystemUsers["iSystemUserID"].ToString()));
            }
        }
        else
            glstSystemUsers.Add("No SystemUsers Available.");
        strReturnList = glstSystemUsers.ToArray();
        return strReturnList;
    }

    #endregion
}
