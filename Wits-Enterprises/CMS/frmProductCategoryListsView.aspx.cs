
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;

/// <summary>
/// Summary description for clsProductCategoryListsView
/// </summary>
public partial class CMS_clsProductCategoryListsView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtProductCategoryListsList;

    List<clsProductCategoryLists> glstProductCategoryLists;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
        }
            PopulateFormData();

    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string FilterExpression ="(strTitle) LIKE '%" + txtSearch.Text + "%'";
        DataTable dtProductCategoryListsList = clsProductCategoryLists.GetProductCategoryListsList(FilterExpression, "");

        Session["dtProductCategoryListsList"] = dtProductCategoryListsList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmProductCategoryListsAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsProductCategoryListsList object
        try
        {
            dtProductCategoryListsList = new DataTable();

            if (Session["dtProductCategoryListsList"] == null)
                dtProductCategoryListsList = clsProductCategoryLists.GetProductCategoryListsList();
            else
                dtProductCategoryListsList = (DataTable)Session["dtProductCategoryListsList"];

            dgrGrid.DataSource = dtProductCategoryListsList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["ProductCategoryListsView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["ProductCategoryListsView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["ProductCategoryListsView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iProductCategoryListID = int.Parse((sender as LinkButton).CommandArgument);

        clsProductCategoryLists.Delete(iProductCategoryListID);

        PopulateFormData();
        Session["dtProductCategoryListsList"] = null;
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["ProductCategoryListsView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtProductCategoryListsList = new DataTable();

        if (Session["dtProductCategoryListsList"] == null)
            dtProductCategoryListsList = clsProductCategoryLists.GetProductCategoryListsList();
        else
            dtProductCategoryListsList = (DataTable)Session["dtProductCategoryListsList"];

        DataView dvTemp = dtProductCategoryListsList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtProductCategoryListsList = dvTemp.ToTable();
        Session["dtProductCategoryListsList"] = dtProductCategoryListsList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iProductCategoryListID
            int iProductCategoryListID = 0;
            iProductCategoryListID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmProductCategoryListsAddModify.aspx?action=edit&iProductCategoryListID=" + iProductCategoryListID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    #endregion

    #region ProductCategoryLists FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strTitle) LIKE '%" + prefixText + "%'";
        DataTable dtProductCategoryLists = clsProductCategoryLists.GetProductCategoryListsList(FilterExpression, "");
        List<string> glstProductCategoryLists = new List<string>();

        if (dtProductCategoryLists.Rows.Count > 0)
        {
            foreach (DataRow dtrProductCategoryLists in dtProductCategoryLists.Rows)
            {
                glstProductCategoryLists.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrProductCategoryLists["strTitle"].ToString(), dtrProductCategoryLists["iProductCategoryListID"].ToString()));
            }
        }
        else
            glstProductCategoryLists.Add("No ProductCategoryLists Available.");
        strReturnList = glstProductCategoryLists.ToArray();
        return strReturnList;
    }

    #endregion
}
