
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;

/// <summary>
/// Summary description for clsCoolantsView
/// </summary>
public partial class CMS_clsCoolantsView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtCoolantsList;

    List<clsCoolants> glstCoolants;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
        }
            PopulateFormData();

    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string FilterExpression ="(strTitle) LIKE '%" + txtSearch.Text + "%'";
        DataTable dtCoolantsList = clsCoolants.GetCoolantsList(FilterExpression, "");

        Session["dtCoolantsList"] = dtCoolantsList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmCoolantsAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsCoolantsList object
        try
        {
            dtCoolantsList = new DataTable();

            if (Session["dtCoolantsList"] == null)
                dtCoolantsList = clsCoolants.GetCoolantsList();
            else
                dtCoolantsList = (DataTable)Session["dtCoolantsList"];

            dgrGrid.DataSource = dtCoolantsList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["CoolantsView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["CoolantsView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["CoolantsView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iCoolantID = int.Parse((sender as LinkButton).CommandArgument);

        clsCoolants.Delete(iCoolantID);

        PopulateFormData();
        Session["dtCoolantsList"] = null;
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["CoolantsView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtCoolantsList = new DataTable();

        if (Session["dtCoolantsList"] == null)
            dtCoolantsList = clsCoolants.GetCoolantsList();
        else
            dtCoolantsList = (DataTable)Session["dtCoolantsList"];

        DataView dvTemp = dtCoolantsList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtCoolantsList = dvTemp.ToTable();
        Session["dtCoolantsList"] = dtCoolantsList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iCoolantID
            int iCoolantID = 0;
            iCoolantID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmCoolantsAddModify.aspx?action=edit&iCoolantID=" + iCoolantID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    #endregion

    #region Coolants FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strTitle) LIKE '%" + prefixText + "%'";
        DataTable dtCoolants = clsCoolants.GetCoolantsList(FilterExpression, "");
        List<string> glstCoolants = new List<string>();

        if (dtCoolants.Rows.Count > 0)
        {
            foreach (DataRow dtrCoolants in dtCoolants.Rows)
            {
                glstCoolants.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrCoolants["strTitle"].ToString(), dtrCoolants["iCoolantID"].ToString()));
            }
        }
        else
            glstCoolants.Add("No Coolants Available.");
        strReturnList = glstCoolants.ToArray();
        return strReturnList;
    }

    #endregion
}
