
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;

/// <summary>
/// Summary description for clsCompressionsView
/// </summary>
public partial class CMS_clsCompressionsView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtCompressionsList;

    List<clsCompressions> glstCompressions;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
        }
            PopulateFormData();

    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string FilterExpression ="(strTitle) LIKE '%" + txtSearch.Text + "%'";
        DataTable dtCompressionsList = clsCompressions.GetCompressionsList(FilterExpression, "");

        Session["dtCompressionsList"] = dtCompressionsList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmCompressionsAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsCompressionsList object
        try
        {
            dtCompressionsList = new DataTable();

            if (Session["dtCompressionsList"] == null)
                dtCompressionsList = clsCompressions.GetCompressionsList();
            else
                dtCompressionsList = (DataTable)Session["dtCompressionsList"];

            dgrGrid.DataSource = dtCompressionsList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["CompressionsView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["CompressionsView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["CompressionsView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iCompressionID = int.Parse((sender as LinkButton).CommandArgument);

        clsCompressions.Delete(iCompressionID);

        PopulateFormData();
        Session["dtCompressionsList"] = null;
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["CompressionsView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtCompressionsList = new DataTable();

        if (Session["dtCompressionsList"] == null)
            dtCompressionsList = clsCompressions.GetCompressionsList();
        else
            dtCompressionsList = (DataTable)Session["dtCompressionsList"];

        DataView dvTemp = dtCompressionsList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtCompressionsList = dvTemp.ToTable();
        Session["dtCompressionsList"] = dtCompressionsList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iCompressionID
            int iCompressionID = 0;
            iCompressionID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmCompressionsAddModify.aspx?action=edit&iCompressionID=" + iCompressionID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    #endregion

    #region Compressions FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strTitle) LIKE '%" + prefixText + "%'";
        DataTable dtCompressions = clsCompressions.GetCompressionsList(FilterExpression, "");
        List<string> glstCompressions = new List<string>();

        if (dtCompressions.Rows.Count > 0)
        {
            foreach (DataRow dtrCompressions in dtCompressions.Rows)
            {
                glstCompressions.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrCompressions["strTitle"].ToString(), dtrCompressions["iCompressionID"].ToString()));
            }
        }
        else
            glstCompressions.Add("No Compressions Available.");
        strReturnList = glstCompressions.ToArray();
        return strReturnList;
    }

    #endregion
}
