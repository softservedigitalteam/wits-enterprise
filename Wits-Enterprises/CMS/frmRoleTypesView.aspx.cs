
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;

/// <summary>
/// Summary description for clsRoleTypesView
/// </summary>
public partial class CMS_clsRoleTypesView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtRoleTypesList;

    List<clsRoleTypes> glstRoleTypes;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
        }
            PopulateFormData();

    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string FilterExpression ="(strTitle) LIKE '%" + txtSearch.Text + "%'";
        DataTable dtRoleTypesList = clsRoleTypes.GetRoleTypesList(FilterExpression, "");

        Session["dtRoleTypesList"] = dtRoleTypesList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmRoleTypesAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsRoleTypesList object
        try
        {
            dtRoleTypesList = new DataTable();

            if (Session["dtRoleTypesList"] == null)
                dtRoleTypesList = clsRoleTypes.GetRoleTypesList();
            else
                dtRoleTypesList = (DataTable)Session["dtRoleTypesList"];

            dgrGrid.DataSource = dtRoleTypesList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["RoleTypesView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["RoleTypesView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["RoleTypesView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iRoleTypeID = int.Parse((sender as LinkButton).CommandArgument);

        clsRoleTypes.Delete(iRoleTypeID);

        PopulateFormData();
        Session["dtRoleTypesList"] = null;
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["RoleTypesView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtRoleTypesList = new DataTable();

        if (Session["dtRoleTypesList"] == null)
            dtRoleTypesList = clsRoleTypes.GetRoleTypesList();
        else
            dtRoleTypesList = (DataTable)Session["dtRoleTypesList"];

        DataView dvTemp = dtRoleTypesList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtRoleTypesList = dvTemp.ToTable();
        Session["dtRoleTypesList"] = dtRoleTypesList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iRoleTypeID
            int iRoleTypeID = 0;
            iRoleTypeID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmRoleTypesAddModify.aspx?action=edit&iRoleTypeID=" + iRoleTypeID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    #endregion

    #region RoleTypes FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strTitle) LIKE '%" + prefixText + "%'";
        DataTable dtRoleTypes = clsRoleTypes.GetRoleTypesList(FilterExpression, "");
        List<string> glstRoleTypes = new List<string>();

        if (dtRoleTypes.Rows.Count > 0)
        {
            foreach (DataRow dtrRoleTypes in dtRoleTypes.Rows)
            {
                glstRoleTypes.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrRoleTypes["strTitle"].ToString(), dtrRoleTypes["iRoleTypeID"].ToString()));
            }
        }
        else
            glstRoleTypes.Add("No RoleTypes Available.");
        strReturnList = glstRoleTypes.ToArray();
        return strReturnList;
    }

    #endregion
}
