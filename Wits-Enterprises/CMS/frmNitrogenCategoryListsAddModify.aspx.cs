using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CMS_frmNitrogenCategoryListsAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsNitrogenCategoryLists clsNitrogenCategoryLists;
    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        if (!IsPostBack)
        {
             popChemicalFormula();
             //popLevel();

            //### If the iNitrogenCategoryListID is passed through then we want to instantiate the object with that iNitrogenCategoryListID
            if ((Request.QueryString["iNitrogenCategoryListID"] != "") && (Request.QueryString["iNitrogenCategoryListID"] != null))
            {
                clsNitrogenCategoryLists = new clsNitrogenCategoryLists(Convert.ToInt32(Request.QueryString["iNitrogenCategoryListID"]));

                //### Populate the form
                popFormData();
            }
            else
            {
                PopNitrogenCategories(0);
                clsNitrogenCategoryLists = new clsNitrogenCategoryLists();
            }
            Session["clsNitrogenCategoryLists"] = clsNitrogenCategoryLists;
        }
        else
        {
            clsNitrogenCategoryLists = (clsNitrogenCategoryLists)Session["clsNitrogenCategoryLists"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmNitrogenCategoryListsView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

       bCanSave = clsValidation.IsNullOrEmpty(txtTitle, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(lstChemicalFormula, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtPrice, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtDensity, bCanSave);

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">NitrogenCategoryList added successfully</div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - NitrogenCategoryList not added</div></div>";
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        txtTitle.Text = "";
        clsValidation.SetValid(txtTitle);
        txtDescription.Text = "";
        clsValidation.SetValid(txtDescription);
        lstChemicalFormula.SelectedValue = "0";
        clsValidation.SetValid(lstChemicalFormula);
        txtPrice.Text = "";
        clsValidation.SetValid(txtPrice);
        txtDensity.Text = "";
        clsValidation.SetValid(txtDensity);
        //lstLevel.SelectedValue = "0";
        //clsValidation.SetValid(lstLevel);
    }

    #endregion 

    #region POPULATE DATA METHODS

    private void popFormData()
    {
         txtTitle.Text = clsNitrogenCategoryLists.strTitle;
         txtDescription.Text = clsNitrogenCategoryLists.strDescription;
         lstChemicalFormula.SelectedValue = clsNitrogenCategoryLists.iChemicalFormulaID.ToString();
        txtPrice.Text = clsNitrogenCategoryLists.dblPrice.ToString();
        txtDensity.Text = clsNitrogenCategoryLists.dblDensity.ToString();
        //lstLevel.SelectedValue = clsNitrogenCategoryLists.iLevelID.ToString();
        PopNitrogenCategories(clsNitrogenCategoryLists.iLevelID);
    }
    
    private void popChemicalFormula()
    {
         DataTable dtChemicalFormulasList = new DataTable();
         lstChemicalFormula.DataSource = clsChemicalFormulas.GetChemicalFormulasList();

         //### Populates the drop down list with PK and TITLE;
         lstChemicalFormula.DataValueField = "iChemicalFormulaID";
         lstChemicalFormula.DataTextField = "strTitle";

         //### Bind the data to the list;
         lstChemicalFormula.DataBind();

         //### Add default select option;
         lstChemicalFormula.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    }


    /// <summary>
    /// Populate the Tree view with the category types
    /// </summary>
    private void PopNitrogenCategories(int clsNitrogenCategoriesiCategoryID)
    {
        treeNitrogenCategories.Nodes.Clear();
        TreeNode Mainnode = new TreeNode();
        bool bIsSelected = false;
        Mainnode.Value = "0";
        Mainnode.Text = "Top level category";
        if (clsNitrogenCategoriesiCategoryID == 0)
        {
            Mainnode.Selected = true;
        }
        treeNitrogenCategories.Nodes.Add(Mainnode);

        DataTable DT = new DataTable();
        DT = clsNitrogenCategoryLists.GetNitrogenCategoryListsList("iLevelID = 0", "strTitle");
        foreach (DataRow r in DT.Rows)
        {
            int iNitrogenCategoryListID = 0;
            iNitrogenCategoryListID = int.Parse(r["iNitrogenCategoryListID"].ToString());
            TreeNode node = new TreeNode(r["strTitle"].ToString(), r["iNitrogenCategoryListID"].ToString());
            if (clsNitrogenCategoriesiCategoryID == int.Parse(r["iNitrogenCategoryListID"].ToString()))
            {
                node.Selected = true;
            }

            //### Now we call the PopChildCategories method to build the sub categories
            PopChildCategories(ref node, int.Parse(r["iNitrogenCategoryListID"].ToString()), clsNitrogenCategoriesiCategoryID, ref bIsSelected);
            if (bIsSelected)
            {
                node.Expanded = true;
            }
            else
            {
                node.Expanded = false;
            }
            treeNitrogenCategories.Nodes.Add(node);
        }
    }

    /// <summary>
    /// Populate the Subcategories for the Category Treeview.
    /// This Method will call itself to build the Subcategory of the current Subcategory
    /// </summary>
    /// <param name="ChildNode">The current Top level category</param>
    /// <param name="iNitrogenCategoryListID">The ID of the Category</param>
    /// <param name="clsNitrogenCategoriesiCategoryID">The ID of the clsCategories.iCategoryID</param>
    private void PopChildCategories(ref TreeNode ChildNode, int iNitrogenCategoryListID, int clsNitrogenCategoriesiCategoryID, ref bool bIsSelected)
    {
        DataTable DT = new DataTable();
        DT = clsNitrogenCategoryLists.GetNitrogenCategoryListsList("iLevelID = " + iNitrogenCategoryListID.ToString(), "strTitle");

        foreach (DataRow r in DT.Rows)
        {
            TreeNode node = new TreeNode(r["strTitle"].ToString(), r["iNitrogenCategoryListID"].ToString());
            if (clsNitrogenCategoriesiCategoryID == int.Parse(r["iNitrogenCategoryListID"].ToString()))
            {
                node.Selected = true;
                bIsSelected = true;
            }
            if (bIsSelected)
            {
                node.Expanded = true;
            }
            ChildNode.ChildNodes.Add(node);

            //### Now we call this methode again to build the subsubcategories
            PopChildCategories(ref node, int.Parse(r["iNitrogenCategoryListID"].ToString()), clsNitrogenCategoriesiCategoryID, ref bIsSelected);
        }
    }

    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        clsNitrogenCategoryLists.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsNitrogenCategoryLists.iAddedBy = clsUsers.iUserID;
        clsNitrogenCategoryLists.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsNitrogenCategoryLists.iEditedBy = clsUsers.iUserID;
        clsNitrogenCategoryLists.strTitle = txtTitle.Text;
        clsNitrogenCategoryLists.strDescription = txtDescription.Text;
        clsNitrogenCategoryLists.iChemicalFormulaID = Convert.ToInt32(lstChemicalFormula.SelectedValue.ToString());
        int iLevelID;

        if (treeNitrogenCategories.Nodes.Count != 0)
        {
            if (treeNitrogenCategories.SelectedNode != null)
            {
                iLevelID = int.Parse(treeNitrogenCategories.SelectedNode.Value);
            }
            else
            {
                iLevelID = 0;
            }
        }
        else
        {
            iLevelID = 0;
        }

        clsNitrogenCategoryLists.iLevelID = iLevelID;
        clsNitrogenCategoryLists.dblPrice = Convert.ToDouble(txtPrice.Text.ToString());
        clsNitrogenCategoryLists.dblDensity = Convert.ToDouble(txtDensity.Text.ToString());
        //clsNitrogenCategoryLists.iLevelID = Convert.ToInt32(lstLevel.SelectedValue.ToString());

        clsNitrogenCategoryLists.Update();

        Session["dtNitrogenCategoryListsList"] = null;

        //### Go back to view page
        Response.Redirect("frmNitrogenCategoryListsView.aspx");
    }

    #endregion
}