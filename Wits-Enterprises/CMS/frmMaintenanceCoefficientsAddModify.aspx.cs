using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CMS_frmMaintenanceCoefficientsAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsMaintenanceCoeficcients clsMaintenanceCoeficcients;
    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        if (!IsPostBack)
        {
             popProductCategoryList();
             popBiomassList();
             popCarbonSourceCategoryList();

            //### If the iMaintenanceCoeficcientID is passed through then we want to instantiate the object with that iMaintenanceCoeficcientID
            if ((Request.QueryString["iMaintenanceCoeficcientID"] != "") && (Request.QueryString["iMaintenanceCoeficcientID"] != null))
            {
                clsMaintenanceCoeficcients = new clsMaintenanceCoeficcients(Convert.ToInt32(Request.QueryString["iMaintenanceCoeficcientID"]));

                //### Populate the form
                popFormData();
            }
            else
            {
                clsMaintenanceCoeficcients = new clsMaintenanceCoeficcients();
            }
            Session["clsMaintenanceCoeficcients"] = clsMaintenanceCoeficcients;
        }
        else
        {
            clsMaintenanceCoeficcients = (clsMaintenanceCoeficcients)Session["clsMaintenanceCoeficcients"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmMaintenanceCoefficientsView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

       bCanSave = clsValidation.IsNullOrEmpty(txtTitle, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtPrice, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(lstProductCategoryList, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(lstBiomassList, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(lstCarbonSourceCategoryList, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtCoefficient, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtTime, bCanSave);

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">MaintenanceCoeficcient added successfully</div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - MaintenanceCoeficcient not added</div></div>";
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        txtTitle.Text = "";
        clsValidation.SetValid(txtTitle);
        txtDescription.Text = "";
        clsValidation.SetValid(txtDescription);
        txtPrice.Text = "";
        clsValidation.SetValid(txtPrice);
        lstProductCategoryList.SelectedValue = "0";
        clsValidation.SetValid(lstProductCategoryList);
        lstBiomassList.SelectedValue = "0";
        clsValidation.SetValid(lstBiomassList);
        lstCarbonSourceCategoryList.SelectedValue = "0";
        clsValidation.SetValid(lstCarbonSourceCategoryList);
        txtCoefficient.Text = "";
        clsValidation.SetValid(txtCoefficient);
        txtTime.Text = "";
        clsValidation.SetValid(txtTime);
        txtReferenceNotes.Text = "";
        clsValidation.SetValid(txtReferenceNotes);
    }

    #endregion 

    #region POPULATE DATA METHODS

    private void popFormData()
    {
         txtTitle.Text = clsMaintenanceCoeficcients.strTitle;
         txtDescription.Text = clsMaintenanceCoeficcients.strDescription;
        txtPrice.Text = clsMaintenanceCoeficcients.dblPrice.ToString();
        lstProductCategoryList.SelectedValue = clsMaintenanceCoeficcients.iProductCategoryListID.ToString();
         lstBiomassList.SelectedValue = clsMaintenanceCoeficcients.iBiomassListID.ToString();
         lstCarbonSourceCategoryList.SelectedValue = clsMaintenanceCoeficcients.iCarbonSourceCategoryListID.ToString();
        txtCoefficient.Text = clsMaintenanceCoeficcients.dblCoeficcient.ToString();
        txtTime.Text = clsMaintenanceCoeficcients.dblTime.ToString();
        txtReferenceNotes.Text = clsMaintenanceCoeficcients.strReferenceNotes;
        lstAnEstimate.SelectedValue = clsMaintenanceCoeficcients.bIsAnEstimate.ToString();
    }
    
    private void popProductCategoryList()
    {
         DataTable dtProductCategoryListsList = new DataTable();
         lstProductCategoryList.DataSource = clsProductCategoryLists.GetProductCategoryListsList();

         //### Populates the drop down list with PK and TITLE;
         lstProductCategoryList.DataValueField = "iProductCategoryListID";
         lstProductCategoryList.DataTextField = "strTitle";

         //### Bind the data to the list;
         lstProductCategoryList.DataBind();

         //### Add default select option;
         lstProductCategoryList.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    }
    
    private void popBiomassList()
    {
         DataTable dtBiomassListsList = new DataTable();
         lstBiomassList.DataSource = clsBiomassLists.GetBiomassListsList();

         //### Populates the drop down list with PK and TITLE;
         lstBiomassList.DataValueField = "iBiomassListID";
         lstBiomassList.DataTextField = "strTitle";

         //### Bind the data to the list;
         lstBiomassList.DataBind();

         //### Add default select option;
         lstBiomassList.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    }
    
    private void popCarbonSourceCategoryList()
    {
         DataTable dtCarbonSourceCategoryListsList = new DataTable();
         lstCarbonSourceCategoryList.DataSource = clsCarbonSourceCategoryLists.GetCarbonSourceCategoryListsList();

         //### Populates the drop down list with PK and TITLE;
         lstCarbonSourceCategoryList.DataValueField = "iCarbonSourceCategoryListID";
         lstCarbonSourceCategoryList.DataTextField = "strTitle";

         //### Bind the data to the list;
         lstCarbonSourceCategoryList.DataBind();

         //### Add default select option;
         lstCarbonSourceCategoryList.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    }
    
    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        clsMaintenanceCoeficcients.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsMaintenanceCoeficcients.iAddedBy = clsUsers.iUserID;
        clsMaintenanceCoeficcients.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsMaintenanceCoeficcients.iEditedBy = clsUsers.iUserID;
        clsMaintenanceCoeficcients.strTitle = txtTitle.Text;
        clsMaintenanceCoeficcients.strDescription = txtDescription.Text;
        clsMaintenanceCoeficcients.dblPrice = Convert.ToDouble(txtPrice.Text);
        clsMaintenanceCoeficcients.iProductCategoryListID = Convert.ToInt32(lstProductCategoryList.SelectedValue.ToString());
        clsMaintenanceCoeficcients.iBiomassListID = Convert.ToInt32(lstBiomassList.SelectedValue.ToString());
        clsMaintenanceCoeficcients.iCarbonSourceCategoryListID = Convert.ToInt32(lstCarbonSourceCategoryList.SelectedValue.ToString());
        clsMaintenanceCoeficcients.dblCoeficcient = Convert.ToDouble(txtCoefficient.Text);
        clsMaintenanceCoeficcients.dblTime = Convert.ToDouble(txtTime.Text);
        clsMaintenanceCoeficcients.bIsAnEstimate = Convert.ToBoolean(lstAnEstimate.Text);
        clsMaintenanceCoeficcients.strReferenceNotes = txtReferenceNotes.Text;

        clsMaintenanceCoeficcients.Update();

        Session["dtMaintenanceCoeficcientsList"] = null;

        //### Go back to view page
        Response.Redirect("frmMaintenanceCoefficientsView.aspx");
    }

    #endregion
}